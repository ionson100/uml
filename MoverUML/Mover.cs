﻿using System.Collections.Generic;
//using System.Linq;
using System.Windows.Forms;

namespace MoverUML
{
  public partial  class Mover
    {
      
      Control _key1;
      Control _key2;
      readonly string _name;
      readonly List<Control> _li = new List<Control>();
      const int L = 2;
      readonly string _parentTabl;
      readonly string _childTabl;
      Control _a, _b;
      readonly Control _panel;

      public Mover(string parentTabl, string childTabl,string nameIdRelation)
        {
            _parentTabl = parentTabl;
            _childTabl = childTabl; 
            _panel = Singlton.ControlP;
            _name = nameIdRelation;
            _a = UpFaintControl(_parentTabl, _panel);
            _b = UpFaintControl(_childTabl, _panel);
            Pos= FinderPositions();
            Paint();
            //Singlton.ML.Add(this);
                   

        }
        int FinderPositions()//находит позицию при создании и рефреше
        {
            int ff = MinusNo();
           
            if (_a.Location.Y > _b.Location.Y && Up())
            return 1;

            if (_a.Location.Y > _b.Location.Y && _a.Location.X < _b.Location.X && (_b.Location.Y + _b.Height - _a.Location.Y) < 0)
                return 2;
            if (_a.Location.Y > _b.Location.Y && _a.Location.X > _b.Location.X &&(_b.Location.Y+_b.Height - _a.Location.Y)<0)
                return 8;
            //--------
            if (_a.Location.Y < _b.Location.Y && Up())
                return 5;

            if (_a.Location.Y < _b.Location.Y && _a.Location.X < _b.Location.X)
                return 4;
            if (_a.Location.Y < _b.Location.Y && _a.Location.X > _b.Location.X)
                return 6;
            if (_a.Location.Y > _b.Location.Y && _a.Location.X > _b.Location.X)
                return 7;
            if (_a.Location.Y > _b.Location.Y && _a.Location.X < _b.Location.X)
                return 3;
            //-----------------------------
            if (_a.Location.X == _b.Location.X && _a.Location.Y > _b.Location.Y)
                return 1;
            if (_a.Location.X == _b.Location.X && _a.Location.Y < _b.Location.Y)
                return 5;

            if (_b.Location.X < _a.Location.X && _a.Location.Y == _b.Location.Y)
                return 7;
            if (_b.Location.X > _a.Location.X && _a.Location.Y == _b.Location.Y)
                return 3;

           

            return 0;
        }
       
        int MinusNo()
        {
            if (_a.Location.X+_a.Width - _b.Location.X+_b.Width >= 0)
                return _a.Location.X + _a.Width - _b.Location.X + _b.Width;
            return (_a.Location.X + _a.Width - _b.Location.X + _b.Width) * -1;
        }

      public int Pos { get; private set; }

      bool Up()//ориентатор для определения позиции
        {
            if (_a.Location.X < _b.Location.X && _a.Location.X+_a.Width > _b.Location.X )
                return true;
            return _a.Location.X > _b.Location.X && _a.Location.X < _b.Location.X + _b.Width;
        }
        void Paint()//рисует ключь и отвертку
        {
            switch (Pos)
            {
                case 1:
                    PaintKey();
                    
                    _key1.Location = new System.Drawing.Point(Xx(), _b.Location.Y + _b.Height);
                    _key2.Location = new System.Drawing.Point(Xx(), _a.Location.Y - _key2.Height);
                    PaintLine(Pos);
                   
                    break;

                case 2:
                    PaintKey();
                    
                    _key2.Location = new System.Drawing.Point(_a.Location.X+_a.Width, _a.Location.Y);
                    _key1.Location = new System.Drawing.Point(_b.Location.X+_b.Width/2, _b.Location.Y +_b.Height);
                    PaintLine(Pos);
                   
                    break;
                case 8:
                    PaintKey();
                    
                    _key2.Location = new System.Drawing.Point(_a.Location.X -_key2.Width, _a.Location.Y);
                    _key1.Location = new System.Drawing.Point(_b.Location.X + _b.Width / 2, _b.Location.Y + _b.Height);
                    PaintLine(Pos);
                   
                    break;
                case 5:
                    PaintKey();

                    _key2.Location = new System.Drawing.Point(Xx(), _a.Location.Y + _a.Height); //((key2)Key2).Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                    _key1.Location = new System.Drawing.Point(Xx(), _b.Location.Y - _key1.Height);
                    PaintLine(Pos);

                    break;
                case 6:
                    PaintKey();

                    _key2.Location = new System.Drawing.Point(_a.Location.X - _key2.Width, _a.Location.Y);
                    _key1.Location = new System.Drawing.Point(_b.Location.X + _b.Width / 2, _b.Location.Y -_key1.Height);
                    PaintLine(Pos);

                    break;
                case 4:
                    PaintKey();

                    _key2.Location = new System.Drawing.Point(_a.Location.X +_a.Width, _a.Location.Y);
                    _key1.Location = new System.Drawing.Point(_b.Location.X + _b.Width / 2, _b.Location.Y - _key1.Height);
                    PaintLine(Pos);

                    break;
                case 7:
                    PaintKey();

                    _key2.Location = new System.Drawing.Point(_a.Location.X - _key2.Width, _a.Location.Y);
                    _key1.Location = new System.Drawing.Point(_b.Location.X + _b.Width , _key2.Location.Y);
                    PaintLine(Pos);

                    break;
                case 3:
                    PaintKey();

                    _key2.Location = new System.Drawing.Point(_a.Location.X + _a.Width, _a.Location.Y);
                    _key1.Location = new System.Drawing.Point(_b.Location.X - _key1.Width, _key2.Location.Y);
                    PaintLine(Pos);

                    break;
            }
        }
        int Xx()
        {
            if ((_b.Location.X - _a.Location.X) >= 0)
                return _a.Location.X + (_b.Location.X - _a.Location.X);
            return _b.Location.X - (_b.Location.X - _a.Location.X);
        }

      void ClearLi()// очистка списка линий при рефреше
        {
            if (_li.Count != 0)
            {
                foreach (Line lin in _li)
                {
                    var c=_panel.Controls.Find(lin.Name,false);
                    if (c.Length != 0)
                    {
                        _panel.Controls.Remove(c[0]);
                    }
                }
            }
            _li.Clear();
        }
        void PaintKey()//создание  обьектов для работы(хуево назвал)
        {
            if (_key1 != null || _key2 != null) return;
            _key1 = new Key(_name); _key1.Show(); _panel.Controls.Add(_key1);// Key1.Name = Guid.NewGuid().ToString();
            _key2 = new Key2(_name); _key2.Show(); _panel.Controls.Add(_key2);// Key2.Name = Guid.NewGuid().ToString();
        }
        void PaintLine(int pos)//рисование линий
        {
            ClearLi();
                switch(pos)
                {
                    case 1:
                        {

                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Width = L;// Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key1.Location.X + _key1.Width / 2) + L / 2, _key1.Location.Y + _key2.Height);
                            line.Height = _key2.Location.Y - _key1.Location.Y - _key1.Height; ((Line)line).CursorBum();
                            
                            _li.Add(line);
                        }

                        break;
                    case 2:
                        {
                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Width = L;// Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key1.Location.X + _key1.Width / 2) + L / 2, _key1.Location.Y + _key1.Height);
                            line.Height = _key2.Location.Y - _key1.Location.Y - _key1.Height / 2 - L/2; ((Line)line).CursorBum();
                            _li.Add(line);
                            Control line2 = new Line(_name); line2.Show(); _panel.Controls.Add(line2); line2.Height = L;// Line2.Name = Guid.NewGuid().ToString();
                            line2.Location = new System.Drawing.Point(_key2.Location.X + _key2.Width  , _key2.Location.Y + _key2.Height/2-L);
                            line2.Width = line.Location.X - _key2.Location.X - _key2.Width + L; ((Line)line2).CursorBum();
                            _li.Add(line2);
                        }
                        break;
                    case 8:
                        {
                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Width = L;// Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key1.Location.X + _key1.Width / 2) + L / 2, _key1.Location.Y + _key1.Height);
                            line.Height = _key2.Location.Y - _key1.Location.Y - _key1.Height / 2 - L; ((Line)line).CursorBum();
                            _li.Add(line);
                            Control line2 = new Line(_name); line2.Show(); _panel.Controls.Add(line2); line2.Height = L;// Line.Name = Guid.NewGuid().ToString();
                            line2.Location = new System.Drawing.Point(line.Location.X, line.Location.Y + line.Height );
                            line2.Width = _key2.Location.X - line.Location.X; ((Line)line2).CursorBum();
                            _li.Add(line2);
                        }
                        break;
                    case 5:
                        {

                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Width = L; //Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key2.Location.X + _key1.Width / 2) + L / 2, _key2.Location.Y + _key2.Height);
                            line.Height = _key1.Location.Y - _key2.Location.Y - _key2.Height; ((Line)line).CursorBum();
                            _li.Add(line);
                        }

                        break;
                    case 6:
                        {
                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Width = L; //Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key1.Location.X + _key1.Width / 2) + L / 2, _key2.Location.Y + _key1.Height/2-L/2);
                            line.Height = _key1.Location.Y - _key2.Location.Y - _key1.Height / 2 - L / 2; ((Line)line).CursorBum();
                            _li.Add(line);
                            Control line2 = new Line(_name); line2.Show(); _panel.Controls.Add(line2); line2.Height = L;// Line.Name = Guid.NewGuid().ToString();
                            line2.Location = new System.Drawing.Point(line.Location.X, line.Location.Y); ((Line)line2).CursorBum();
                            line2.Width = _key2.Location.X - line.Location.X;
                            _li.Add(line2);
                        }
                        break;
                    case 4:
                        {
                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Width = L;// Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key1.Location.X + _key1.Width / 2) + L / 2, _key2.Location.Y + _key2.Height/2);
                            line.Height = _key1.Location.Y - _key2.Location.Y - _key1.Height / 2 - L; ((Line)line).CursorBum();
                            _li.Add(line);
                            Control line2 = new Line(_name); line2.Show(); _panel.Controls.Add(line2); line2.Height = L; //Line2.Name = Guid.NewGuid().ToString();
                            line2.Location = new System.Drawing.Point(_key2.Location.X + _key2.Width, _key2.Location.Y + _key2.Height / 2 - L);
                            line2.Width = line.Location.X - _key2.Location.X - _key2.Width + L; ((Line)line2).CursorBum();
                            _li.Add(line2);
                        }
                        break;
                    case 7:
                        {

                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Height = L;//Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key1.Location.X + _key1.Width ), _key1.Location.Y + _key1.Height/2-L/2);
                            line.Width =  _key2.Location.X - _key1.Location.X - _key1.Height; ((Line)line).CursorBum();

                            _li.Add(line);
                        }
                        break;

                    case 3:
                        {

                            Control line = new Line(_name); line.Show(); _panel.Controls.Add(line); line.Height = L;// Line.Name = Guid.NewGuid().ToString();
                            line.Location = new System.Drawing.Point((_key2.Location.X + _key2.Width), _key2.Location.Y + _key2.Height / 2 - L / 2);
                            line.Width =  _key1.Location.X - _key2.Location.X - _key2.Height; ((Line)line).CursorBum();

                            _li.Add(line);
                        }
                        break;
                }
        }
        public string Name
        {
            get { return _name; }
        }
    }
}
