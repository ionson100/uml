﻿//using System.Linq;
using System.Drawing;

namespace MoverUML
{
   public interface IChangeColour
    {
       void ChangeColourForMoving(Color colorForMoving);
       void ChangeColourBase();
       void SetBlockingDrag(bool blockingDrag);
       bool GetBlockingDrag();
     
    }
}
