﻿namespace MoverUML
{
    partial class Line
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьСвязьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.редактироватьСвязьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.idNameRelationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьСвязьToolStripMenuItem,
            this.редактироватьСвязьToolStripMenuItem,
            this.idNameRelationToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(162, 92);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1Opening);
            // 
            // удалитьСвязьToolStripMenuItem
            // 
            this.удалитьСвязьToolStripMenuItem.Image = global::MoverUML.Properties.Resources.error;
            this.удалитьСвязьToolStripMenuItem.Name = "удалитьСвязьToolStripMenuItem";
            this.удалитьСвязьToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.удалитьСвязьToolStripMenuItem.Text = "Delete relation";
            this.удалитьСвязьToolStripMenuItem.Click += new System.EventHandler(this.УдалитьСвязьToolStripMenuItemClick);
            // 
            // редактироватьСвязьToolStripMenuItem
            // 
            this.редактироватьСвязьToolStripMenuItem.Image = global::MoverUML.Properties.Resources.EditTableHS;
            this.редактироватьСвязьToolStripMenuItem.Name = "редактироватьСвязьToolStripMenuItem";
            this.редактироватьСвязьToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.редактироватьСвязьToolStripMenuItem.Text = "Edit relation";
            this.редактироватьСвязьToolStripMenuItem.Click += new System.EventHandler(this.РедактироватьСвязьToolStripMenuItemClick);
            // 
            // idNameRelationToolStripMenuItem
            // 
            this.idNameRelationToolStripMenuItem.Name = "idNameRelationToolStripMenuItem";
            this.idNameRelationToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.idNameRelationToolStripMenuItem.Text = "IdNameRelation";
            this.idNameRelationToolStripMenuItem.Click += new System.EventHandler(this.IdNameRelationToolStripMenuItemClick);
            // 
            // Line
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Goldenrod;
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "Line";
            this.Size = new System.Drawing.Size(64, 30);
            this.Load += new System.EventHandler(this.LineLoad);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem удалитьСвязьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem редактироватьСвязьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem idNameRelationToolStripMenuItem;
    }
}
