﻿//using System.Linq;

namespace MoverUML
{
   public interface ISerializableTables
    {
        SerializableTables GetSerializableTables
        {
            get;
        }
        System.Data.DataTable Table
        {
            get;
            set;
        }
    }
}
