﻿using System;
using System.Drawing;
//using System.Linq;
using System.Windows.Forms;

namespace MoverUML
{
    public partial class Key : UserControl,IChangeColour
    {
        
        bool _blockingDrag = true;
        public Key(string name)
        {
            InitializeComponent();
            Name = name;
        }

        private void PictureBox1Click(object sender, EventArgs e)
        {

        }
        #region IChangeColour Members

        void IChangeColour.ChangeColourForMoving(Color colorForMoving)
        {

        }

        void IChangeColour.ChangeColourBase()
        {

        }

        void IChangeColour.SetBlockingDrag(bool blockingDrag)
        {
            _blockingDrag = blockingDrag;
        }

        bool IChangeColour.GetBlockingDrag()
        {
            return _blockingDrag;
        }
        

        #endregion
    }
}
