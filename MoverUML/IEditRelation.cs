﻿namespace MoverUML
{
  public  interface IEditRelation
    {
      System.Data.DataRelation Relation
      {
          get;
          set;
      }

    }
}
