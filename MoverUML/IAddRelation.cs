﻿using System.Data;

namespace MoverUML
{
  public  interface IAddRelation
    {
      DataRelation Relation
      {
          get;
          set;
      }
    }
}
