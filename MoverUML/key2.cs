﻿using System.Drawing;
//using System.Linq;
using System.Windows.Forms;

namespace MoverUML
{
    public partial class Key2 : UserControl,IChangeColour
    {
        bool _blockingDrag = true;
        public Key2(string name)
        {
            InitializeComponent();
            Name = name;
        }
        #region IChangeColour Members

        void IChangeColour.ChangeColourForMoving(Color colorForMoving)
        {

        }

        void IChangeColour.ChangeColourBase()
        {

        }

        void IChangeColour.SetBlockingDrag(bool blockingDrag)
        {
            _blockingDrag = blockingDrag;
        }

        bool IChangeColour.GetBlockingDrag()
        {
            return _blockingDrag;
        }
       

        #endregion

        public Image Image
        {
            get { return pictureBox1.Image; }
            set { pictureBox1.Image = value; }
        }
    }
}
