﻿using System.Data;

namespace MoverUML
{
  public  interface IEditTable
    {
        DataTable Table
        {
            get;
            set;
        }
       
    }
}
