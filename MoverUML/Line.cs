﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
//using System.Linq;
using System.Windows.Forms;
using MoverUML.Properties;

namespace MoverUML
{ 
    //public delegate void Remove(DataRelation R);
    public partial class Line : UserControl, IChangeColour, IDeleteRelation
    {
        //public event Remove Up_Assa;
        readonly string _nameMover;
        bool _blockingDrag=true;
        public Line(string nameMover)
        {
            InitializeComponent();
            _nameMover = nameMover;
            Name = nameMover;
           
        }

        private void LineLoad(object sender, EventArgs e)
        {
           

        }
        public string Namemover
        {
            get { return _nameMover; }
        }
        public void CursorBum()
        {
            Cursor = Height > Width ? Cursors.SizeWE : Cursors.SizeNS;
        }

        private void УдалитьСвязьToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.Line_УдалитьСвязьToolStripMenuItemClick_Подтвердите_намерения, Resources.Line_УдалитьСвязьToolStripMenuItemClick_Ахтунг__, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
            {
               foreach(DataTable t in Singlton.DataSet.Tables)
               {
                   DataRelation R = null;

                   foreach (DataRelation r in t.ParentRelations)
                   {
                       if (r.RelationName == _nameMover)
                       {
                           R = r;
                           //if (Up_Assa != null)
                           //   Up_Assa(R);
                       }
                   }
                 
                   t.ParentRelations.Remove(R);

               }
               foreach (DataTable t in Singlton.DataSet.Tables)
               {
                   DataRelation R = null;

                   foreach (DataRelation r in t.ChildRelations)
                   {
                       if (r.RelationName == _nameMover)
                       {
                           R = r;
                           //if (Up_Assa != null)
                           //    Up_Assa(R);
                       }
                   }
                  
                   t.ParentRelations.Remove(R);
                   t.ChildRelations.Remove(R);
               }
               Singlton.RefreshMoverForRelationAll();


                
            }
        }

        #region IChangeColour Members

        void IChangeColour.ChangeColourForMoving(Color colorForMoving)
        {
            
        }

        void IChangeColour.ChangeColourBase()
        {
            
        }

        void IChangeColour.SetBlockingDrag(bool blockingDrag)
        {
            _blockingDrag = blockingDrag;
        }

        bool IChangeColour.GetBlockingDrag()
        {
            return _blockingDrag;
        }
       

        #endregion

        private void РедактироватьСвязьToolStripMenuItemClick(object sender, EventArgs e)
        {
            Object o = Singlton.CreateObject("IEditRelation");
            if (o == null) return;
            var f = (IEditRelation)o;
            DataRelation R = null;
            foreach (DataTable t in Singlton.DataSet.Tables)
            {
                foreach (DataRelation r in t.ParentRelations)
                {
                    if (r.RelationName == _nameMover)
                    {
                        R = r;
                    }
                }
            }
            f.Relation = R;
            if (((Form)o).ShowDialog() == DialogResult.OK)
            {
                //Singlton.GetMover(NameMover).Relation = f.Relation;
            }

        }

        private void IdNameRelationToolStripMenuItemClick(object sender, EventArgs e)
        {
            MessageBox.Show(_nameMover);
        }
        //для удаление связииз формы1 при редактировании

        private void ContextMenuStrip1Opening(object sender, CancelEventArgs e)
        {
            Singlton.DeleteRelationFromForm1 = this;
        }
      
       
        //для удаление связииз формы1 при редактировании
        #region IDeleteRelation Members

        void IDeleteRelation.DeleteRelation()
        {
            foreach (DataTable t in Singlton.DataSet.Tables)
            {
                DataRelation R = null;

                foreach (DataRelation r in t.ParentRelations)
                {
                    if (r.RelationName == _nameMover)
                    {
                        R = r;
                        //if (Up_Assa != null)
                        //   Up_Assa(R);
                    }
                }

                t.ParentRelations.Remove(R);

            }
            foreach (DataTable t in Singlton.DataSet.Tables)
            {
                DataRelation R = null;

                foreach (DataRelation r in t.ChildRelations)
                {
                    if (r.RelationName == _nameMover)
                    {
                        R = r;
                        //if (Up_Assa != null)
                        //    Up_Assa(R);
                    }
                }

                t.ParentRelations.Remove(R);
                t.ChildRelations.Remove(R);
            }
            Singlton.RefreshMoverForRelationAll();
        }

        #endregion
    }
}
