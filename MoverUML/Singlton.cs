﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Xml.Serialization;
using MoverUML.Properties;

namespace MoverUML
{
  public  class Singlton
    {
      public static IDeleteRelation DeleteRelationFromForm1;
        DataSet _ds = new DataSet();
        int _incrementForControlName;
        List<Control> _listControlForMove = new List<Control>();
        Control _controlP;
        static int _fx, _fy;
        Form _ff;
        static readonly Singlton S;
        static Rectangle _rect;
        static Singlton()
        {
            S = new Singlton();
        }
        public static void Init(Form f)
        {
            S._ff = f;
            S._controlP.MouseMove += ControlPMouseMove;
            S._controlP.MouseDown += ControlPMouseDown;
            S._controlP.MouseUp += ControlPMouseUp;
            S._ff.FormClosing += FfFormClosing;
        }
        public static DataSet DataSet
        {
            get { return S._ds; }
            set { S._ds = value; }
        }
       

        public static List<Control> ListControlForMove
        {
            get { return S._listControlForMove; }
            set { S._listControlForMove = value; }
        }
        public static int IncrementForControlName
        {
            get { return S._incrementForControlName; }
            set { S._incrementForControlName = value; }
        }

      public static int X { get; private set; }

      public static int Y { get; private set; }

      public static Control ControlP
        {
            get { return S._controlP; }
            set { S._controlP = value; }
        }
       
       
        public static void GroupMoveLeft( )
        {
            foreach (Control c in S._controlP.Controls)
            {
                if (!(c is ILock) && !(c is Button))
                    ((IChangeColour)c).SetBlockingDrag(true);

            }

            foreach (Control c in S._controlP.Controls)
            {
                if (!(c is ILock) || ((IChangeColour) c).GetBlockingDrag()) continue;
                c.Left = _rect.X;
                ((IChangeColour)c).SetBlockingDrag(true);
                ((IChangeColour)c).ChangeColourBase();
                RefreshMoverForRelationAll();
            }
        }
        public static void GroupMoveRight( )
        {
            foreach (Control c in S._controlP.Controls)
            {
                if (!(c is ILock))
                    ((IChangeColour)c).SetBlockingDrag(true);

            }

            foreach (Control c in S._controlP.Controls)
            {
                if (c is ILock && ((IChangeColour)c).GetBlockingDrag() == false)
                {
                    c.Left = _rect.X + _rect.Width - c.Width;
                    ((IChangeColour)c).SetBlockingDrag(true);
                    ((IChangeColour)c).ChangeColourBase();
                    RefreshMoverForRelationAll();
                }
            }
        }
        public static void GroupMoveTop( )
        {
            foreach (Control c in S._controlP.Controls)
            {
                if (!(c is ILock) && !(c is Button))
                    ((IChangeColour)c).SetBlockingDrag(true);

            }

            foreach (Control c in S._controlP.Controls)
            {
                if (c is ILock && ((IChangeColour)c).GetBlockingDrag() == false)
                {
                    c.Top = _rect.Y;
                    ((IChangeColour)c).SetBlockingDrag(true);
                    ((IChangeColour)c).ChangeColourBase();
                    RefreshMoverForRelationAll();
                }
            }
        }
        public static void GroupMoveBottom( )
        {
            foreach (Control c in S._controlP.Controls)
            {
                if (!(c is ILock) )
                    ((IChangeColour)c).SetBlockingDrag(true);

            }

            foreach (Control c in S._controlP.Controls)
            {
                if (c is ILock && ((IChangeColour)c).GetBlockingDrag() == false)
                {
                    c.Top = _rect.Y + _rect.Height - c.Height;
                    ((IChangeColour)c).SetBlockingDrag(true);
                    ((IChangeColour)c).ChangeColourBase();
                    RefreshMoverForRelationAll();
                }
            }
        }
        public static void SerializableTable( string fileName)
        {
            try
            {
                var ff = new itogSer();
                foreach (var c in S._controlP.Controls)
                {
                    if (c is ILock)
                    {
                        ff.d.Add(((ISerializableTables)c).GetSerializableTables);
                    }
                }
               
                ff.DataSet = DataSet;
                var sr = new XmlSerializer(ff.GetType());
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {

                    sr.Serialize(fs, ff);

                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(Resources.Singlton_SerializableTable_Error_Serializable__ + ee.Message, Resources.Singlton_SerializableTable_error);
            }
        }
        public static void DesirealizeTable( string fileName)
        {
            try
            {
                DataSet.Tables.Clear();
                var ff=new itogSer();
                using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    S._incrementForControlName=0;
                    var sr = new XmlSerializer(ff.GetType());
                    ff = (itogSer)sr.Deserialize(fs);
                   
                    
                    foreach (var ss in ff.d)
                    {
                        var o=CreateObject("ILock");

                        if (o != null)
                        {
                            ((Control)o).Height = ss.ControlHeight;
                            ((Control)o).Width = ss.ControlWidth;
                            ((ILock)o).Lock(ss.ControlLock);
                            ((Control)o).Name = ss.ControlName;
                            ((Control)o).Left = ss.ControlLeft;
                            ((Control)o).Top = ss.ControlTop;
                            ((ILock)o).PictureTagSet(ss.TagPicture);
                            ((ILock)o).GetH(ss.H);
                            
                            
                           ++ S._incrementForControlName;
                            S._controlP.Controls.Add(((Control)o));
                        }
                        else
                        {
                          throw  new Exception("У табличнеого контрола отсутствует интерфейс ILock");
                        }
                    }
                   
                  DataSet.Tables.Clear();
                  DataSet = ff.DataSet;
                  foreach(DataTable T in  DataSet.Tables)
                  {
                      foreach(Control c in ControlP.Controls)
                      {
                          if (c.Name == T.TableName&&c is ILock)
                          {
                              ((ISerializableTables)c).Table = T;
                              
                          }
                      }
                  }
                    //////////////////////////////////////////////

                  RefreshMoverForRelationAll();
                   
                   
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(Resources.Singlton_DesirealizeTable_Error_DeSerializable__ + ee.Message, Resources.Singlton_SerializableTable_error);
            }
        }
        /// <summary>
        /// Проверяет есть ли у таблицы связи
        /// </summary>
        public static bool GetMoverForTable(string tableName)
        {
            return S._ds.Tables[S._ds.Tables.IndexOf(tableName)].ParentRelations.Count != 0;
        }

      /// <summary>
        /// Удаляет все связи у таблици сразу
        /// </summary>
        public  static void DeleteMoverForTable(string tableName)
        {
            S._ds.Tables[S._ds.Tables.IndexOf(tableName)].ParentRelations.Clear();
        }
        public static void ControlPMouseMove(object sender, MouseEventArgs e)
        {
           
            if (e.Button == MouseButtons.Left)
            {
                using (var f = S._controlP.CreateGraphics())
                {
                    S._controlP.CreateGraphics().Clear(S._controlP.BackColor);
                    using (var df = new GraphicsPath(new[]{
                                                                      new Point(_fx,_fy),
                                                                      new Point(e.X,_fy), 
                                                                      new Point(e.X,e.Y),
                                                                      new Point (_fx,e.Y),
                                                                      new Point(_fx,_fy)
                                                          },
                                                       new[]{
                                                                     (byte)PathPointType.Start,
                                                                     (byte)PathPointType.Line,
                                                                     (byte)PathPointType.Line,
                                                                     (byte)PathPointType.Line,
                                                                     (byte)PathPointType.Line
                                                                 }))
                    {
                        using (var pens = new Pen(Color.Blue) {DashStyle = DashStyle.DashDotDot})
                        {
                             f.DrawPath(pens, df);
                        }
                        
                   
                    _rect.Y = _fy;
                    _rect.X = _fx;
                    _rect.Height = e.Y - _fy;
                    _rect.Width = e.X - _fx;
                    foreach (Control c in S._controlP.Controls)
                    {
                        if (c.Top > _fy && c.Top < e.Y && c.Left > _fx && c.Left < e.X)
                        {
                            if (c is IChangeColour)
                            {
                                ((IChangeColour)c).ChangeColourForMoving(Color.Azure);
                                ((IChangeColour)c).SetBlockingDrag(false);

                            }

                            if (ListControlForMove.IndexOf(c) == -1)
                                ListControlForMove.Add(c);
                        }

                    }
                    }
                }
            }
        }
        private static void ControlPMouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right)
            {
                X = e.X; Y = e.Y;
            }
            else
            {
                // ToolGrouMoveEnable(false);
                try
                {
                    if (ListControlForMove.Count != 0)
                    {
                        foreach (var c in ListControlForMove)
                        {
                            if (c is IChangeColour)
                            {
                                ((IChangeColour)c).ChangeColourBase();
                                ((IChangeColour)c).SetBlockingDrag(true);
                            }

                        }
                    }
                    ListControlForMove.Clear();
                   
                }
                catch
                {
                   
                }
                _fx = e.X; _fy = e.Y;

            }
        }
        private static void ControlPMouseUp(object sender, MouseEventArgs e)
        {
            
            S._controlP.CreateGraphics().Clear(S._controlP.BackColor);
        }
        private static void FfFormClosing(object sender, FormClosingEventArgs e)
        {
            if (ControlP.Controls.Count != 0)
            {
                var sfd = new SaveFileDialog {Filter = Resources.Singlton_FfFormClosing_XML___xml_All____};
                var dr = MessageBox.Show(Resources.Singlton_FfFormClosing_Save_scheme__, Resources.Singlton_FfFormClosing_Question__, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                switch (dr)
                {
                    case DialogResult.Yes:
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            SerializableTable(sfd.FileName);
                            e.Cancel = false;
                        }
                        else
                        {
                            e.Cancel = true;
                        }
                        break;
                    case DialogResult.No:
                        e.Cancel = false;
                        break;
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
            else
            {
                e.Cancel = false;
            }
        }

        public static Object CreateObject(String interfaceName)
        {
            
            object o = null;
           
            foreach (var As in AppDomain.CurrentDomain.GetAssemblies())
            {
                var fullName = As.FullName;
                foreach (var m in As.GetModules())
                {

                    foreach (Type t in m.GetTypes())
                    {
                        foreach (Type tt in t.GetInterfaces())
                        {
                            if (tt.Name == interfaceName)
                            {
                                if (t.FullName != null)
                                    o = AppDomain.CurrentDomain.CreateInstanceAndUnwrap(fullName, t.FullName);
                            }
                        }
                    }

                }
            }
            return o;
        }

      

        public static void RefreshMoverForRelationAll()
        {
            var lc = (from Control c in S._controlP.Controls where !(c is ILock) select c).ToList();
            foreach (var c in lc)
            {
                S._controlP.Controls.Remove(c);
            }

            foreach (DataTable t in S._ds.Tables)
            {
                foreach (DataRelation r in t.ParentRelations)
                {
                    new Mover(r.ParentTable.TableName, r.ChildTable.TableName, r.RelationName);
                }
            }
        }

        public static void DeleteRelationOne(String tableName)
        {
            var drl = new List<DataRelation>();
            foreach (DataTable t in S._ds.Tables)
            {
                foreach (DataRelation r in t.ParentRelations)
                {
                    if (r.ParentTable.TableName == tableName || r.ChildTable.TableName == tableName)
                    {
                        drl.Add(r);
                    }
                }
                foreach (DataRelation r in drl)
                {
                    t.ParentRelations.Remove(r);
                }
                drl.Clear();
            }
        }

        public static void RefreshMoverForRelationOne(string tableName)
        {
            foreach (DataRelation r in S._ds.Tables[S._ds.Tables.IndexOf(tableName)].ParentRelations)
                    {
                            S.DeleteControlAkaRelationName(r.RelationName);
                             new Mover(r.ParentTable.TableName, r.ChildTable.TableName, r.RelationName);
                       
                    }
                     foreach (DataRelation r in S._ds.Tables[S._ds.Tables.IndexOf(tableName)].ChildRelations)
                    {
                            S.DeleteControlAkaRelationName(r.RelationName);
                            new Mover(r.ParentTable.TableName, r.ChildTable.TableName, r.RelationName);
                       
                    }          
           
        }
      private  void DeleteControlAkaRelationName(string relationName)
      {
          var ll = new List<Control>();
          var l = new List<string>();
          foreach (Control c in S._controlP.Controls)
          {
              if (c.Name == relationName)
                  ll.Add(c);
              l.Add(c.Name);
          }
          foreach (var c in ll)
          {
              S._controlP.Controls.Remove(c);
          }
      }
      
    }
}
