﻿//using System.Linq;

namespace MoverUML
{
   public interface ILock
    {
        void Lock(bool Lock);
        void PictureTagSet(object tag);
        void GetH(int h);
       
       
    }
   public interface IOpenOrClose
   {
       void Open();
       void Close();
   }
}
