﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace uml
{

    public interface MyUserEditor
    {
        Type GetTypeCell();
        Object GetObject();
    }
    public partial class RowEditor : UserControl,MyUserEditor
    {
        DataRow DataRow;
        bool isNull;
        private readonly Type _type;

        public RowEditor( string columnName,object value,DataRow dataRow,bool isNull,Type type)
        {
            InitializeComponent();
            label1.Text = columnName;
            textBox1.Text = value.ToString();
            DataRow = dataRow;
            this.isNull = isNull;
            _type = type;
            if (!isNull)
            {
                label2.Visible = true;
            }
        }

        public Type GetTypeCell()
        {
            return _type;
        }

        public Object GetObject()
        {
            return Get(textBox1.Text, _type);
        }

        private object Get(string str, Type type)
        {
            if (type == typeof (int))
            {
                return int.Parse(str);
            }
            if (type == typeof(uint))
            {
                return uint.Parse(str);
            }
            if (type == typeof(short))
            {
                return short.Parse(str);
            }
            if (type == typeof(ushort))
            {
                return ushort.Parse(str);
            }
            if (type == typeof(long))
            {
                return long.Parse(str);
            }
            if (type == typeof(ulong))
            {
                return ulong.Parse(str);
            }
            if (type == typeof(decimal))
            {
                return decimal.Parse(str);
            }
            if (type == typeof(float))
            {
                return float.Parse(str);
            }
            if (type == typeof(string))
            {
                return str;
            }
            if (type == typeof(DateTime))
            {
                return DateTime.Parse(str);
            }
            throw new Exception("Не могу определить тип");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void RowEditor_Load(object sender, EventArgs e)
        {

        }
    }
}
