﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using MoverUML;
using System.Diagnostics;
using uml.Properties;

namespace uml
{
    public partial class MapperSubClass : Form
    {
        public MapperSubClass()
        {
            InitializeComponent();
        }

        private void MapperSubClassLoad(object sender, EventArgs e)
        {
            foreach (DataTable t in Singlton.DataSet.Tables)
            {
                listBox1.Items.Add(t.TableName);
            }
        }

        private void Button1Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox1, listBox2);
        }

        private void Button2Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox2, listBox1);

        }

        private void Button5Click(object sender, EventArgs e)
        {
            if (listBox2.Items.Count == 0|textBox1.Text==""|textBox2.Text=="")
            {
                MessageBox.Show(Resources.MapperSubClass_button5_Click_Нет_таблиц_для_маппинга);
                return;
            }
            var ltm = new List<string>();
            foreach (string s in listBox2.Items)
            {
                ltm.Add(s);
            }

            try
            {
                MapperCardsharp.Mapp.SubClass(Application.StartupPath + "/pattern/areal.xml",
                    Settings1.Default.path,
                    Singlton.DataSet,
                    ltm,textBox1.Text,textBox2.Text,
                    Settings1.Default.help,
                    Settings1.Default.overrides,
                    Settings1.Default.namecpase,
                    Settings1.Default.assembly);
                Process.Start(Settings1.Default.path);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }

        }
    }
}
