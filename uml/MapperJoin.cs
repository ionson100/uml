﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using MoverUML;
using System.Diagnostics;
using uml.Properties;

namespace uml
{
    public partial class MapperJoin : Form
    {
        List<string> _l = new List<string>();
        public MapperJoin()
        {
            InitializeComponent();
        }

        private void MapperJoinLoad(object sender, EventArgs e)
        {
            foreach (DataTable t in Singlton.DataSet.Tables)
            {
                listBox1.Items.Add(t.TableName);
            }
        }

        private void Button1Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox1, listBox2);
        }

        private void Button2Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox2, listBox1);
        }

        private void Button7Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox1, listBox3);
        }

        private void Button6Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox3, listBox1);
        }

        private void Button5Click(object sender, EventArgs e)
        {

            if (listBox2.Items.Count == 0|listBox3.Items.Count==0)
            {
                MessageBox.Show(Resources.MapperJoin_Button5Click_Нет_таблиц_для_маппинга);
                return;
            }
            var ltm = new List<string>();
            foreach (string s in listBox3.Items)
            {
                ltm.Add(s);
            }

            try
            {
                MapperCardsharp.Mapp.JoinMapp(Application.StartupPath + "/pattern/areal.xml",
                    Settings1.Default.path,
                    Singlton.DataSet,listBox2.Items[0].ToString(),
                    ltm,
                    Settings1.Default.help,
                    Settings1.Default.overrides,
                    Settings1.Default.namecpase,
                    Settings1.Default.assembly);
                Process.Start(Settings1.Default.path);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }





            ////if (((this.Conect != "") && (this.TV.Nodes.Count != 0)) && (((this.TV1.Nodes.Count != 0) && (this.Patch != "")) && (this.Patch != null)))
            ////{
            //string tableClass = "";
            //    XmlDocument document = new XmlDocument();
            //    document.Load(Application.StartupPath + "/pattern/areal.xml");
            //    XmlNode lastChild = document.LastChild;
            //    this.Cursor = Cursors.WaitCursor;
            //    StringBuilder builder = new StringBuilder("");
            //    try
            //    {
            //        string innerText = lastChild.SelectNodes("head")[0].InnerText;
            //        string str2 = lastChild.SelectNodes("colum")[0].InnerText;
            //        string str3 = lastChild.SelectNodes("boins")[0].InnerText;
            //        string str4 = lastChild.SelectNodes("bootom")[0].InnerText;
            //        string[] strArray = lastChild.SelectNodes("uncus")[0].InnerText.Split(new char[] { ';' });
            //        builder.ToString().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            //        int num = 0;
            //        tableClass = this.listBox2.Items[0].ToString();//hed
            //        StringBuilder builder2 = new StringBuilder();
            //        foreach (DataRow R in Util.ii.ShowFields(listBox2.Items[0].ToString()).Rows)
            //        {
            //            //if (node2.ImageIndex == 3)
            //            //{
            //            //    builder2.Append(strArray[1].Replace("name=\"Name\"", "name=\"" + node2.Text + "_1\"").Replace("column=\"Name\"", "column=\"" + node2.Text + "\"").Replace("\"Type\"", "\"" + node2.Tag.ToString() + "\"").Replace("native", "assigned"));
            //            //}
            //            //else
            //            //{
            //            builder2.Append(str2.Replace("name=\"Name\"", "name=\"" + R[0].ToString() + "_1\"").Replace("column=\"Name\"", "column=\"" + R[0].ToString() + "\"").Replace("Type", R[0].ToString()).Replace("length=\"Length\"", " "));
            //            //}
            //            builder2.Append("\r\n");
            //        }
            //        StringBuilder builder3 = new StringBuilder();
            //        StringBuilder builder4 = new StringBuilder();
            //        StringBuilder builder5 = new StringBuilder();
            //        foreach (object O in listBox3.Items)
            //        {
            //            builder4.Remove(0, builder4.Length);
            //            builder3.Remove(0, builder3.Length);
            //            string newValue = "";
            //            foreach (DataRow R in Util.ii.ShowFields(O.ToString()).Rows)
            //            {
            //                //if (node4.ImageIndex == 3)
            //                //{
            //                //    newValue = node4.Text;
            //                //}
            //                //else
            //                //{
            //                builder3.Append(str2.Replace("Name", R[0].ToString()).Replace("Type", R[0].ToString()).Replace("length=\"Length\"", " ") + "\r\n");
            //                //}
            //            }
            //            builder4.Append(str3.Replace("#NAME", O.ToString()).Replace("#TABLE", O.ToString()).Replace("#KeyID", newValue).Replace("#PROPERTY", builder3.ToString()));
            //            builder5.Append(builder4);
            //        }
            //        string str6 = "";
            //        if (Settings1.Default.help)
            //        {
            //            str6 = lastChild.SelectNodes("helper")[0].InnerText;
            //        }
            //        File.WriteAllText(Settings1.Default.path + "/" + tableClass + ".hbm.xml", string.Concat(new object[] { innerText.Replace("\"Class\"", "\"" + tableClass + "\"").Replace("\"Table\"", "\"" + tableClass + "\"").Replace("\"Namespace\"", "\"" + Settings1.Default.namecpase + "\"").Replace("\"Assembly\"", "\"" + Settings1.Default.assembly + "\""), "\r\n", builder2.ToString(), "\r\n", builder5, "\r\n", str4, "\r\n\r\n\r\n", str6 }), Encoding.UTF8);
            //        num++;
            //        string str7 = lastChild.SelectNodes("headlist")[0].InnerText;
            //        string str8 = lastChild.SelectNodes("ctor")[0].InnerText;
            //        string str9 = lastChild.SelectNodes("metod")[0].InnerText;
            //        string str10 = lastChild.SelectNodes("metodJoin")[0].InnerText;
            //        builder2.Remove(0, builder2.Length);
            //        builder2.Append(str7.Replace("#CLASS", tableClass).Replace("#NAMECPASE",Settings1.Default.namecpase) + "\r\n\r\n");
            //        foreach (DataRow R in Util.ii.ShowFields(listBox2.Items[0].ToString()).Rows)
            //        {
            //            builder2.Append("        private " + "tip" + " " + R[0].ToString() + "_1_;\r\n");
            //        }
            //        builder2.Append("\r\n");
            //        builder2.Append(str8.Replace("#CLASS", tableClass) + "\r\n");
            //        builder2.Append("        #region Public\r\n");
            //        foreach (DataRow R in Util.ii.ShowFields(listBox2.Items[0].ToString()).Rows)
            //        {
            //            builder2.Append(str9.Replace("#TYPE", "TYP ").Replace("#METODNAME", R[0].ToString() + "_1") + "\r\n");
            //        }
            //        builder2.Append("        #endregion\r\n    }\r\n}");
            //        File.WriteAllText(Settings1.Default.path + "/" + tableClass + ".cs", builder2.ToString(), Encoding.UTF8);




            //        foreach (Object O in listBox3.Items)//join
            //        {
            //            builder2.Remove(0, builder2.Length);
            //            builder2.Append(str7.Replace("#CLASS", O.ToString()).Replace("#NAMECPASE", Settings1.Default.namecpase) + "\r\n\r\n");
            //            foreach (DataRow R in Util.ii.ShowFields(O.ToString()).Rows)
            //            {
            //                builder2.Append("        private " + "TIP" + " " + R[0].ToString() + "_;\r\n");
            //            }
            //            foreach (DataRow R in Util.ii.ShowFields(listBox2.Items[0].ToString()).Rows)
            //            {
            //                builder2.Append("        private " + "TIP" + " " + R[0].ToString() + "_1_;\r\n");
            //            }
            //            builder2.Append("\r\n");
            //            builder2.Append(str8.Replace("#CLASS", O.ToString()) + "\r\n");
            //            builder2.Append("        #region Public\r\n");
            //            foreach (DataRow R  in Util.ii.ShowFields(O.ToString()).Rows)
            //            {
            //                builder2.Append(str9.Replace("#TYPE", "TYP").Replace("#METODNAME", R[0].ToString()) + "\r\n");
            //            }
            //            foreach (DataRow R in Util.ii.ShowFields( listBox2.Items[0].ToString()).Rows)
            //            {
            //                builder2.Append(str10.Replace("#TYPE", "TYP").Replace("#METODNAME", R[0].ToString()) + "\r\n");
            //            }
            //            builder2.Append("        #endregion\r\n    }\r\n}");
            //            File.WriteAllText(Settings1.Default.path + "/" + O.ToString() + ".cs", builder2.ToString(), Encoding.UTF8);
            //        }




            //    }
            //    catch (Exception exception)
            //    {
            //        MessageBox.Show(exception.Message, "error");
            //        return;
            //    }
            //    finally
            //    {
            //        this.Cursor = Cursors.Default;
            //    }
            //    MessageBox.Show("End to operations", "ok");
            //    Process.Start(Settings1.Default.path);
            ////}

        }
    }
}
