﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace uml
{
    public partial class RowsEditorImage : UserControl,MyUserEditor
    {
        public object Value { get; set; }
        private readonly string _columnName;
        private readonly DataRow _dataRow;
        private readonly bool _isNull;
        private readonly Type _type;


        public RowsEditorImage(string columnName, object value, DataRow dataRow, bool isNull, Type type)
        {
            Value = value;
            _columnName = columnName;
            _dataRow = dataRow;
            _isNull = isNull;
            _type = type;
            InitializeComponent();
            if(value is DBNull) return;
            ImageConverter imageConverter = new ImageConverter();
            pictureBox1.Image =(Image) imageConverter.ConvertFrom(value);
          
            label1.Text = columnName;
            label2.Visible = !isNull;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Bitmap image1 = (Bitmap)Image.FromFile(openFileDialog1.FileName, true);
                pictureBox1.Image = image1;
            }
        }

        public Type GetTypeCell()
        {
            if (pictureBox1.Image == null)
            {
                return  _type;
            }
            return typeof (byte[]);
        }

        public object GetObject()
        {
            if (pictureBox1.Image == null)
            {
                return null;
            }
            ImageConverter imageConverter = new ImageConverter();
            byte[] xByte = (byte[])imageConverter.ConvertTo(pictureBox1.Image, typeof(byte[]));
            return xByte;
        }
    }
}
