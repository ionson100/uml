﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using MoverUML;
using System.Diagnostics;
using MapperCardsharp;
using uml.Properties;

namespace uml
{
    public partial class MapperShowmen : Form
    {
        List<string> _l = new List<string>();
        //string key = "";
        
        public MapperShowmen()
        {
            InitializeComponent();
           
        }

        private void MapperShowmenLoad(object sender, EventArgs e)
        {
            var tt = new ToolTip();
            tt.SetToolTip(button5, "Generation of the code");

           foreach(DataTable T in Singlton.DataSet.Tables)
            {
                listBox1.Items.Add(T.TableName);
               
            }
           foreach (var s in Util.SimpleMappingTable)
           {
               listBox2.Items.Add(s);
               listBox1.Items.Remove(s);
           }

        }

        private void Button1Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox1, listBox2);
         
        }

        private void Button2Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox2, listBox1);
        }

        private void Button3Click(object sender, EventArgs e)
        {
            Util.SimpleMappingTable.Clear();
            foreach (var o in listBox2.Items)
            {
                Util.SimpleMappingTable.Add(o.ToString());
            }
            Close();
        }

      

        private void Button5Click(object sender, EventArgs e)
        {
            if (listBox2.Items.Count == 0)
            {
                MessageBox.Show(Resources.MapperShowmen_Button5Click_Нет_таблиц_для_маппинга);
                return;
            }
            var ltm = new List<string>();
            foreach (string s in listBox2.Items)
            {
                ltm.Add(s);
            }

            try
            {
                BiderictionalType type = 0;
                if (Settings1.Default.UseBiderctional)
                    type =(BiderictionalType) Settings1.Default.BideriectionalType;
                Mapp.SinpleMapp(Application.StartupPath + "/pattern/areal.xml",
                    Settings1.Default.path,
                    Singlton.DataSet,
                    ltm,
                    Settings1.Default.help,
                    Settings1.Default.overrides,
                    Settings1.Default.namecpase,
                    Settings1.Default.assembly,type);
                Process.Start(Settings1.Default.path);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }

        
        }

        private void button6_Click(object sender, EventArgs e)
        {
            foreach (string s in listBox1.Items)
            {
                listBox2.Items.Add(s);
            }
            listBox1.Items.Clear();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            foreach (string s in listBox2.Items)
            {
                if(listBox1.Items.IndexOf(s)==-1)
                listBox1.Items.Add(s);
            }
            listBox2.Items.Clear();

        }


    }
}
