﻿using System;
using System.Collections.Generic;
using System.Text;
using MoverUML;
using System.Windows.Forms;
using System.Data;

namespace uml
{
   public class Constructor:ISupplier,IDisposable
    {
        #region ISupplier Members

        public DataTable ShowDataBase()
        {
            throw new NotImplementedException();
        }

        public DataTable ShowTablesFromBase(string bases)
        {
            throw new NotImplementedException();
        }

        public DataTable SelectFromTable(string table)
        {
            var con = new TableS();

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == table)
                    con = (TableS)c;
            }
            return con.Table;
        }

        public string Base
        {
            get
            {
                return "base";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string ConnectString
        {
            get { return  Settings1.Default.ConString; }
        }

        public string Dialect
        {
            get { return Util.DubleIi.Dialect; }
        }

        public void Updata(DataTable t)
        {
            throw new NotImplementedException();
        }

        public DataTable ShowFields(string tablename)
        {
            throw new NotImplementedException();
        }

        public List<RelationObject> GetRelationObject(string tableName)
        {
            return new List<RelationObject>();
           // throw new NotImplementedException();
        }

        public void DrobForeignKey(string tableName, string id)
        {
            throw new NotImplementedException();
        }

        public void CreateForeiginKey(string tableNameChild, string tableNameParent, string columnNameChild, string columnNameParenr, string idRelation)
        {
          //  throw new NotImplementedException();
        }

        public string[] GetTypeColumn()
        {
           return  new[] { "Int32", "Int16", "Int64","Byte[]",
               "String", "Char", "Image", "Byte", "Float", "Double", "Bool","DateTime","Guid" };

        }

        public DataTable ShowFieldsForEdit(string tableName)////////////////////////////////////////////////////
        {

            var con = new TableS();

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == tableName)
                    con = (TableS)c;
            }
            var t = new DataTable();
            t.Columns.Add(new DataColumn("Field",typeof(string)));
            t.Columns.Add(new DataColumn("Type", typeof(string)));
            t.Columns.Add(new DataColumn("Collation", typeof(string)));
            t.Columns.Add(new DataColumn("Null", typeof(string)));
            t.Columns.Add(new DataColumn("Key", typeof(string)));
            t.Columns.Add(new DataColumn("Default", typeof(string)));
            t.Columns.Add(new DataColumn("Extra", typeof(string)));
            t.Columns.Add(new DataColumn("Privilrges", typeof(string)));
            t.Columns.Add(new DataColumn("Comment", typeof(string)));
            foreach (DataColumn c in con.Table.Columns)
            {
                var name = c.ColumnName;
                var type = c.DataType.Name;
                if(!c.DataType.IsValueType)
                    type=type+"("+c.MaxLength+")";
                var nulls = c.AllowDBNull ? "YES" : "NO";
                var extra = c.AutoIncrement ? "auto_increment" : "";
                var key="";
                if(con.Table.PrimaryKey.Length!=0)
                key = con.Table.PrimaryKey[0].ColumnName == c.ColumnName ? "PRI" : "";
                t.Rows.Add(name, type, "", nulls, key, "", extra, "", "");
            }
            return t;

           
        }

        public void DeletePrimaryKey(string tableName)
        {

            var con = new TableS();

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == tableName)
                    con = (TableS)c;
            }
            con.Table.PrimaryKey = null;

            //throw new NotImplementedException();
        }

        public void AddPrimaryKey(string tableName, string columnnane)
        {
            var con = new TableS();  

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == tableName)
                    con = (TableS)c;
            }

            con.Table.PrimaryKey = new[] { con.Table.Columns[columnnane] };

          //  throw new NotImplementedException();
        }

        public void ModufyColumn(string table, string nameOld, string nameNew, string type, string defaults, string nulls, string comment)
        {
            var con = new TableS();

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == table)
                    con = (TableS)c;
            }
            var dc = new DataColumn();
            foreach (DataColumn c in con.Table.Columns)
            {
                if (c.ColumnName != nameOld) continue;
                dc = c;
                break;
            }
            dc.ColumnName = nameNew;
            dc.DataType = GetTypeS(type.Substring(0,type.IndexOf("(", StringComparison.Ordinal)));
            if(!dc.DataType.IsValueType)
            dc.MaxLength = 10;
            if (nulls == "NULL") dc.AllowDBNull = true;
            if (nulls == "NOT NULL") dc.AllowDBNull = false;
        }

        public void AddColumn(string table, string name, string type, string defaults, string nulls)
        {
            var con = new TableS();

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == table)
                    con = (TableS)c;
            }
            var dc = new DataColumn(name,GetTypeS(type));
            if (nulls == "NULL") dc.AllowDBNull = true;
            if (nulls == "NOT NULL") dc.AllowDBNull = false;
            con.Table.Columns.Add(dc);
            

           // throw new NotImplementedException();
        }

        public void DeleteColumn(string table, string columnname)
        {
            var con = new TableS();

            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c.Name == table)
                    con = (TableS)c;
            }
            con.Table.Columns.Remove(columnname);
            
        }

        public void CreateBase(string basename, string patch)
        {
            throw new NotImplementedException();
        }

        public void DeleteBase(string basename)
        {
            throw new NotImplementedException();
        }

        public void RenameRelation(DataRelation newRelation, string idOldRelation)
        {
           // throw new NotImplementedException();
        }

        public void CreateTable(string tableName)
        {
           // throw new NotImplementedException();
        }

        public DataSet SelectFromTableForMap(string sql)
        {
            throw new NotImplementedException();
        }

        public void DeleteTable(string tableName)
        {
          //  throw new NotImplementedException();
        }

        public DataTable SqlExecute(string sql)
        {
            throw new NotImplementedException();
        }

        public string[] Word
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        //"Int32", "Int16", "Int64", "String", "Char", "Image", "Byte", "float", "double", "bool","DateTime"
       static Type GetTypeS(string type)
       {
            
           switch (type.ToUpper().Substring(0,type.IndexOf("(", StringComparison.Ordinal)))
           {
               case "INT32":
                   return Type.GetType("System.Int32");
                  // break;
               case "SYSTEM.INT32":
                   return Type.GetType("System.Int32");
                 //  break;
               case "INT16":
                   return Type.GetType("System.Int16");
                 //  break;
               case "INT64":
                   return Type.GetType("System.Int64");
                 //  break;
               case "SYSTEM.STRING":
                   return typeof(String);
                 //  break;
               case "STRING":
                   return typeof(String);
                  // break;
               case "CHAR":
                   return typeof(Char);
                 //  break;
               case "IMAGE":
                   return typeof(Byte[]);
                 //  break;
               case "BYTE[]":
                   return typeof(Byte[]);
                 //  break;
               case "BYTE":
                   return typeof(Byte);
                 //  break;
               case "FLOAT":
                   return typeof(float);
                //   break;
               case "DOUBLE":
                   return typeof(double);
                 //  break;
               case "BOOL":
                   return typeof(bool);
                 //  break;
               case "DATETIME":
                   return typeof(DateTime);
                //   break;
               case "GUID":
                   return typeof(Guid);
                 //  break;
               default:
                   throw new Exception("Can Not find type- " + type);
                //   break;
                  

               
           }
       }
        public void CreateTableS(DataTable T, BaseType bt)
        {
            if (bt == BaseType.MySql)
            {
                var b=new StringBuilder();
                b.Append(string.Format( @"CREATE TABLE `{0}` ({1}",T.TableName,Environment.NewLine));
                var c=T.PrimaryKey;
                b.Append(string.Format("`{0}` {1} NOT NULL auto_increment,{2}",c[0].ColumnName,c[0].DataType.Name,Environment.NewLine));


                b.Append(string.Format("PRIMARY KEY  (`{0}`){1}", c[0].ColumnName,Environment.NewLine));
                b.Append(string.Format(") ENGINE=InnoDB DEFAULT CHARSET=latin1{0}", Environment.NewLine));
                string ddd = b.ToString();
//   @"CREATE TABLE `client` (
//  `id` int(11) NOT NULL auto_increment,
//  `clientname` varchar(255) default NULL,
//  `statusinfo` int(11) default NULL,
//  `adress` varchar(255) default NULL,
//  `city` varchar(100) default NULL,
//  `pochtacode` varchar(100) default NULL,
//  `telephone` varchar(20) default NULL,
//  `fax` varchar(20) default NULL,
//  `cell` varchar(10) default NULL,
//  `email` varchar(30) default NULL,
//  `link` varchar(10) default NULL,
//  `girl` int(11) default NULL,
//  `datein` datetime default NULL,
//  PRIMARY KEY  (`id`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1"
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
           
        }

        #endregion
    }
}
