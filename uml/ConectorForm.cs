﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using uml.Properties;

namespace uml
{
    public partial class ConectorForm : Form
    {
        readonly string[] _lines = File.ReadAllLines(Application.StartupPath + "/pattern/ConnectString.txt");
        bool _firststart;
        public ConectorForm()
        {
            InitializeComponent();
        }

        private void ConectorFormLoad(object sender, EventArgs e)
        {
            


            foreach (string s in Enum.GetNames(typeof(BaseType)))
            {
                comboBox1.Items.Add(s);
            }
            comboBox1.SelectedIndex = comboBox1.Items.IndexOf(Enum.GetName(typeof(BaseType), Settings1.Default.ProviderEnum));
            //comboBox1.Text = Settings1.Default.Provider;
            ConnectionString.Text = Settings1.Default.ConString;
        }

        private void Button3Click(object sender, EventArgs e)
        {
            try
            {

                if (comboBox1.Text.Trim(' ') == "MySql")
                {
                    Util.PingMySql(ConnectionString.Text);
                }
                else if (comboBox1.Text.Trim(' ') == "PgSql")
                {
                   Util.PingMyPgsql(ConnectionString.Text);
                }
                else if (comboBox1.Text.Trim(' ') == "MsSql")
                {
                   Util.PingMssql(ConnectionString.Text);
                }
                MessageBox.Show(Resources.ConectorForm_Button3Click_OK);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message,Resources.ConectorForm_Button3Click_Error,MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1, 0);
            }
        }

        private void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if ((BaseType)Enum.Parse(typeof(BaseType),  comboBox1.Text)==BaseType.MySql)
            {
                ConnectionString.Text = _lines[0];
                
            }
            if ((BaseType)Enum.Parse(typeof(BaseType), comboBox1.Text) == BaseType.PgSql)
            {
                ConnectionString.Text = _lines[1];

            }
            if ((BaseType)Enum.Parse(typeof(BaseType), comboBox1.Text) == BaseType.Mssql)
            {
                ConnectionString.Text = _lines[2];

            }
            
        }

        private void ConectionStringTextChanged(object sender, EventArgs e)
        {
            button_OK.Enabled = _firststart;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            _firststart = true;
            base.OnPaint(e);
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            if (ConnectionString.Text == "") return;
            Settings1.Default.ProviderEnum =(int) Enum.Parse(typeof(BaseType), comboBox1.Text);
            Settings1.Default.ConString = ConnectionString.Text;
            Settings1.Default.Save();
            DialogResult = DialogResult.OK;
        }

        private void Button1Click(object sender, EventArgs e)
        {
            if ((BaseType)(comboBox1.SelectedIndex+1)==BaseType.MySql )
            {
                var f=new MySQLConnect(ConnectionString.Text);
                if(f.ShowDialog()==DialogResult.OK)
                {
                    ConnectionString.Text = f.ConnString;
                }
                
            }
            else if ((BaseType)(comboBox1.SelectedIndex + 1) == BaseType.Mssql)
            {
                var f = new MssqlConnect(ConnectionString.Text);
                if (f.ShowDialog() == DialogResult.OK)
                {
                    ConnectionString.Text = f.ConnString;
                }

            }
            else if ((BaseType)(comboBox1.SelectedIndex + 1) == BaseType.PgSql)
            {
                var f = new Npgsql.Design.ConnectionStringEditorForm();
               
                if (f.ShowDialog() == DialogResult.OK)
                {
                   // ConnectionString.Text = f.
                }

            }
        }

        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(Environment.CurrentDirectory+"\\pattern\\ConnectString.txt");
        }
    }
}
