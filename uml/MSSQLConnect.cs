﻿using System;
using System.Windows.Forms;
using uml.Properties;

namespace uml
{
    public partial class MssqlConnect : Form
    {
        readonly string _connstring;
        public MssqlConnect(string connstring)
        {
            InitializeComponent();
            this._connstring = connstring;
        }

        private void MssqlConnectLoad(object sender, EventArgs e)
        {
             if (String.IsNullOrEmpty(_connstring)) return;
             if(  userInstance.SelectedIndex == -1) return;
            //Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\NORTHWIND.MDF;
            //Integrated Security=True;Connect Timeout=30;User Instance=True

             string[] S = _connstring.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
             filrname.Text = S[1].Remove(0, S[1].IndexOf("=", StringComparison.Ordinal) + 1);
            
            integSecurity.SelectedIndex = integSecurity.Items.IndexOf(S[2].Remove(0, S[2].IndexOf("=", StringComparison.Ordinal) + 1));
             timeout.Text = S[3].Remove(0, S[3].IndexOf("=", StringComparison.Ordinal) + 1);
             userInstance.SelectedIndex = userInstance.Items.IndexOf(S[4].Remove(0, S[4].IndexOf("=", StringComparison.Ordinal) + 1));
        }

        private void LinkLabel2LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            openFileDialog1.Filter = Resources.MssqlConnect_LinkLabel2LinkClicked_DataFile____mdf____mdf_All_files__________;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filrname.Text = openFileDialog1.FileName;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var con=new System.Data.SqlClient.SqlConnection( @"Data Source=.\SQLEXPRESS;"+
                "AttachDbFilename="+filrname.Text+
            ";Integrated Security="+integSecurity.Text+";Connect Timeout="+timeout.Text+";User Instance="+userInstance.Text);
            try
            {
                con.Open();
                MessageBox.Show("OK");
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, "Error");
            }
            finally
            {
                con.Close();
            }
        }

        public string ConnString
        {
            get { return @"Data Source=.\SQLEXPRESS;"+
                "AttachDbFilename="+filrname.Text+
            ";Integrated Security="+integSecurity.Text+";Connect Timeout="+timeout.Text+";User Instance="+userInstance.Text; }
        }
    }
}
