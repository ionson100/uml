﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using System.Windows.Forms;
using System.IO;

namespace uml
{
    class Mssql:ISupplier,IDisposable
    {
        readonly string[] _word = File.ReadAllLines(Application.StartupPath + "/pattern/MySQL_Word.txt");
        string[] _sType;
        readonly System.Data.SqlClient.SqlConnection _connect = new System.Data.SqlClient.SqlConnection(Settings1.Default.ConString);
        System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand();
        System.Data.SqlClient.SqlDataAdapter _da;

        readonly string _createtable;
        readonly string _showfields;
        string _showkey, _showtable;
        readonly string _droptable;
        string _showdatabases;
        readonly string _addcolumn;
        string _change, _dropfield;
        readonly string _typecolumn;
        readonly string _deletecolumn;
        readonly string _modify;
        string _dropbase, _showcomment;
        readonly string _dialect;
        readonly string _createdatabase;
        readonly string _createForeiginKey;
        string _dropkey;
        readonly string _drobForeignKey;
        readonly string _constr;

        public Mssql()
        {
            com.Connection = _connect;
           
            var doc = new XmlDocument();
            doc.Load(Application.StartupPath + "/pattern/MSSQL_SQL.xml");
            var areal = doc.LastChild;
            var xmlNodeList = areal.SelectNodes("createtable");
            if (xmlNodeList != null) _createtable = xmlNodeList[0].InnerText;
            var selectNodes = areal.SelectNodes("showfields");
            if (selectNodes != null) _showfields = selectNodes[0].InnerText;
            var nodeList = areal.SelectNodes("showkey");
            if (nodeList != null) _showkey = nodeList[0].InnerText;
            var nodes = areal.SelectNodes("showtable");
            if (nodes != null) _showtable = nodes[0].InnerText;
            var list = areal.SelectNodes("droptable");
            if (list != null) _droptable = list[0].InnerText;
            var xmlNodeList1 = areal.SelectNodes("showdatabases");
            if (xmlNodeList1 != null)
                _showdatabases = xmlNodeList1[0].InnerText;
            var selectNodes1 = areal.SelectNodes("addcolumn");
            if (selectNodes1 != null) _addcolumn = selectNodes1[0].InnerText;
            var nodeList1 = areal.SelectNodes("change");
            if (nodeList1 != null) _change = nodeList1[0].InnerText;
            var nodes1 = areal.SelectNodes("dropfield");
            if (nodes1 != null) _dropfield = nodes1[0].InnerText;
            var list1 = areal.SelectNodes("typecolumn");
            if (list1 != null) _typecolumn = list1[0].InnerText;
            var xmlNodeList2 = areal.SelectNodes("deletecolumn");
            if (xmlNodeList2 != null)
                _deletecolumn = xmlNodeList2[0].InnerText;
            _modify = areal.SelectNodes("modify")[0].InnerText;
            _dropbase = areal.SelectNodes("dropbase")[0].InnerText;
            _showcomment = areal.SelectNodes("showcomment")[0].InnerText;
         //   addkey = areal.SelectNodes("addkey")[0].InnerText;
            _dropkey = areal.SelectNodes("dropkey")[0].InnerText;
            _constr = areal.SelectNodes("constr")[0].InnerText;
            _dialect = areal.SelectNodes("dialect")[0].InnerText;
            _createdatabase = areal.SelectNodes("createdatabase")[0].InnerText;
            _createForeiginKey = areal.SelectNodes("createForeiginKey")[0].InnerText;
            _drobForeignKey = areal.SelectNodes("drobForeignKey")[0].InnerText;
        }
        #region ISupplier Members
       
        public DataTable ShowDataBase()
        {
            try
            {

                var t = new DataTable();
                var c = new DataColumn("tablename");
                t.Columns.Add(c);
                var s = Settings1.Default.ConString.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                t.Rows.Add(s[1].Remove(0, s[1].IndexOf("=", StringComparison.Ordinal) + 1));
                return t;
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);

            }
        }

        public DataTable ShowTablesFromBase(string bases)
        {
            try
            {
                using (_da = new System.Data.SqlClient.SqlDataAdapter("SELECT NAME FROM sys.tables", _connect))
                {
                    var t = new DataTable();
                    _da.Fill(t);
                    _da.FillSchema(t, SchemaType.Mapped);
                    Util.PrintSql("SELECT NAME FROM sys.tables");
                    return t;
                }
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);

            }
        }

        public DataTable SelectFromTable(string table)
        {
            try
            {

                //Util.PrintSQL("SELECT * FROM [" + table + "]");
                using (var da =
                    new System.Data.SqlClient.SqlDataAdapter(String.Format("SELECT {0} * FROM [{1}]", Util.Limit, table), _connect))
                {
                    var t = new DataTable();
                    da.Fill(t);
                    da.FillSchema(t, SchemaType.Mapped);


                    Util.PrintSql(String.Format("SELECT {0} * FROM [{1}]", Util.Limit, table));
                    return t;
                }
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);

            }
       //      SELECT DB_ID()
//2. SELECT database_id, name FROM sys.databases
        }

        public string Base
        {
            get
            {
                var s = Settings1.Default.ConString.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                return s[1].Remove(0, s[1].IndexOf("=", StringComparison.Ordinal) + 1);
            }
            set
            {
                //throw new NotImplementedException();
            }
        }

        public string ConnectString
        {
            get { return Settings1.Default.ConString; }
        }

        public string Dialect
        {
            get { return _dialect; }
        }

        public void Updata(DataTable t)
        {
            using (_da = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM  [" + t.TableName + "]", _connect))
            {
                var cb = new System.Data.SqlClient.SqlCommandBuilder(_da);
                _da.Update(t);
            }
        }

        public DataTable ShowFields(string tablename)
        {
            throw new NotImplementedException();
        }

        public List<RelationObject> GetRelationObject(string tableName)///////////////////////////////////////////////
        {
            try
            {
                using (_da = new System.Data.SqlClient.SqlDataAdapter(_constr.Replace("###", tableName), _connect))
                {
                    var t = new DataTable();
                    Util.PrintSql(_constr.Replace("###", tableName));
                    _da.Fill(t);
                    var l = new List<RelationObject>();
                    foreach (DataRow r in t.Rows)
                    {
                        var o = new RelationObject(r["tableParent"].ToString(), r["tableChildren"].ToString(),
                            r["columnChildren"].ToString(), r["columnParent"].ToString(), "CASCADE", "CASCADE", r["name"].ToString());
                        l.Add(o);

                    }
                    return l;
                }
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);

            }
            
        }

        public void DrobForeignKey(string tableName, string id)
        {
            var sql = _drobForeignKey.Replace("###TableName", tableName).Replace("###KEY", id);
            using (var command = new System.Data.SqlClient.SqlCommand(sql,
                 new System.Data.SqlClient.SqlConnection(Settings1.Default.ConString)))
            {
                command.Connection.Open();
                try
                {
                    Util.PrintSql(sql);
                    command.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message, ee);

                }
                finally
                {
                    command.Connection.Close();
                    Util.PrintSql(sql);
                }
            }
        }

        public void CreateForeiginKey(string tableNameChild, string tableNameParent, string columnNameChild, string columnNameParenr, string idRelation)
        {
            var onDelete = "";
            var onUpdate = "";
            if (Settings1.Default.OnUpdate != "")
                onUpdate = "ON UPDATE " + Settings1.Default.OnUpdate.Trim().ToUpper();
            if (!String.IsNullOrEmpty(Settings1.Default.OnDelete))
                onDelete = " ON DELETE " + Settings1.Default.OnDelete.Trim().ToUpper(Application. CurrentCulture);
            var sql = _createForeiginKey.
                Replace("###TableNameChild", tableNameChild).
                Replace("###IdRelation",idRelation).
                Replace("###ColumnNameChild", columnNameChild).
                Replace("###TableNameParent",tableNameParent).
                Replace("###ColumnNameParent",columnNameParenr).
                Replace(" ###OnDelete", onDelete).
                Replace("###OnUpdate", onUpdate);

            using (var command = new System.Data.SqlClient.SqlCommand("",
                 new System.Data.SqlClient.SqlConnection(Settings1.Default.ConString)))
            {
                Util.PrintSql(sql);
                command.CommandText = sql;
                command.Connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message, ee);
                }
                finally
                {
                    command.Connection.Close();

                }
            }
        }

        public string[] GetTypeColumn()
        {
            if (_sType == null)
            {
                _da = new System.Data.SqlClient.SqlDataAdapter(_typecolumn, _connect);
                var t = new DataTable();
                Util.PrintSql(_typecolumn );
                _da.Fill(t);
                _sType = new string[t.Rows.Count];
                for (var i = 0; i < t.Rows.Count; i++)
                {
                    _sType[i] = t.Rows[i][0].ToString().ToUpper();
                }
            }
            return _sType;
            
           
        }

        public DataTable ShowFieldsForEdit(string tableName)
        {
            var t = new DataTable();
           var c = new DataColumn("Field");
           var c1 = new DataColumn("Type");
           var c2 = new DataColumn("Collation");
           var c3 = new DataColumn("Null");
           var c4 = new DataColumn("Key");
           var c5 = new DataColumn("Default");
           var c6 = new DataColumn("Extra");
           var c7 = new DataColumn("Privileges");
           var c8 = new DataColumn("Comment");
            t.Columns.Add(c);
            t.Columns.Add(c1);
            t.Columns.Add(c2);
            t.Columns.Add(c3);
            t.Columns.Add(c4);
            t.Columns.Add(c5);
            t.Columns.Add(c6);
            t.Columns.Add(c7);
            t.Columns.Add(c8);
            using (var da1 =
                new System.Data.SqlClient.SqlDataAdapter(_showfields.Replace("###", tableName), _connect))
            {

                var tt = new DataTable();
                da1.Fill(tt);
                _da = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM [" + tableName + "]", _connect);
                var t2 = new DataTable();
                _da.FillSchema(t2, SchemaType.Mapped);

                var pri = new List<string>();
                foreach (var cs in t2.PrimaryKey)
                {
                    pri.Add(cs.ColumnName);
                }
                var i = -1;
                foreach (DataColumn col in t2.Columns)
                {
                    i++;
                    t.Rows.Add(
                        col.ColumnName,
                        tt.Rows[i][5].ToString().ToUpper() + "(" + tt.Rows[i][7] + ")",
                        "",
                        col.AllowDBNull ? "YES" : "NO",
                        pri.IndexOf(col.ColumnName) != -1 ? "PRI" : "",
                        col.DefaultValue,
                        col.AutoIncrement ? "auto_increment" : "",
                        "",
                        col.Caption);

                }
                return t;
            }
        }

        public void DeletePrimaryKey(string tableName)
        {
            throw new NotImplementedException();
        }

        public void AddPrimaryKey(string tableName, string columnnane)
        {
            throw new NotImplementedException();
        }

        public void ModufyColumn(string table, string nameOld, string nameNew, string type, string defaults, string nulls, string comment)
        {
            string sql = _modify.Replace("#table", table).Replace("#nameold", nameOld).Replace("#namenew", nameNew).
                   Replace("#type", type).Replace("#default", defaults).Replace("#null", nulls).Replace("#comment", comment);
            com.CommandText = sql;
                com.Connection.Open();
                try
                {
                    Util.PrintSql(sql );
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                    
                }
            
        }

        public void AddColumn(string table, string name, string type, string defaults, string nulls)
        {
            var sql = _addcolumn.Replace("#table", table).Replace("#name", name).
                    Replace("#type", type).Replace("#default", defaults).Replace("#null", nulls);
            com.CommandText = sql;
            com.Connection.Open();
           
            try
            {
                Util.PrintSql(sql);
                com.ExecuteNonQuery();
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message );
                throw new Exception(ee.Message,ee);

            }
            finally
            {
                com.Connection.Close();
               
            }
        }

        public void DeleteColumn(string table, string columnname)
        {
            string sql = _deletecolumn.Replace("###TABLE", table).Replace("###COLUMN", columnname);
            com.CommandText = sql;
           
            try
            {
                _connect.Open();
                Util.PrintSql(sql   );
                com.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message    );
                throw new Exception(ee.Message,ee);
            }
            finally
            {
                _connect.Close();
            }
        }

        public void CreateBase(string basename,string patch)
        {
            string sql = _createdatabase.Replace("###NAMEBASE", basename).Replace("###PATH", patch);
            com.CommandText = sql;
           
            try
            {
                _connect.Open();
                Util.PrintSql(sql   );
                com.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message    );
                throw new Exception(ee.Message,ee);
            }
            finally
            {
                _connect.Close();
            }
        }

        public void DeleteBase(string basename)
        {
            throw new NotImplementedException();
        }

        public void RenameRelation(DataRelation newRelation, string idOldRelation)
        {
            throw new NotImplementedException();
        }

        public void CreateTable(string tableName)
        {
            string sql = _createtable.Replace("###TABLENAME", tableName);
            com.CommandText = sql;
            com.Connection.Open();
            
            try
            {
                Util.PrintSql(sql);
                com.ExecuteNonQuery();
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message );
                throw new Exception(ee.Message,ee);

            }
            finally
            {
                com.Connection.Close();
                
            }
            
        }

        public DataSet SelectFromTableForMap(string sql)
        {
            throw new NotImplementedException();
        }

        public void DeleteTable(string tableName)
        {
            string sql = _droptable.Replace("###NAME", tableName);
            com.CommandText = sql;
            com.Connection.Open();
           
            try
            {
                Util.PrintSql(sql );
                com.ExecuteNonQuery();
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message,ee);

            }
            finally
            {
                com.Connection.Close();

            }
        }

        public DataTable SqlExecute(string sql)
        {
            try
            {
                using (var da = new System.Data.SqlClient.SqlDataAdapter(sql, _connect))
                {
                    var t = new DataTable();
                    da.Fill(t);
                    return t;
                }
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);

            }
        }

        public string[] Word
        {
            get { return _word; }
        }

        #endregion

          #region IDisposable Members

        public void Dispose()
        {
            if (_connect != null)
            {
                if (_connect.State != ConnectionState.Closed)
                    _connect.Close();
                _da.Dispose();
                _connect.Dispose();
            }
        }

        #endregion
       
       
    }
}
