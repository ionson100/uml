﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace uml
{
    public partial class Diagnostyc : Form
    {
        readonly List<DataTable> _t;
        readonly DataRelationCollection _col;
        public Diagnostyc(List<DataTable> t,DataRelationCollection col)
        {
            InitializeComponent();
            _t = t;
            _col = col;
        }

        private void DiagnostycLoad(object sender, EventArgs e)
        {
            foreach (var tt in _t)
            {
                var n = new TreeNode(tt.TableName) {Tag = null};
                var l=(List<string[]>)tt.ExtendedProperties["list"];
                foreach (var d in l)
                {
                    var nn = new TreeNode("list") {Tag = d};
                    n.Nodes.Add(nn);
                }
                treeView1.Nodes.Add(n);
            }
            Text =""+ _col.Count;

        }

        private void TreeView1AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode.Tag == null) return;
            var s = (string[])treeView1.SelectedNode.Tag;
            richTextBox1.Text = s[0];
            richTextBox2.Text = s[1];
            richTextBox3.Text = s[2];
        }
    }
}
