﻿namespace uml
{
    partial class EditRelation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_Delete = new System.Windows.Forms.ComboBox();
            this.comboBox_Update = new System.Windows.Forms.ComboBox();
            this.comboBoxChildTable = new System.Windows.Forms.ComboBox();
            this.comboBoxParentTable = new System.Windows.Forms.ComboBox();
            this.comboBoxChildColumn = new System.Windows.Forms.ComboBox();
            this.comboBoxParentColumn = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ChildTable";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "ParentTable";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(24, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(182, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.TextBox1TextChanged);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(281, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(183, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(214, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "ChildColumn";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(214, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "ParentColumn";
            // 
            // comboBox_Delete
            // 
            this.comboBox_Delete.FormattingEnabled = true;
            this.comboBox_Delete.Items.AddRange(new object[] {
            "NO ACTION",
            "CASCADE",
            "SET NULL",
            "RESTRICT"});
            this.comboBox_Delete.Location = new System.Drawing.Point(24, 165);
            this.comboBox_Delete.Name = "comboBox_Delete";
            this.comboBox_Delete.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Delete.TabIndex = 12;
            this.comboBox_Delete.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDeleteSelectedIndexChanged);
            // 
            // comboBox_Update
            // 
            this.comboBox_Update.FormattingEnabled = true;
            this.comboBox_Update.Items.AddRange(new object[] {
            "NO ACTION",
            "CASCADE",
            "SET NULL",
            "RESTRICT"});
            this.comboBox_Update.Location = new System.Drawing.Point(24, 210);
            this.comboBox_Update.Name = "comboBox_Update";
            this.comboBox_Update.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Update.TabIndex = 13;
            this.comboBox_Update.SelectedIndexChanged += new System.EventHandler(this.ComboBoxUpdateSelectedIndexChanged);
            // 
            // comboBoxChildTable
            // 
            this.comboBoxChildTable.FormattingEnabled = true;
            this.comboBoxChildTable.Location = new System.Drawing.Point(24, 78);
            this.comboBoxChildTable.Name = "comboBoxChildTable";
            this.comboBoxChildTable.Size = new System.Drawing.Size(121, 21);
            this.comboBoxChildTable.TabIndex = 14;
            this.comboBoxChildTable.SelectedIndexChanged += new System.EventHandler(this.ComboBoxChildTableSelectedIndexChanged);
            // 
            // comboBoxParentTable
            // 
            this.comboBoxParentTable.FormattingEnabled = true;
            this.comboBoxParentTable.Location = new System.Drawing.Point(24, 124);
            this.comboBoxParentTable.Name = "comboBoxParentTable";
            this.comboBoxParentTable.Size = new System.Drawing.Size(121, 21);
            this.comboBoxParentTable.TabIndex = 15;
            this.comboBoxParentTable.SelectedIndexChanged += new System.EventHandler(this.ComboBoxParentTableSelectedIndexChanged);
            // 
            // comboBoxChildColumn
            // 
            this.comboBoxChildColumn.FormattingEnabled = true;
            this.comboBoxChildColumn.Location = new System.Drawing.Point(216, 78);
            this.comboBoxChildColumn.Name = "comboBoxChildColumn";
            this.comboBoxChildColumn.Size = new System.Drawing.Size(121, 21);
            this.comboBoxChildColumn.TabIndex = 16;
            this.comboBoxChildColumn.SelectedIndexChanged += new System.EventHandler(this.ComboBoxChildColumnSelectedIndexChanged);
            // 
            // comboBoxParentColumn
            // 
            this.comboBoxParentColumn.FormattingEnabled = true;
            this.comboBoxParentColumn.Location = new System.Drawing.Point(216, 124);
            this.comboBoxParentColumn.Name = "comboBoxParentColumn";
            this.comboBoxParentColumn.Size = new System.Drawing.Size(121, 21);
            this.comboBoxParentColumn.TabIndex = 17;
            this.comboBoxParentColumn.SelectedIndexChanged += new System.EventHandler(this.ComboBoxParentColumnSelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "OnDelete";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "OnUpdate";
            // 
            // EditRelation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 276);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxParentColumn);
            this.Controls.Add(this.comboBoxChildColumn);
            this.Controls.Add(this.comboBoxParentTable);
            this.Controls.Add(this.comboBoxChildTable);
            this.Controls.Add(this.comboBox_Update);
            this.Controls.Add(this.comboBox_Delete);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditRelation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "EditRelation";
            this.Load += new System.EventHandler(this.EditRelationLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_Delete;
        private System.Windows.Forms.ComboBox comboBox_Update;
        private System.Windows.Forms.ComboBox comboBoxChildTable;
        private System.Windows.Forms.ComboBox comboBoxParentTable;
        private System.Windows.Forms.ComboBox comboBoxChildColumn;
        private System.Windows.Forms.ComboBox comboBoxParentColumn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}