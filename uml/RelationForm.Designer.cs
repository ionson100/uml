﻿namespace uml
{
    partial class RelationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_TableParent = new System.Windows.Forms.ComboBox();
            this.comboBox_tableChild = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listBox_ColumnParent = new System.Windows.Forms.ListBox();
            this.listBox_ColumChild = new System.Windows.Forms.ListBox();
            this.comboBox_Update = new System.Windows.Forms.ComboBox();
            this.comboBox_Delete = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox_TableParent
            // 
            this.comboBox_TableParent.FormattingEnabled = true;
            this.comboBox_TableParent.Location = new System.Drawing.Point(26, 8);
            this.comboBox_TableParent.Name = "comboBox_TableParent";
            this.comboBox_TableParent.Size = new System.Drawing.Size(121, 21);
            this.comboBox_TableParent.TabIndex = 0;
            this.comboBox_TableParent.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
            // 
            // comboBox_tableChild
            // 
            this.comboBox_tableChild.FormattingEnabled = true;
            this.comboBox_tableChild.Items.AddRange(new object[] {
            "\t\t\t\t\t"});
            this.comboBox_tableChild.Location = new System.Drawing.Point(210, 9);
            this.comboBox_tableChild.Name = "comboBox_tableChild";
            this.comboBox_tableChild.Size = new System.Drawing.Size(121, 21);
            this.comboBox_tableChild.TabIndex = 1;
            this.comboBox_tableChild.SelectedIndexChanged += new System.EventHandler(this.ComboBox2SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(256, 310);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "AddRelation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(166, 310);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::uml.Properties.Resources.PrimaryKeyHS;
            this.pictureBox1.Location = new System.Drawing.Point(1, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(19, 18);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // listBox_ColumnParent
            // 
            this.listBox_ColumnParent.FormattingEnabled = true;
            this.listBox_ColumnParent.Location = new System.Drawing.Point(26, 47);
            this.listBox_ColumnParent.Name = "listBox_ColumnParent";
            this.listBox_ColumnParent.Size = new System.Drawing.Size(120, 173);
            this.listBox_ColumnParent.TabIndex = 5;
            // 
            // listBox_ColumChild
            // 
            this.listBox_ColumChild.FormattingEnabled = true;
            this.listBox_ColumChild.Location = new System.Drawing.Point(210, 47);
            this.listBox_ColumChild.Name = "listBox_ColumChild";
            this.listBox_ColumChild.Size = new System.Drawing.Size(120, 173);
            this.listBox_ColumChild.TabIndex = 6;
            // 
            // comboBox_Update
            // 
            this.comboBox_Update.FormattingEnabled = true;
            this.comboBox_Update.Items.AddRange(new object[] {
            "No Action",
            "Cascade",
            "Set Null",
            "Restrict"});
            this.comboBox_Update.Location = new System.Drawing.Point(26, 244);
            this.comboBox_Update.Name = "comboBox_Update";
            this.comboBox_Update.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Update.TabIndex = 0;
            // 
            // comboBox_Delete
            // 
            this.comboBox_Delete.FormattingEnabled = true;
            this.comboBox_Delete.Items.AddRange(new object[] {
            "No Action",
            "Cascade",
            "Set Null",
            "Restrict"});
            this.comboBox_Delete.Location = new System.Drawing.Point(26, 284);
            this.comboBox_Delete.Name = "comboBox_Delete";
            this.comboBox_Delete.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Delete.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 268);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "On Delete";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "On Update";
            // 
            // RelationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(357, 345);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_Delete);
            this.Controls.Add(this.comboBox_Update);
            this.Controls.Add(this.listBox_ColumChild);
            this.Controls.Add(this.listBox_ColumnParent);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox_tableChild);
            this.Controls.Add(this.comboBox_TableParent);
            this.Name = "RelationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Создание связи";
            this.Load += new System.EventHandler(this.Form2Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_TableParent;
        private System.Windows.Forms.ComboBox comboBox_tableChild;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox listBox_ColumnParent;
        private System.Windows.Forms.ListBox listBox_ColumChild;
        private System.Windows.Forms.ComboBox comboBox_Update;
        private System.Windows.Forms.ComboBox comboBox_Delete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}