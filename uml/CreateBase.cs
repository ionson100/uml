﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using uml.Properties;

namespace uml
{
    public delegate void SelectNode(object sender, TreeViewEventArgs e);
    public partial class CreateBase : Form
    {
        public event SelectNode SelectNodeS;
        readonly List<Control>_l=new List<Control>();
        readonly TreeView _treeView;
        string _patch;
        public CreateBase(TreeView treeView,Panel panelH)
        {
            InitializeComponent();
            _treeView = treeView;
            foreach(Control c in panelH.Controls)
            _l.Add(c);
        }

        private void Button2Click(object sender, EventArgs e)
        {
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
            {
                if (string.IsNullOrEmpty(_patch))
                {
                    MessageBox.Show(Resources.CreateBase_Button2Click_No_Select_Patch_from_base,Resources.CreateBase_Button2Click_Erorr);
                    return;
                }
            }
            if (
                MessageBox.Show(string.Format("Crate Base Name: {0}?", textBox1.Text),
                                Resources.CreateBase_Button2Click_Create_Base, MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) != DialogResult.OK ||
                String.IsNullOrEmpty(textBox1.Text)) return;
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
            {
              using( ISupplier d = new Mssql())
              {
                d.CreateBase(textBox1.Text, _patch);
              }
                
                Settings1.Default.SelectedPath_MSSQL_BASE =textBox1.Text;
                Settings1.Default.Save();
                var node = new TreeNode(textBox1.Text) {Tag = 0};
                _treeView.Nodes.Add(node);
                _treeView.SelectedNode = node;
                if (SelectNodeS != null)
                {
                    var ee=new TreeViewEventArgs(node);
                    SelectNodeS(_treeView,ee);
                }
                foreach(Control c in Util.PanelH.Controls)
                {

                    ((Constructor)Util.Ii).CreateTableS(((TableS)c).Table, BaseType.Mssql);

                }
                    
                    
            }
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.MySql)
            {
                ISupplier d = new MySQL();
                d.CreateBase(textBox1.Text, "sdasdasd");
                var node = new TreeNode(textBox1.Text) {Tag = 0};
                _treeView.Nodes.Add(node);
                _treeView.SelectedNode = node;
                if (SelectNodeS != null)
                {
                    var ee = new TreeViewEventArgs(node);
                    SelectNodeS(_treeView, ee);
                }
                foreach (var c in _l.Where(c => Util.Ii is Constructor))
                {
                    ((Constructor)Util.Ii).CreateTableS(((TableS)c).Table, BaseType.MySql);
                }
                    

            }
            Close();
        }

        private void CreateBaseLoad(object sender, EventArgs e)
        {
            linkLabel1.Visible = (BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql;
        }

        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                _patch = folderBrowserDialog1.SelectedPath;
        }
    }
}
