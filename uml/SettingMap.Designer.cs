﻿namespace uml
{
    partial class SettingMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.MappType = new System.Windows.Forms.ComboBox();
            this.checkBox_relation = new System.Windows.Forms.CheckBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.checkBox_overrites = new System.Windows.Forms.CheckBox();
            this.checkBox_help = new System.Windows.Forms.CheckBox();
            this.textBox_assembly = new System.Windows.Forms.TextBox();
            this.textBox_namespace = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox_limit = new System.Windows.Forms.CheckBox();
            this.textBox_limit = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.checkBox_relation);
            this.panel1.Controls.Add(this.linkLabel1);
            this.panel1.Controls.Add(this.checkBox_overrites);
            this.panel1.Controls.Add(this.checkBox_help);
            this.panel1.Controls.Add(this.textBox_assembly);
            this.panel1.Controls.Add(this.textBox_namespace);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(325, 231);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.MappType);
            this.groupBox1.Location = new System.Drawing.Point(23, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 62);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bidirectional";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(141, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "BidedirectionalType";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(15, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(102, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "UseBidirectional";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1CheckedChanged);
            // 
            // MappType
            // 
            this.MappType.FormattingEnabled = true;
            this.MappType.Location = new System.Drawing.Point(144, 30);
            this.MappType.Name = "MappType";
            this.MappType.Size = new System.Drawing.Size(121, 21);
            this.MappType.TabIndex = 8;
            // 
            // checkBox_relation
            // 
            this.checkBox_relation.AutoSize = true;
            this.checkBox_relation.Enabled = false;
            this.checkBox_relation.Location = new System.Drawing.Point(226, 67);
            this.checkBox_relation.Name = "checkBox_relation";
            this.checkBox_relation.Size = new System.Drawing.Size(81, 17);
            this.checkBox_relation.TabIndex = 7;
            this.checkBox_relation.Text = "add relation";
            this.checkBox_relation.UseVisualStyleBackColor = true;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(20, 155);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(67, 13);
            this.linkLabel1.TabIndex = 6;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Where write.";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1LinkClicked);
            // 
            // checkBox_overrites
            // 
            this.checkBox_overrites.AutoSize = true;
            this.checkBox_overrites.Location = new System.Drawing.Point(122, 67);
            this.checkBox_overrites.Name = "checkBox_overrites";
            this.checkBox_overrites.Size = new System.Drawing.Size(66, 17);
            this.checkBox_overrites.TabIndex = 5;
            this.checkBox_overrites.Text = "overrites";
            this.checkBox_overrites.UseVisualStyleBackColor = true;
            this.checkBox_overrites.CheckedChanged += new System.EventHandler(this.CheckBox2CheckedChanged);
            // 
            // checkBox_help
            // 
            this.checkBox_help.AutoSize = true;
            this.checkBox_help.Location = new System.Drawing.Point(23, 67);
            this.checkBox_help.Name = "checkBox_help";
            this.checkBox_help.Size = new System.Drawing.Size(48, 17);
            this.checkBox_help.TabIndex = 4;
            this.checkBox_help.Text = "Help";
            this.checkBox_help.UseVisualStyleBackColor = true;
            // 
            // textBox_assembly
            // 
            this.textBox_assembly.Location = new System.Drawing.Point(167, 26);
            this.textBox_assembly.Name = "textBox_assembly";
            this.textBox_assembly.Size = new System.Drawing.Size(140, 20);
            this.textBox_assembly.TabIndex = 3;
            this.textBox_assembly.Text = "aaa";
            // 
            // textBox_namespace
            // 
            this.textBox_namespace.Location = new System.Drawing.Point(13, 26);
            this.textBox_namespace.Name = "textBox_namespace";
            this.textBox_namespace.Size = new System.Drawing.Size(148, 20);
            this.textBox_namespace.TabIndex = 2;
            this.textBox_namespace.Text = "namespace";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "assembly";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "namespace";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(136, 249);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(65, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(272, 249);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(65, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Ok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_limit);
            this.groupBox2.Controls.Add(this.checkBox_limit);
            this.groupBox2.Location = new System.Drawing.Point(23, 171);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(284, 46);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Limit Query";
            // 
            // checkBox_limit
            // 
            this.checkBox_limit.AutoSize = true;
            this.checkBox_limit.Location = new System.Drawing.Point(15, 20);
            this.checkBox_limit.Name = "checkBox_limit";
            this.checkBox_limit.Size = new System.Drawing.Size(77, 17);
            this.checkBox_limit.TabIndex = 0;
            this.checkBox_limit.Text = "Using Limit";
            this.checkBox_limit.UseVisualStyleBackColor = true;
            this.checkBox_limit.CheckedChanged += new System.EventHandler(this.CheckBoxLimitCheckedChanged);
            // 
            // textBox_limit
            // 
            this.textBox_limit.Location = new System.Drawing.Point(196, 18);
            this.textBox_limit.Name = "textBox_limit";
            this.textBox_limit.Size = new System.Drawing.Size(69, 20);
            this.textBox_limit.TabIndex = 1;
            // 
            // SettingMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(349, 281);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SettingMap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SettingMapp";
            this.Load += new System.EventHandler(this.SettingMapLoad);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox_namespace;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox_assembly;
        private System.Windows.Forms.CheckBox checkBox_overrites;
        private System.Windows.Forms.CheckBox checkBox_help;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.CheckBox checkBox_relation;
        private System.Windows.Forms.ComboBox MappType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBox_limit;
        private System.Windows.Forms.TextBox textBox_limit;
    }
}