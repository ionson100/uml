﻿using System;
using System.Data;
using System.Windows.Forms;
using MoverUML;
using uml.Properties;

//using System.Linq;

//using System.Data;

namespace uml
{
    public partial class EditRelation : Form,IEditRelation
    {
       DataRelation _rel;
        public EditRelation()
        {
            InitializeComponent();
           
        }

        private void EditRelationLoad(object sender, EventArgs e)
        {
            foreach (DataTable s in Singlton.DataSet.Tables)
            {
                comboBoxChildTable.Items.Add(s.TableName);
                comboBoxParentTable.Items.Add(s.TableName);
            }
            comboBoxChildTable.SelectedIndex=comboBoxChildTable.Items.IndexOf(_rel.ChildTable.TableName);
            comboBoxParentTable.SelectedIndex = comboBoxParentTable.Items.IndexOf(_rel.ParentTable.TableName);

            comboBoxChildColumn.SelectedIndex = comboBoxChildColumn.Items.IndexOf(_rel.ChildColumns[0].ColumnName);
            comboBoxParentColumn.SelectedIndex = comboBoxParentColumn.Items.IndexOf(_rel.ParentColumns[0].ColumnName);



            textBox1.Text = _rel.RelationName;


            try
            {
                var value = _rel.ExtendedProperties["OnDelete"].ToString();
                var ii = comboBox_Delete.Items.IndexOf(value.ToUpper());
                comboBox_Delete.SelectedIndex = ii != -1 ? ii : 0;
                var ii1 = comboBox_Update.Items.IndexOf(_rel.ExtendedProperties["OnUpdate"].ToString().ToUpper());
                comboBox_Update.SelectedIndex = ii1 != -1 ? ii1 : 0;

                button2.Enabled = false;
            }
            catch
            {
            }
            
        }
      

        private void Button2Click(object sender, EventArgs e)
        {
            //удaвление
            ////////////////////////////////////////////////
            if (MessageBox.Show(Resources.EditRelation_Button2Click_Confirm_action, Resources.EditRelation_Button2Click_Ahtung, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
            {
                DataRelation dr;
               

                Settings1.Default.OnDelete = comboBox_Delete.Text;
                Settings1.Default.OnUpdate = comboBox_Update.Text;
                var s1Parent = new DataColumn[1];
                var s2Child = new DataColumn[1];


                s1Parent[0] = Util.GetColumn(Singlton.DataSet, comboBoxParentTable.Text, comboBoxParentColumn.Text);
                s2Child[0] = Singlton.DataSet.Tables[comboBoxChildTable.Text].Columns[comboBoxChildColumn.Text];
                try
                {
                    dr = new DataRelation(textBox1.Text, s1Parent, s2Child);
                    dr.ExtendedProperties.Add("OnDelete", comboBox_Delete.Text);
                    dr.ExtendedProperties.Add("OnUpdate", comboBox_Update.Text);

                }
                catch (Exception ee)
                {
                    MessageBox.Show(Resources.EditRelation_Button2Click_Ошибка_создания_связи__ + ee.Message, Resources.EditRelation_Button2Click_Error,MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1, 0);
                    button2.DialogResult = DialogResult.None;

                    return;
                }
                try
                {
                    Singlton.DeleteRelationFromForm1.DeleteRelation();
                    Singlton.DataSet.Tables[comboBoxChildTable.Text].ParentRelations.Add(dr);

                    var m = new Mover(dr.ParentTable.TableName,
                              dr.ChildTable.TableName, dr.RelationName);
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message,Resources.EditRelation_Button2Click_Error,MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1, 0);
                  //  Singlton.DataSet.Tables[comboBoxChildTable.Text].ParentRelations.Remove(DR);
                }



                //Thread.Sleep(20);

                //Util.UpdatePanel_H();


            }  
            ////////////////////////////////////////////////////
        }



        #region IEditRelation Members

        public DataRelation Relation
        {
            get
            {
                return _rel;
            }
            set
            {
                _rel = value;
            }
        }

        #endregion

        private void ComboBoxChildTableSelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxChildColumn.Items.Clear();
            
            foreach (DataColumn c in Singlton.DataSet.Tables[((ComboBox)sender).Text].Columns)
            {
                comboBoxChildColumn.Items.Add(c.ColumnName);
            }
            button2.Enabled = true;
        }

        private void ComboBoxParentTableSelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxParentColumn.Items.Clear();

            foreach (DataColumn c in Singlton.DataSet.Tables[((ComboBox)sender).Text].Columns)
            {
                comboBoxParentColumn.Items.Add(c.ColumnName);
            }
            button2.Enabled = true;

        }

        private void TextBox1TextChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void ComboBoxDeleteSelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void ComboBoxUpdateSelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void ComboBoxChildColumnSelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

        private void ComboBoxParentColumnSelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
        }

       
    }
}
