﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using MySql.Data.MySqlClient;
using System.IO;


namespace uml
{
    public class MySQL : ISupplier,IDisposable
    {
        readonly string[] _word = File.ReadAllLines(Application.StartupPath + "/pattern/MySQL_Word.txt");
        //Fireball.Windows.Forms.CodeEditorControl tb;
        string _base="";
        MySqlDataAdapter _da;
        readonly MySqlConnection _connect = new MySqlConnection(Settings1.Default.ConString);
        readonly string _createtable;

        readonly string _showfields;

        string _showkey, _showtable, _droptable, _showdatabases;

        readonly string _addcolumn;

        string Change;

        readonly string _dropfield;

        readonly string _typecolumn;

        string _deleteColumn;

        readonly string _modify;

        readonly string _dropbase;

        string _showcomment;

        readonly string _addkey;
        readonly string _dropkey;

        public MySQL()//CodeEditorControl tb)
        {
           
            var doc = new XmlDocument();
            doc.Load(Application.StartupPath + "/pattern/MySQL_SQL.xml");
            var areal = doc.LastChild;
            var xmlNodeList = areal.SelectNodes("createtable");
            if (xmlNodeList != null) _createtable = xmlNodeList[0].InnerText;
            var selectNodes = areal.SelectNodes("showfields");
            if (selectNodes != null) _showfields = selectNodes[0].InnerText;
            var nodeList = areal.SelectNodes("showkey");
            if (nodeList != null) _showkey = nodeList[0].InnerText;
            var nodes = areal.SelectNodes("showtable");
            if (nodes != null) _showtable = nodes[0].InnerText;
            var list = areal.SelectNodes("droptable");
            if (list != null) _droptable = list[0].InnerText;
            var xmlNodeList1 = areal.SelectNodes("showdatabases");
            if (xmlNodeList1 != null)
                _showdatabases = xmlNodeList1[0].InnerText;
            var selectNodes1 = areal.SelectNodes("addcolumn");
            if (selectNodes1 != null) _addcolumn = selectNodes1[0].InnerText;
            var nodeList1 = areal.SelectNodes("change");
            if (nodeList1 != null) Change = nodeList1[0].InnerText;
            var nodes1 = areal.SelectNodes("dropfield");
            if (nodes1 != null) _dropfield = nodes1[0].InnerText;
            var list1 = areal.SelectNodes("typecolumn");
            if (list1 != null) _typecolumn = list1[0].InnerText;
            var xmlNodeList2 = areal.SelectNodes("deletecolumn");
            if (xmlNodeList2 != null)
                _deleteColumn = xmlNodeList2[0].InnerText;
            _modify = areal.SelectNodes("modify")[0].InnerText;
            _dropbase = areal.SelectNodes("dropbase")[0].InnerText;
            _showcomment = areal.SelectNodes("showcomment")[0].InnerText;
            _addkey = areal.SelectNodes("addkey")[0].InnerText;
            _dropkey = areal.SelectNodes("dropkey")[0].InnerText;
            
            
        }
        public DataTable ShowDataBase()
        {
            try
            {
                
                using (_da = new MySqlDataAdapter("SHOW DATABASES", _connect))
                {
                    var t = new DataTable();
                    _da.Fill(t);
                  Util.PrintSql("show databases");
                    return t;
                }
            }
            catch (Exception ee)
            { 
               Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);
               
            }
            
        }
        public DataTable ShowTablesFromBase(string bases)
        {
            try
            {
                using (_da = new MySqlDataAdapter(string.Format("show tables from `{0}`", bases), _connect))
                {
                    var t = new DataTable();
                   Util.PrintSql(string.Format("show tables from `{0}`",bases));
                    _da.Fill(t);
                    return t;
                }
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);
                
            }
        }
        public DataTable SelectFromTable(string table)
        {
            try
            {
              
                _connect.ConnectionString = ConString(Settings1.Default.ConString);
                using (_da = new MySqlDataAdapter(string.Format("select * from   `{0}` {1}", table,Util.Limit), _connect))
                {
                   
                  Util.PrintSql(string.Format("select * from   `{0}` {1}", table,Util.Limit));
                    var ds = new DataSet();
                    _da.FillSchema(ds, SchemaType.Mapped);
                    _da.Fill(ds);


                    var tt = ds.Tables[0].Copy();
                    tt.TableName = table;
                    return tt;
                }
            }
            catch (Exception ee)
            {
              Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);
                
            }
        }
        public string Base
        {
            get { return _base; }
            set { _base = value;
           
            }
        }

        public string ConnectString
        {
            get {return ConString(Settings1.Default.ConString); }
           
        }
        public string Dialect
        {
            get { return "NHibernate.Dialect.MySQLDialect"; }
        }
        private string ConString(string conString)
        {
            //Database=
            int i = conString.ToLower().IndexOf("database=", StringComparison.Ordinal);
            if (i == -1)
                throw new Exception("Check ConnectString of the join with base");

            var ee = conString.Split(';');
            var sb=new StringBuilder();
            foreach (var s in ee)
            {
                if(s.ToLower().IndexOf("database", StringComparison.Ordinal)!=-1)
                {
                    sb.Append("Database="+_base);
                }
                else
                {
                    sb.Append(s);
                }
                sb.Append(";");
            }

            return sb.ToString();
        }
        public void Updata(DataTable t)
        {
            
            StringBuilder builder=new StringBuilder();
            foreach (DataColumn column in t.Columns)
            {
                builder.Append(", ").Append("`").Append(column.ColumnName).Append("`");
            }

            string sql = "SELECT " + builder.ToString().Trim(',') + " FROM  `" + t.TableName + "`";
            using (_da = new MySqlDataAdapter(sql, _connect))
            {
                var cb = new MySqlCommandBuilder(_da);
                _da.Update(t);
            }
        }
        public DataTable ShowFields(string tablename)
        {
            try
            {
                _connect.ConnectionString = ConString(Settings1.Default.ConString);
                using (_da = new MySqlDataAdapter(string.Format("show fields from `{0}`",tablename), _connect))
                {

                    var t = new DataTable();
                    Util.PrintSql(string.Format("show fields from `{0}`", tablename));
                    _da.Fill(t);

                    return t;
                }
            }
            
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);
               
            }

        }
        public List<RelationObject> GetRelationObject(string tableName)
        {
            var l = new List<RelationObject>();
            _connect.ConnectionString=ConString(Settings1.Default.ConString);
            using (_da = new MySqlDataAdapter(string.Format("show create table `{0}`", tableName), _connect))
    {
            var t = new DataTable();
            Util.PrintSql(string.Format("show create table `{0}`",tableName));
            _da.Fill(t);
            string[] text = t.Rows[0][1].ToString().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var s in text)
            {
                if (s.IndexOf("CONSTRAINT", StringComparison.Ordinal) != -1)
                {
                   string onDelete;
                   string onUpdate;
                   var ww = s.Remove(0, s.IndexOf("FOREIGN KEY", StringComparison.Ordinal) + 11).
                        Substring(0, s.Remove(0, s.IndexOf("FOREIGN KEY", StringComparison.Ordinal) + 11).IndexOf("REFERENCES", StringComparison.Ordinal));
                    var name1 = ww.Remove(0, ww.IndexOf('`') + 1);
                    var colunmKey = name1.Substring(0, name1.IndexOf('`'));
                    var n1 = s.Remove(0, s.IndexOf("REFERENCES", StringComparison.Ordinal) + 10);
                    var n2 = n1.Remove(0, n1.IndexOf('`') + 1);
                    var tableKey = n2.Substring(0, n2.IndexOf('`'));
                    var n11 = s.Remove(0, s.IndexOf("REFERENCES", StringComparison.Ordinal) + 10);
                    var n22 = n11.Remove(0, n11.IndexOf('(') + 1);
                    var n33 = n22.Remove(0, n22.IndexOf('`') + 1);
                    var columnContrise = n33.Substring(0, n33.IndexOf('`'));
                    var v = s.Remove(0, s.IndexOf('`') + 1);
                    var id = v.Substring(0, v.IndexOf('`'));
                    if (s.ToUpper(Application.CurrentCulture).IndexOf("ON DELETE CASCADE", StringComparison.Ordinal) != -1)
                    {
                        onDelete = "CASCADE";
                    }
                    else if (s.ToUpper(Application.CurrentCulture).IndexOf("ON DELETE SET NULL", StringComparison.Ordinal) != -1)
                    {
                        onDelete = "SET NULL";
                    }
                    else
                    {
                        onDelete = "";
                    }
                    /////////////////////
                    if (s.ToUpper(Application.CurrentCulture).IndexOf("ON UPDATE CASCADE", StringComparison.Ordinal) != -1)
                    {
                        onUpdate = "CASCADE";
                    }
                    else if (s.ToUpper(Application.CurrentCulture).IndexOf("ON DELETE SET NULL", StringComparison.Ordinal) != -1)
                    {
                        onUpdate = "SET NULL";
                    }
                    else
                    {
                        onUpdate = "";
                    }
                    l.Add(new RelationObject(tableKey, tableName, colunmKey, columnContrise,onDelete,onUpdate,id));
                }
            }
            return l;
}

        }
        public void DrobForeignKey(string tableName, string id)
        {
            string sql = "alter table `"+_base+"`.`"+tableName+"` drop foreign key `"+id+"`";
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                {
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                  Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message, ee);


                }
                finally
                {
                    com.Connection.Close();
                 
                }
            }
            
           // ALTER TABLE `assa2`.`book_author` DROP FOREIGN KEY `FK_book_author_1`;
        }
        public void CreateForeiginKey(string tableNameChild,string tableNameParent,string columnNameChild,string columnNameParenr,string idRelation)
        {
            var onDelete = "";
            var onUpadate = "";
            if (Settings1.Default.OnUpdate != "")
                onUpadate = "on update " + Settings1.Default.OnUpdate.Trim().ToUpper();
            if (Settings1.Default.OnDelete != "")
                onDelete = " on delete " + Settings1.Default.OnDelete.Trim().ToUpper();
            var sql = "alter table `"+_base+"`.`"+tableNameChild+"` add constraint `"+idRelation+"` foreign key `"+idRelation+"` (`"+columnNameChild+@"`)
                        references `"+tableNameParent+"` (`"+columnNameParenr+"`) "+onDelete+" "+onUpadate;
            using (var com = new MySqlCommand(sql, _connect))
            {
                Util.PrintSql(sql);
                com.Connection.Open();
                try
                {
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message, ee);
                }
                finally
                {
                    com.Connection.Close();
                    Util.PrintSql(sql);
                }
            }
           
           
            
           //ALTER TABLE `assa3`.`t3` ADD CONSTRAINT `FK_t3_1` FOREIGN KEY `FK_t3_1` (`n`)
           // REFERENCES `t2` (`n`);
        }


        ////////////////////////
        public  string[] GetTypeColumn()
        {
            return _typecolumn.Split(';');
        }
        public DataTable ShowFieldsForEdit(String tableName)
        {
            try
            {
                _connect.ConnectionString = ConString(Settings1.Default.ConString);
                using (_da = new MySqlDataAdapter(_showfields.Replace("#name", tableName), _connect))
                {
                    var t = new DataTable();
                  Util.PrintSql(_showfields.Replace("#name", tableName));
                    _da.Fill(t);
                    return t;
                }
            }
            catch (Exception ee)
            {
                Util.PrintSql(ee.Message);
                throw new Exception(ee.Message, ee);
            }
        }
        public void DeletePrimaryKey(string tableName)
        {
            var sql = _dropkey.Replace("#table", tableName);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                {
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                   Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message, ee);

                }
                finally
                {
                    com.Connection.Close();
                 
                }
            }
            
        }
        public void AddPrimaryKey(string tableName, string columnnane)
        {
            var sql = _addkey.Replace("#table", tableName).Replace("#field", columnnane);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                { 
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                  
                }
            }
        }
        public void ModufyColumn(string table, string nameOld, string nameNew, string type, string defaults, string nulls, string comment)
        {
            var sql = _modify.Replace("#table", table).Replace("#nameold", nameOld).Replace("#namenew", nameNew).
                    Replace("#type", type).Replace("#default", defaults).Replace("#null", nulls).Replace("#comment", comment);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                {
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                    
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    System.Diagnostics.Debug.Write(ee.Message);
                   
                    //Util.PurposeColor(tb, Environment.NewLine + ee.Message);
                    throw new Exception(ee.Message,ee);
                    

                }
                finally
                {
                    com.Connection.Close();
                   
                }
            }
        }
        public void AddColumn(string table, string name, string type, string defaults, string nulls)
        {
            string sql = _addcolumn.Replace("#table", table).Replace("#name", name).
                    Replace("#type", type).Replace("#default", defaults).Replace("#null", nulls);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                { 
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                   Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                  
                }
            }
        }
        public void DeleteColumn(string table, string columnname)
        {
          var sql =_dropfield.Replace("#table", table).Replace("#name", columnname);
          using (var com = new MySqlCommand(sql, _connect))
          {
              com.Connection.Open();
              try
              { 
                  Util.PrintSql(sql);
                  com.ExecuteNonQuery();
              }
              catch (Exception ee)
              {
                  Util.PrintSql(ee.Message);
                  throw new Exception(ee.Message,ee);

              }
              finally
              {
                  com.Connection.Close();
                 
              }
          }
        }
        public void CreateBase(string basename, string patch)
        {
           // "CREATE DATABASE " + basename
            var sql = string.Format("create database `{0}`", basename);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                { 
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                    Util.PrintSql(sql);
                }
                catch (Exception ee)
                {
                   Util.PrintSql(ee.Message);
                   com.Connection.Close();
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                  
                }
            }
        }
        public void DeleteBase(string basename)
        {
            //dropbase.Replace("#name", basename)
            var sql = _dropbase.Replace("#name", basename);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                { 
                    Util.PrintSql(sql);
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                    
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                   
                }
            }
        }

        public void DeleteTable(string tableName)
        {
           // DROP TABLE aaaa
            var sql =String.Format( "drop table  `{0}`",tableName);
            using (var com = new MySqlCommand(sql, _connect))
            {
                com.Connection.Open();
                try
                { 
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                    Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                   
                }
            }
        }
        public void RenameRelation(DataRelation newRelation, string idOldRelation)
        {
           // string sql = dropfield.Replace("#table", table).Replace("#name", columnname);
            //MySql.Data.MySqlClient.MySqlCommand com = new MySql.Data.MySqlClient.MySqlCommand(sql, da.SelectCommand.Connection);
            //com.Connection.Open();
            //try
            //{
            //    com.ExecuteNonQuery();
            //}
            //catch (Exception ee)
            //{
            //    throw new Exception(ee.Message);

            //}
            //finally
            //{
            //    com.Connection.Close();
            //   Util.PrintSQLsql;
            //}
        }

        public void CreateTable(String tableName)
        {
            var sql = _createtable.Replace("#name", tableName);
            using (var com = new MySqlCommand(sql, new MySqlConnection(ConString(Settings1.Default.ConString))))
            {
                com.Connection.Open();
                try
                { 
                    Util.PrintSql(sql);
                    com.ExecuteNonQuery();
                }
                catch (Exception ee)
                {
                  Util.PrintSql(ee.Message);
                    throw new Exception(ee.Message,ee);

                }
                finally
                {
                    com.Connection.Close();
                  
                }
            }
        }
        public DataSet SelectFromTableForMap(string sql)
        {
            _connect.ConnectionString = ConString(Settings1.Default.ConString);
            using (var adapter = new MySqlDataAdapter(sql, _connect))
            {
                var dataSet = new DataSet();
                adapter.Fill(dataSet);
                adapter.FillSchema(dataSet, SchemaType.Mapped);
                return dataSet;
            }
        }

        public DataTable SqlExecute(string sql)
        {
            _connect.ConnectionString = ConString(Settings1.Default.ConString);
            using (_da = new MySqlDataAdapter(sql, _connect))
            {
                var t = new DataTable();
                Util.PrintSql(sql);
                _da.Fill(t);
                return t;
            }
        }
        public String[] Word
        {
            get {return _word; }
        }





        #region IDisposable Members

        public void Dispose()
        {
            if (_connect != null)
            {
                if (_connect.State != ConnectionState.Closed)
                    _connect.Close();
                _da.Dispose();
                _connect.Dispose();
            }
        }

        #endregion
       
    }
}
