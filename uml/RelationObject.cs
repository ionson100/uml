﻿using System;

namespace uml
{
    [Serializable]
  public  class RelationObject
    {
        string _tableNameParent;
        string _tableNameContrais;
        string _columnNameTableParent;
        string _columnNameTableChild;
        readonly string _id;
        readonly string _onDelete;
        readonly string _onUpdate;
        [NonSerialized]
        bool _active;
        public RelationObject()
            
        {
           
        }
        public RelationObject(string tableNameKey,
                              string tableNameContrais,
                              string columnNameTableKey, 
                              string columnNameTableContrains,
                              string onDelete,
                              string onUpadate,
                              string id)
        {
            
            _tableNameParent = tableNameKey;
            _tableNameContrais = tableNameContrais;
            _columnNameTableParent = columnNameTableKey;
            _columnNameTableChild = columnNameTableContrains;
            _id = id;
            _onDelete = onDelete;
            _onUpdate = onUpadate;

        }
        public string TableNameParent
        {
            get { return _tableNameParent; }
            set { _tableNameParent = value; }
        }
        public string TableNameChild
        {
            get { return _tableNameContrais; }
            set { _tableNameContrais = value; }
        }
        public string ColumnNameTableParen
        {
            get { return _columnNameTableParent; }
            set { _columnNameTableParent = value; }
        }
        public string ColumnNameTableChild
        {
            get { return _columnNameTableChild; }
            set { _columnNameTableChild = value; }
        }
        public string Id
        {
            get { return _id; }
        }
        public string OnDelete
        {
            get { return _onDelete; }
        }
        public string OnUpdate
        {
            get { return _onUpdate; }
        }
     
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
