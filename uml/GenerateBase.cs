﻿using System;
using System.Windows.Forms;
using uml.Properties;

namespace uml
{
    public partial class GenerateBase : Form
    {
        string _path;
        public GenerateBase()
        {
            InitializeComponent();
        }

        private void GenerateBaseLoad(object sender, EventArgs e)
        {
            panel1.Visible = (BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql;
        }

        public string BaseName
        {
            get { return textBox1.Text; }
        }
        public string PathBase
        {
            get { return folderBrowserDialog1.SelectedPath; }
        }

        private void Button3Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                _path = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Button2Click(object sender, EventArgs e)
        {
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql && _path == "")
            {
                MessageBox.Show(Resources.GenerateBase_Button2Click_Path_for_base_no_slect);
            }
          
            DialogResult = DialogResult.OK;
        }
    }
}
