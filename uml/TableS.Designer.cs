﻿namespace uml
{
    partial class TableS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.редактироватьТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьСвязьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relationCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel5.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(3, 188);
            this.panel1.TabIndex = 0;
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel1MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // panel2
            // 
            this.panel2.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(138, 3);
            this.panel2.TabIndex = 1;
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel2MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // panel3
            // 
            this.panel3.Cursor = System.Windows.Forms.Cursors.SizeWE;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(138, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 185);
            this.panel3.TabIndex = 2;
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel3MouseMove);
            this.panel3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // panel4
            // 
            this.panel4.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 185);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(135, 3);
            this.panel4.TabIndex = 3;
            this.panel4.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel4MouseMove);
            this.panel4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Wheat;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.ContextMenuStrip = this.contextMenuStrip1;
            this.panel5.Controls.Add(this.listBox1);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(135, 182);
            this.panel5.TabIndex = 4;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.редактироватьТаблицуToolStripMenuItem,
            this.создатьСвязьToolStripMenuItem,
            this.удалитьТаблицуToolStripMenuItem,
            this.relationCountToolStripMenuItem,
            this.cToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(201, 114);
            // 
            // редактироватьТаблицуToolStripMenuItem
            // 
            this.редактироватьТаблицуToolStripMenuItem.Image = global::uml.Properties.Resources.EditTableHS;
            this.редактироватьТаблицуToolStripMenuItem.Name = "редактироватьТаблицуToolStripMenuItem";
            this.редактироватьТаблицуToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.редактироватьТаблицуToolStripMenuItem.Text = "Edit table";
            this.редактироватьТаблицуToolStripMenuItem.Click += new System.EventHandler(this.РедактироватьТаблицуToolStripMenuItemClick);
            // 
            // создатьСвязьToolStripMenuItem
            // 
            this.создатьСвязьToolStripMenuItem.Image = global::uml.Properties.Resources.OrgChartHS;
            this.создатьСвязьToolStripMenuItem.Name = "создатьСвязьToolStripMenuItem";
            this.создатьСвязьToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.создатьСвязьToolStripMenuItem.Text = "Create relation";
            this.создатьСвязьToolStripMenuItem.Click += new System.EventHandler(this.СоздатьСвязьToolStripMenuItemClick);
            // 
            // удалитьТаблицуToolStripMenuItem
            // 
            this.удалитьТаблицуToolStripMenuItem.Image = global::uml.Properties.Resources.delete_16x;
            this.удалитьТаблицуToolStripMenuItem.Name = "удалитьТаблицуToolStripMenuItem";
            this.удалитьТаблицуToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.удалитьТаблицуToolStripMenuItem.Text = "Delete table (daigramm)";
            this.удалитьТаблицуToolStripMenuItem.Click += new System.EventHandler(this.УдалитьТаблицуToolStripMenuItemClick);
            // 
            // relationCountToolStripMenuItem
            // 
            this.relationCountToolStripMenuItem.Enabled = false;
            this.relationCountToolStripMenuItem.Name = "relationCountToolStripMenuItem";
            this.relationCountToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.relationCountToolStripMenuItem.Text = "ParentRelationsCount";
            this.relationCountToolStripMenuItem.Click += new System.EventHandler(this.RelationCountToolStripMenuItemClick);
            // 
            // cToolStripMenuItem
            // 
            this.cToolStripMenuItem.Enabled = false;
            this.cToolStripMenuItem.Name = "cToolStripMenuItem";
            this.cToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.cToolStripMenuItem.Text = "ChildRelationsCount";
            this.cToolStripMenuItem.Click += new System.EventHandler(this.CToolStripMenuItemClick);
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.Wheat;
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 20);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(133, 156);
            this.listBox1.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Khaki;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.pictureBox3);
            this.panel6.Controls.Add(this.pictureBox2);
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(133, 20);
            this.panel6.TabIndex = 0;
            this.panel6.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseMove);
            this.panel6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseDown);
            this.panel6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::uml.Properties.Resources.ProtectFormHS;
            this.pictureBox3.Location = new System.Drawing.Point(91, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(17, 15);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::uml.Properties.Resources.TableHS;
            this.pictureBox2.Location = new System.Drawing.Point(3, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 17);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseMove);
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseDown);
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::uml.Properties.Resources.Collapse_large;
            this.pictureBox1.Location = new System.Drawing.Point(114, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(15, 14);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "1";
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseMove);
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseDown);
            this.label1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel6MouseUp);
            // 
            // TableS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "TableS";
            this.Size = new System.Drawing.Size(141, 188);
            this.Load += new System.EventHandler(this.TableSLoad);
            this.panel5.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem создатьСвязьToolStripMenuItem;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ToolStripMenuItem редактироватьТаблицуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьТаблицуToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ToolStripMenuItem relationCountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cToolStripMenuItem;
    }
}
