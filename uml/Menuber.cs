﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace uml
{
    class Menuber
    {
        ISupplier _ii;
        private ToolStripButton _buttonAdd;
        private ToolStripButton _buttonEdit;
        private ToolStripButton _buttonDeleteOn;
        private ToolStripButton _buttonEditDeleteAll;
        private ToolStripComboBox _columnBox;
        private ToolStripTextBox _textBoxOne;
        private ToolStripTextBox _textBoxAll;
        private DataGridView _gridViewData1;

        public ToolStripButton ButtonAdd
        {
          
            set
            {
                _buttonAdd = value;
                _buttonAdd.Click += ButtonAddClick;
            }
        }

        public ToolStripButton ButtonEdit
        {
            set
            {
                _buttonEdit = value;
                _buttonEdit.Click += ButtonEditClick;
            }
        }

        public ToolStripButton ButtonDeleteOn
        {
            set
            {
                _buttonDeleteOn = value;
                _buttonDeleteOn.Click += ButtonDeleteOneClick;
            }
        }

        public ToolStripButton ButtonEditDeleteAll
        {
            set
            {
                _buttonEditDeleteAll = value;
                _buttonEditDeleteAll.Click += ButtonDeleteAllClickE;
            }
        }

        public ToolStripComboBox ColumnBox
        {
            set
            {
                _columnBox = value;
                _columnBox.Click += searchcolumns_Click;
            }
        }

        public ToolStripTextBox TextBoxAll
        {
            set
            {
                _textBoxAll = value;
            }
        }

        public ToolStripTextBox TextBoxOne
        {
            set
            {
                _textBoxOne = value;
            }
        }

        public DataGridView GridViewData1
        {
            set
            {
                _gridViewData1 = value;
                _gridViewData1.SelectionChanged += (o, args) =>
                {
                    _buttonEdit.Enabled = true;
                };
            }
        }

        public ISupplier Ii
        {
            set
            {
                _ii = value;
            }
        }

        public void PrewShow()
        {
            
        }

        private void ButtonAddClick(object sender, EventArgs e)
        {
            if (_gridViewData1.DataSource == null) return;
            DataTable dataTable = (DataTable)_gridViewData1.DataSource;
            object[] dd = new object[dataTable.Columns.Count];
            int i = 0;
            foreach (DataColumn column in dataTable.Columns)
            {
                dd[i] = GetDefaultValue(column.DataType);
                i++;
            }
            dataTable.Rows.Add(dd);
            DataRow row = dataTable.Rows[dataTable.Rows.Count - 1];
            FormAddRow addRow = new FormAddRow(row, FormAddRow.ActionEnum.add);
            if (addRow.ShowDialog() == DialogResult.OK)
            {
                var t1 = (DataTable)_gridViewData1.DataSource;
                t1.TableName = (string)_gridViewData1.Tag;
                _ii.Updata(t1);
            }
            else
            {
                var t1 = (DataTable)_gridViewData1.DataSource;
                t1.Rows.RemoveAt(t1.Rows.Count - 1);
            }
        }

        object GetDefaultValue(Type t)
        {
            if (t.IsValueType)
                return Activator.CreateInstance(t);

            return null;
        }

        private void ButtonEditClick(object sender, EventArgs e)
        {
            if (_gridViewData1.SelectedRows.Count == 0) return;
            int index = _gridViewData1.SelectedRows[0].Index;
            DataRow row = ((DataTable)_gridViewData1.DataSource).Rows[index];
            DataTable dataTable = (DataTable)_gridViewData1.DataSource;

            FormAddRow addRow = new FormAddRow(row, FormAddRow.ActionEnum.edit);
            if (addRow.ShowDialog() == DialogResult.OK)
            {

                _ii.Updata(dataTable);
            }
        }

        private void ButtonDeleteOneClick(object sender, EventArgs e)
        {
            if (_gridViewData1.SelectedRows.Count == 0) return;

            if (MessageBox.Show("Удалить выбранные строки", "Удаление строк", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                List<int> indexs = (from DataGridViewBand selectedRow in _gridViewData1.SelectedRows select selectedRow.Index).ToList();
                var t1 = (DataTable)_gridViewData1.DataSource;
                t1.TableName = (string)_gridViewData1.Tag;

                indexs.Reverse();
                foreach (var i in indexs)
                {
                    t1.Rows[i].Delete();
                }

                _ii.Updata(t1);
            }
        }


        private void ButtonDeleteAllClickE(object sender, EventArgs e)
        {
            if (_gridViewData1.DataSource == null) return;

            if (MessageBox.Show("Удалить все строки ", "Удаление строк", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {

                var t1 = (DataTable)_gridViewData1.DataSource;
                t1.TableName = (string)_gridViewData1.Tag;
                List<DataRow> rows = new List<DataRow>(t1.Rows.Count);


                for (var i = t1.Rows.Count - 1; i >= 0; i--)
                {
                    rows.Add(t1.Rows[i]);
                    t1.Rows[i].Delete();
                }
                _ii.Updata(t1);



            }
        }


        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_gridViewData1.DataSource == null) return;
            var columnName = (string)_columnBox.SelectedItem;
            var text = ((ToolStripTextBox)sender).Text;
            var dataTable = _ii.SelectFromTable((string)_gridViewData1.Tag);
            if (text.Length == 0)
            {
                _gridViewData1.DataSource = dataTable;
                return;
            }
            Type type = null;
            foreach (DataColumn column in dataTable.Columns)
            {
                if (column.ColumnName == columnName)
                {
                    type = column.DataType;
                    break;
                }
            }
            if (type == null) return;
            string dd;
            if (
                type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(double) ||
                type == typeof(float))
            {
                dd = "" + columnName + " = " + text;
            }
            else
            {
                dd = "" + columnName + " = '" + text + "'";
            }
            var table = _ii.SelectFromTable((string)_gridViewData1.Tag);
            try
            {
                var selectRows = dataTable.Select(dd);
                if (selectRows.Length == 0)
                {
                    for (var i = table.Rows.Count - 1; i >= 0; i--)
                    {
                        table.Rows.RemoveAt(i);
                    }

                    _gridViewData1.DataSource = table;
                    return;
                }


                List<DataRow> dataRows = new List<DataRow>();
                foreach (var selectRow in selectRows)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        if (CompareRows(selectRow, row)) continue;
                        dataRows.Add(row);
                    }
                }

                foreach (var dataRow in dataRows)
                {
                    table.Rows.Remove(dataRow);
                }

                _gridViewData1.DataSource = table;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private bool CompareRows(DataRow row1, DataRow row2)
        {
            if (row1.Table.TableName != row2.Table.TableName)
            {
                return false;
            }
            if (row1.ItemArray.Length != row2.ItemArray.Length)
            {
                return false;
            }
            for (var i = 0; i < row1.ItemArray.Length; i++)
            {
                if (row1.ItemArray[i].ToString() != row2.ItemArray[i].ToString())
                {
                    return false;
                }
            }
            return true;
        }

        private void searchcolumns_Click(object sender, EventArgs e)
        {
            TextBoxTextClear();
        }

        public void TextBoxTextClear()
        {
            this._textBoxOne.TextChanged -= this.searchTextBox_TextChanged;
            _textBoxOne.Text = "";
            this._textBoxOne.TextChanged += this.searchTextBox_TextChanged;
        }

        public void Hide()
        {
            //_buttonAdd.Enabled = false;
            _buttonEdit.Enabled = true;
            //_buttonEditDeleteAll.Enabled = false;
            //   _buttonDeleteOn.Enabled = dataTable.Rows.Count > 0;
            //datamenudeleteone.Enabled = false;
        }
    }
}
