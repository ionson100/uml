﻿using System;
using System.Windows.Forms;
using uml.Properties;

namespace uml
{
    public partial class AddTable : Form
    {
        public AddTable()
        {
            InitializeComponent();
        }

        private void AddTableLoad(object sender, EventArgs e)
        {
            if(Util.Constuctor==false)
            label3.Text = Resources.AddTable_AddTableLoad_ + Util.Ii.Base;
            else
            label3.Text = Resources.AddTable_AddTableLoad_Add_table__;
            comboBox1.SelectedIndex = 0;
            if ((BaseType) Settings1.Default.ProviderEnum != BaseType.Mssql) return;
            label2.Visible = false;
            comboBox1.Visible = false;
            textBox1.Focus();
        }

        public string TableNAme { get; private set; }

        public string TypeTable { get; private set; }

        private void Button1Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text.Trim()))
            {
                DialogResult = DialogResult.None;
                MessageBox.Show(Resources.AddTable_Button1Click_No_Tablename_,Resources.AddTable_Button1Click_Error,MessageBoxButtons.OK,MessageBoxIcon.Warning,MessageBoxDefaultButton.Button1, 0);
                return;
            }
            int r;
            if (Int32.TryParse(textBox1.Text, out r))
            {
                DialogResult = DialogResult.None;
                MessageBox.Show(Resources.AddTable_Button1Click_Bad_Tablename_, Resources.AddTable_Button1Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, 0);
                return;
            }
            DialogResult = DialogResult.OK;
            TableNAme = textBox1.Text;
            TypeTable = comboBox1.Text;
            Close();

        }
    }
}
