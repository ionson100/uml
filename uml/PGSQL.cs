﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Data;
using Fireball.Windows.Forms;

namespace uml
{
    class PGSQL:ISupplier,IDisposable
    {
        Npgsql.NpgsqlDataAdapter da;
        string Createtable, Showfields, Showkey, Showtable, Droptable, Showdatabases, Addcolumn,
              Change, Dropfield, Typecolumn, Deletecolumn, Modify, Dropbase, Showcomment, dialect, Createdatabase, createForeiginKey,
              Addkey, Dropkey, drobForeignKey,showTablesFromBase, constr;
        public PGSQL()
        {
          
            XmlDocument doc = new XmlDocument();
            doc.Load(Application.StartupPath + "/pattern/PGSQL_SQL.xml");
            XmlNode areal = doc.LastChild;
            Createtable = areal.SelectNodes("createtable")[0].InnerText;
            Showfields = areal.SelectNodes("showfields")[0].InnerText;
            Showkey = areal.SelectNodes("showkey")[0].InnerText;
            Showtable = areal.SelectNodes("showtable")[0].InnerText;
            Droptable = areal.SelectNodes("droptable")[0].InnerText;
            Showdatabases = areal.SelectNodes("showdatabases")[0].InnerText;
            Addcolumn = areal.SelectNodes("addcolumn")[0].InnerText;
            Change = areal.SelectNodes("change")[0].InnerText;
            Dropfield = areal.SelectNodes("dropfield")[0].InnerText;
            Typecolumn = areal.SelectNodes("typecolumn")[0].InnerText;
            Deletecolumn = areal.SelectNodes("deletecolumn")[0].InnerText;
            Modify = areal.SelectNodes("modify")[0].InnerText;
            Dropbase = areal.SelectNodes("dropbase")[0].InnerText;
            Showcomment = areal.SelectNodes("showcomment")[0].InnerText;
            Addkey = areal.SelectNodes("addkey")[0].InnerText;
            Dropkey = areal.SelectNodes("dropkey")[0].InnerText;
            constr = areal.SelectNodes("constr")[0].InnerText;
            dialect = areal.SelectNodes("dialect")[0].InnerText;
            Createdatabase = areal.SelectNodes("createdatabase")[0].InnerText;
            createForeiginKey = areal.SelectNodes("createForeiginKey")[0].InnerText;
            drobForeignKey = areal.SelectNodes("drobForeignKey")[0].InnerText;
            showTablesFromBase = areal.SelectNodes("showTablesFromBase")[0].InnerText;
            
        }

        #region ISupplier Members

        public System.Data.DataTable ShowDataBase()
        {
            //"Server=127.0.0.1;Port=5432;User Id=ion100;Password=312873;Database=ionson
            List<string> L = new List<string>();
            L.AddRange(Settings1.Default.ConString.Split(';'));
            string datatbase = "";
            L.ForEach(s => { if (s.IndexOf("Database") != -1)datatbase = s.Split('=')[1]; });
            DataTable t = new DataTable();
            t.Columns.Add(new DataColumn("Name"));
            t.Rows.Add(datatbase);
            return t;
            
        }

        public System.Data.DataTable ShowTablesFromBase(string bases)
        {
           

            da = new Npgsql.NpgsqlDataAdapter(showTablesFromBase, Settings1.Default.ConString);
            DataTable t = new DataTable();

            Util.PrintSql(Showdatabases);
            da.Fill(t);
            Util.PrintSql(showTablesFromBase);
            return t;
            
            
        }

        public System.Data.DataTable SelectFromTable(string table)
        { 
            DataSet ds = new DataSet();
            da = new Npgsql.NpgsqlDataAdapter("select * from " + table + "; SELECT * FROM "+table +" where 2=4" , Settings1.Default.ConString);
            
            ds.Tables.Add(new DataTable());
            da.Fill(ds);
            da.FillSchema(ds,SchemaType.Mapped);
            Util.PrintSql(showTablesFromBase);
            return ds.Tables[1];
            
        }

        public string Base
        {
            get
            {
                List<string> L = new List<string>();
                L.AddRange(Settings1.Default.ConString.Split(';'));
                string datatbase = "";
                L.ForEach(s => { if (s.IndexOf("Database") != -1)datatbase = s.Split('=')[1]; });
                return datatbase;
            }
            set
            {
                //throw new NotImplementedException();
            }
        }

        public string ConnectString
        {
            get { throw new NotImplementedException(); }
        }

        public string Dialect
        {
            get { throw new NotImplementedException(); }
        }

        public void Updata(System.Data.DataTable t)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable ShowFields(string tablename)
        {
            throw new NotImplementedException();
        }

        public List<RelationObject> GetRelationObject(string tableName)
        {
            throw new NotImplementedException();
        }

        public void DrobForeignKey(string tableName, string id)
        {
            throw new NotImplementedException();
        }

        public void CreateForeiginKey(string tableNameChild, string tableNameParent, string columnNameChild, string columnNameParenr, string idRelation)
        {
            throw new NotImplementedException();
        }

        public string[] GetTypeColumn()
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable ShowFieldsForEdit(string tableName)
        {
            throw new NotImplementedException();
        }

        public void DeletePrimaryKey(string tableName)
        {
            throw new NotImplementedException();
        }

        public void AddPrimaryKey(string tableName, string columnnane)
        {
            throw new NotImplementedException();
        }

        public void ModufyColumn(string table, string nameOld, string nameNew, string type, string defaults, string nulls, string comment)
        {
            throw new NotImplementedException();
        }

        public void AddColumn(string table, string name, string type, string defaults, string nulls)
        {
            throw new NotImplementedException();
        }

        public void DeleteColumn(string table, string columnname)
        {
            throw new NotImplementedException();
        }

        public void CreateBase(string basename, string patch)
        {
            throw new NotImplementedException();
           
        }

        public void DeleteBase(string basename)
        {
            throw new NotImplementedException();
        }
        public void RenameRelation(DataRelation newRelation, string idOldRelation)
        {
            throw new NotImplementedException();
        }
        public void CreateTable(String tableName)
        {
        }

        public DataSet SelectFromTableForMap(string sql)
        {
            throw new NotImplementedException();
        }
        public void DeleteTable(string tableName)
        {
            throw new NotImplementedException();
        }
        public DataTable SqlExecute(string sql)
        {
            throw new NotImplementedException();
        }
        public String[] Word
        {
            get { return null; }
        }
        

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
