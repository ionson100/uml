﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace uml
{
    
    public partial class FormAddRow : Form
    {
        ActionEnum _actionEnum;
         public enum ActionEnum
         {
            add=0,edit=1
        
         }
     

        private DataRow DataRow;

        object[] clone;
        public FormAddRow(DataRow row,ActionEnum actionEnum)
        {
            InitializeComponent();
            DataRow = row;
            _actionEnum = actionEnum;
            if (_actionEnum == ActionEnum.add)
            {
                buttonSave.Text = "Добавить";
            }
            else
            {
                buttonSave.Text = "Изменить";
            }
            clone = (object[]) DataRow.ItemArray.Clone();
        }

        private void FormAddRow_Load(object sender, EventArgs e)
        {
            int top = 0;
            DataTable dataTable = DataRow.Table;
            object[] r = DataRow.ItemArray;
            for (int i = 0; i <r.Length; i++)
            {
                DataColumn d = dataTable.Columns[i];
                string name = d.ColumnName;
                object value = r[i];
                if (d.DataType == typeof (byte[]))
                {
                    RowsEditorImage editor = new RowsEditorImage(name, value, DataRow, dataTable.Columns[i].AllowDBNull,
                        d.DataType);
                    editor.Top = top;
                    editor.Tag = d.DataType;
                    top = +top + 30;
                    panelBase.Controls.Add(editor);
                }
                else
                {
                   RowEditor editor = new RowEditor(name, value, DataRow, dataTable.Columns[i].AllowDBNull,d.DataType)
                   {
                     Top = top,
                     Tag = d.DataType
                    };
                    top = +top + 30;
                    panelBase.Controls.Add(editor);
                }
              
               

            }
           
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            for (var i = 0; i < panelBase.Controls.Count; i++)
            {
                DataRow[i]=((MyUserEditor) panelBase.Controls[i]).GetObject();
            }
            DialogResult=DialogResult.OK;
        }
    }
}
