﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using MoverUML;
using uml.Properties;
//using System.Linq;

namespace uml
{

    public partial class EditTabl : Form, IEditTable
    {
        DataTable _tables;
        DataTable _cols;
        string _tableName;
        public EditTabl()
        {
            InitializeComponent();
          
        }

        private void EditTablLoad(object sender, EventArgs e)
        {
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
            {
                DG1.Columns[0].Visible = false;
            }
            _tables = Table;
            Text = Resources.EditTabl_EditTablLoad_EditTable__ + _tables.TableName;
            _tableName = _tables.TableName;
            _cols = Util.Ii.ShowFieldsForEdit(_tables.TableName);
            
            //textBox1.Text = Table.TableName;
          
          //foreach(DataColumn col in Tables.Columns)
          //{
          //    DG.Rows.Add(++i, col.ColumnName, col.DataType.Name,null,false, col.AllowDBNull,
          //        col.AutoIncrement, col.DefaultValue.ToString(),false);
          //}
            var i = 0;
            foreach (DataRow R in _cols.Rows)
            {
                DG1.Columns[4].Visible = false;
                DG1.Columns[0].Visible = false;
                int r = DG1.Rows.Add();
                DG1.Rows[r].Cells[0].Value = i;
                DG1.Rows[r].Cells[1].Value = R[0].ToString();
                var fff = Util.Ii.GetTypeColumn();
                ((DataGridViewComboBoxCell)DG1.Rows[r].Cells[2]).Items.Clear();
                foreach (var s in fff)
                {
                    ((DataGridViewComboBoxCell)DG1.Rows[r].Cells[2]).Items.Add(s.Replace("\r", "").Replace("\n", "").Trim());
                }

                var value = "INT";
                var size = "";
                var unsignet = false;
                if(Util.UsingConstructor)
                {
                    DG1.Rows[r].Cells[2].Value = R["Type"].ToString();//
                }
                else
                {
                    parse(R[1].ToString(), ref size, ref  unsignet, ref value);
                    DG1.Rows[r].Cells[2].Value = value;
                }
               
                DG1.Rows[r].Cells[4].Value = unsignet;
                DG1.Rows[r].Cells[5].Value = R[3].ToString().Trim().ToUpper() == "NO";


                DG1.Rows[r].Cells[7].Value = R[5].ToString();//default 
              //  string ddd = R[6].ToString().Trim();
                DG1.Rows[r].Cells[6].Value = R[6].ToString().Trim() == "auto_increment";
                DG1.Rows[r].Cells[3].Value = size;//размер
                DG1.Rows[r].Cells[8].Value = R[8].ToString().Trim();//comment

                DG1.Rows[r].Cells[9].Value = R[4].ToString().Trim() == "PRI";
                i++;
            }

        }

        #region IEditTable Members

      public  DataTable Table
        {
            get
            {
                return _tables;
            }
            set
            {
                _tables = value;
            }
        }

        #endregion

      private void Button1Click(object sender, EventArgs e)
      {
          //try{

          #region ssssssssssssssssssssssss
          int i = 0;
          var l = new List<String>();
          DataTable ttt = Util.Ii.ShowFieldsForEdit(_tableName);

          try
          {
              if (ttt == null) return;

              foreach (DataGridViewRow r in DG1.Rows)
              {
                  try
                  {
                      if (r.Cells[1].Value == null) continue;
                      var sb = new StringBuilder("");
                      //sb.Append(r.Cells[0].Value.ToString() + ";");//число
                      sb.Append(r.Cells[1].Value + ";");//имя

                      sb.Append(r.Cells[2].Value + "");//тип
                      if (r.Cells[3].Value.ToString().Trim() != "")
                          sb.Append("(" + r.Cells[3].Value + ")");//тип

                      if (r.Cells[4].Value.ToString() == "True")
                          sb.Append(" unsigned");//тип

                      sb.Append(";");

                      sb.Append(r.Cells[5].Value.ToString() == "True" ? "NO;" : "YES;");
                      sb.Append(r.Cells[7].Value + ";");//default
                      sb.Append(r.Cells[6].Value.ToString() == "True" ? "auto_increment;" : ";");

                      sb.Append(r.Cells[8].Value + ";");//coment

                      sb.Append(r.Cells[9].Value.ToString() == "True" ? "PRI" : "");
                      l.Add(sb.ToString());
                      i++;
                  }
                  catch
                  {
                  }

              }
          }
          catch (Exception ee)
          {
              MessageBox.Show(ee.Message, Resources.EditTabl_button1_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
              return;
          }
          i = 0; var del = 0; /*string mess;*/

          foreach (string s in l)
          {
              try
              {
                  var dd = new StringBuilder(
                     ttt.Rows[i][0] + ";" //name 0
                      + ttt.Rows[i][1] + ";" +//tupe 1
                      ttt.Rows[i][3] + ";"//null 2
                      + ttt.Rows[i][5] + ";" //default 3
                      + ttt.Rows[i][6] + ";"// Auto 4

                      + ttt.Rows[i][8] + ";"//commrnt 5
                      + ttt.Rows[i][4]);//prim

                  if (s == dd.ToString())
                  {

                      del++;
                  }
                  else
                  {
                      var ss = s.Split(';');
                      ////////////////////////////////////////////////////////// prim
                      if (ss[6] == "PRI" && ttt.Rows[i][4].ToString().ToUpper() != "PRI")
                      {
                          //ADD
                          Util.Ii.AddPrimaryKey(_tableName, ss[0]);
                          
                      }
                      if (ss[6] != "PRI" && ttt.Rows[i][4].ToString().ToUpper() == "PRI")
                      {
                          //DELETE
                          Util.Ii.DeletePrimaryKey(_tableName);
                          
                      }
                      ///////////////////////////////////////////////////////////
                      string defa = "", auto = "";
                      string nulls = "NULL";
                      if (ss[3].Trim() != "")
                          defa = "DEFAULT \"" + ss[3] + "\"";
                      if (ss[2].Trim() != "YES")
                          nulls = "NOT NULL";
                      if (ss[4].ToLower() == "auto_increment")
                          auto = " auto_increment";
                      if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql&&Util.UsingConstructor==false)
                      {
                          if (ttt.Rows[i][0].ToString().ToLower() != ss[0].ToLower())
                          {
                              var sql = "EXEC sp_rename '" + _tableName + "." + ttt.Rows[i][0] + "', '" + ss[0] + "', 'COLUMN'";
                              var con = new System.Data.SqlClient.SqlCommand(sql,
                                  new System.Data.SqlClient.SqlConnection(Settings1.Default.ConString));
                              
                              try
                              {
                                  con.Connection.Open();
                                  con.ExecuteNonQuery();
                                  ttt.Rows[i][0]= ss[0];
                                  Util.PrintSql(sql);
                              }
                              catch (Exception ee)
                              {
                                  Util.PrintSql(ee.Message);
                                  MessageBox.Show(ee.Message, Resources.EditTabl_button1_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                                  return;
                                  
                              }
                              finally
                              {
                                  con.Connection.Close();
                              }
                              
                          }
                      }
                      if (Util.UsingConstructor == false)
                      {
                          if ((BaseType)Settings1.Default.ProviderEnum == BaseType.MySql)
                          {
                              Util.Ii.ModufyColumn(_tableName, ttt.Rows[i][0].ToString(), ss[0], ss[1] + auto, defa, nulls, ss[5]);//comm
                          }
                          else
                          {
                              if (ttt.Rows[i][1].ToString().ToLower() != ss[1].ToLower())
                              {
                                  if (ss[1].IndexOf("IDENTITY", StringComparison.Ordinal) != -1)
                                      ss[1] = ss[1].Remove(ss[1].IndexOf("IDENTITY", StringComparison.Ordinal), 8);
                                  Util.Ii.ModufyColumn(_tableName, ttt.Rows[i][0].ToString(), ss[0], ss[1], defa, nulls, ss[5]);//comm
                              }
                          }
                      }
                      else
                      {

                          Util.Ii.ModufyColumn(_tableName, ttt.Rows[i][0].ToString(), ss[0], ss[1], defa, nulls, ss[5]);//comm
                      }

                          if (Util.UsingConstructor == false)
                          {
                              DG1.Rows.Clear();
                              EditTablLoad(null, null);
                          }
                          //action(TableName, null);
                  }
              }
              catch(Exception ee)
              {//добавление новых

                

                  if (ee.GetBaseException().GetType().Name == "SqlException"|ee.GetBaseException().GetType().Name == "MySqlException")
                       {
                           MessageBox.Show(ee.Message, Resources.EditTabl_button1_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                      return;
                  }
                
                  try
                  {
                      var ss = s.Split(';');

                      var defa = "";
                      var nulls = "NULL";

                      if (ss[3].Trim() != "")
                          defa = "DEFAULT " + ss[3];
                      if (ss[2].Trim() != "YES")
                          nulls = "NOT NULL";

                      Util.Ii.AddColumn(_tableName, ss[0], ss[1], defa, nulls);

                      DG1.Rows.Clear();
                      EditTablLoad(null, null);
                  }
                  catch (Exception eee)
                  {
                      MessageBox.Show(eee.Message, Resources.EditTabl_button1_Click_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                      return;
                  }
                  // action(TableName, null);
              }
              i++;
          }
          if (i == del)
          {
              MessageBox.Show(Resources.EditTabl_button1_Click_No_New_Data);
          }


          if (Util.UsingConstructor == false)
          {
              Util.UpdatePanelH();
          }
          #endregion




          DialogResult = DialogResult.OK;
        
      }
      private void parse(string typ, ref string size, ref bool unsignet, ref string value)
      {
          if (size == null) throw new ArgumentNullException("size");
          if (typ.ToUpper().IndexOf('(') == -1)
          {
              value = typ.ToUpper().Trim();
              size = "";
              unsignet = false;
          }
          else
          {
              if (typ.ToUpper(Application.CurrentCulture).Trim().IndexOf("UNSIGNED", StringComparison.Ordinal) != -1)
                  unsignet = true;
              string dd = typ.Substring(0, typ.IndexOf('(')).ToUpper();
              value = dd;
              size = typ.Remove(0, typ.IndexOf('(') + 1).Substring(0, typ.Remove(0, typ.IndexOf('(') + 1).IndexOf(')'));
          }
      }

        private void Dg1DataError(object sender, DataGridViewDataErrorEventArgs e)
      {
          e.Cancel = true;
      }

      private void Dg1RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
      {
          if ((Int32?) e.Row.Cells[0].Value != null) return;
          e.Row.Cells[3].Value = "";
          e.Row.Cells[4].Value = false;
          e.Row.Cells[5].Value = false;
          e.Row.Cells[6].Value = false;
          e.Row.Cells[7].Value = "";
          e.Row.Cells[8].Value = "";
          e.Row.Cells[9].Value = false;
          ((DataGridViewComboBoxCell)e.Row.Cells[2]).Items.Clear();
          var fff = Util.Ii.GetTypeColumn();
          foreach (var s in fff)
          {
              ((DataGridViewComboBoxCell)e.Row.Cells[2]).Items.Add(s.Replace("\r", "").Replace("\n", "").Trim());
          }
      }

      private void DeleteColumnToolStripMenuItemClick(object sender, EventArgs e)
      {
          if (DG1.SelectedRows.Count != 0)
          {
              if (MessageBox.Show(Resources.EditTabl_DeleteColumnToolStripMenuItemClick_Delete_column__ + DG1.SelectedRows[0].Cells[1].Value, Resources.EditTabl_DeleteColumnToolStripMenuItemClick_Delete_column, MessageBoxButtons.OKCancel) == DialogResult.OK)
              {
                  Util.Ii.DeleteColumn(_tableName, DG1.SelectedRows[0].Cells[1].Value.ToString());
                
              }
          }
          DG1.Rows.Clear();
         EditTablLoad(null, null);
         //action(TableName, null);
      }

     
    }
}
