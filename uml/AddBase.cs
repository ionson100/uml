﻿using System;
using System.Windows.Forms;
using uml.Properties;

namespace uml
{
    public partial class AddBase : Form
    {
        string _patch;
        public AddBase()
        {
            InitializeComponent();
        }

        private void TextBox1TextChanged(object sender, EventArgs e)
        {

        }

        private void Button2Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text)) return;
            if (_patch == null && (BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
            {
                MessageBox.Show(Resources.AddBase_Button2Click_No_Path_from_base);
                return;
            }
            DialogResult = DialogResult.OK;
        }
        public string NameBase
        {
            get { return textBox1.Text; }
        }
        public string PathBase
        {
            get { return _patch; }
        }


        private void AddBaseLoad(object sender, EventArgs e)
        {
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
                linkLabel1.Visible = true;
        }

        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!string.IsNullOrEmpty(Settings1.Default.SelectedPath_MSSQL_BASE))
                folderBrowserDialog1.SelectedPath = Settings1.Default.SelectedPath_MSSQL_BASE;
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                _patch = folderBrowserDialog1.SelectedPath;
            }
        }
    }
}
