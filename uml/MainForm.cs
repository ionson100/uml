﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MoverUML;
using System.Xml;
using System.Diagnostics;
using Fireball.CodeEditor.SyntaxFiles;
using uml.Properties;

namespace uml
{
    public partial class MainForm : Form
    {
        private readonly Menuber _menuber=new Menuber();
        private ISupplier _suppler;
        private Cursor _cur;
        private bool _dragDropTreeView;
   

        public MainForm()
        {
            InitializeComponent();
           
        }

        private void Form1Load(object sender, EventArgs e)

        {
            GridView_Data.ReadOnly = true;
            tabControl1.SelectTab(Settings1.Default.SelectTab); 
            splitter1.SplitPosition = Settings1.Default.SplitY;
            splitter3.SplitPosition = Settings1.Default.SplitX;
            splitter2.SplitPosition = Settings1.Default.SplitX2;
            _cur = new Cursor(AppDomain.CurrentDomain.BaseDirectory+ "assa23.cur");
            Util.EditControl = CodeEditControl;
           
            Util.EditControl.Document.Text = string.Empty;
           
           
               // Очитска панели при перезагрузкеZZZ
            try
            {
                    Singlton.DataSet.Relations.CollectionChanged -= RelationsCollectionChanged;
                    if (Singlton.ControlP!=null)
                    {
                        Singlton.ControlP.Controls.Clear();
                        Singlton.IncrementForControlName = 0;
                        ToolGrouMoveEnable(false);
                        Singlton.DataSet = new DataSet();
                    }
                
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message,Resources.Head_Form1Load_Error,MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1, 0);
                return;
            }

            try
            {
                treeView.Nodes.Clear();
                dataGridViewEXE.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
                if (string.IsNullOrEmpty(Settings1.Default.path))
                {
                    Settings1.Default.path = Application.StartupPath + "/DataMap";
                    Settings1.Default.Save();
                }
                Util.PanelH = panel_H;
                Util.FormMainForm = this;
                Singlton.ControlP = panel_H;
                Singlton.Init(this);
                if (Settings1.Default.ConString == "")
                {
                    MessageBox.Show(Resources.Head_Form1Load_Line_of_the_connection_or_provider_database__is_not_chose);
                }
                else
                {
                    switch ((BaseType)Settings1.Default.ProviderEnum)
                    {
                        case BaseType.MySql:
                            CodeEditorSyntaxLoader.SetSyntax(textSQL, SyntaxLanguage.MySql_SQL);
                            CodeEditorSyntaxLoader.SetSyntax(Util.EditControl, SyntaxLanguage.MySql_SQL);
                            _suppler = new MySQL();
                            break;
                        case BaseType.PgSql:
                            _suppler = new PGSQL();
                            break;
                        case BaseType.Mssql:
                            CodeEditorSyntaxLoader.SetSyntax(textSQL, SyntaxLanguage.SqlServer2K5);
                            CodeEditorSyntaxLoader.SetSyntax(Util.EditControl, SyntaxLanguage.SqlServer2K5);
                            _suppler = new Mssql();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }


                    Util.Ii = _suppler;
                    if (_suppler == null) return;
                    using (var tt = _suppler.ShowDataBase())
                    {
                        foreach (DataRow r in tt.Rows)
                        {
                            var node = new TreeNode(r[0].ToString()) {Tag = 0};

                            var tt1 = _suppler.ShowTablesFromBase(r[0].ToString());
                            foreach (DataRow rr in tt1.Rows)
                            {
                                var nTable = new TreeNode(rr[0].ToString(), 7, 7) {Tag = 1};
                                node.Nodes.Add(nTable);
                            }
                            treeView.Nodes.Add(node);
                        }
                    }
                }
                //////////////////////////////////////////
                treeView.MouseDown += TreeViewMouseDown;
                treeView.MouseUp += TreeViewMouseUp;
                treeView.MouseMove += TreeViewMouseMove;
                /////////////////////////////////////////////
                _menuber.ButtonAdd = datamenuadd;
                _menuber.ButtonEdit = datamenuedit;
                _menuber.ButtonDeleteOn = datamenudeleteone;
                _menuber.ButtonEditDeleteAll = datamenudeleteall;
                _menuber.ColumnBox = searchcolumns;
                _menuber.TextBoxOne = searchTextBox;
                _menuber.TextBoxAll = dataFreeText;
                _menuber.GridViewData1 = GridView_Data;
                _menuber.Ii = _suppler;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message + Resources.Head_Form1Load__Or_No_Select_DataBase, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }
        }
        /// ////////////////////
        private void TreeViewMouseDown(object sender, MouseEventArgs e)
        {
            if (treeView.SelectedNode == null) return;
            if ((int) treeView.SelectedNode.Tag == -1) return;

            _dragDropTreeView = treeView.SelectedNode.Bounds.Contains(e.X, e.Y);
        }

        private void TreeViewMouseUp(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Default;
            if (!_dragDropTreeView) return;
            _dragDropTreeView = false;
            panel_H.MouseMove += Singlton.ControlPMouseMove;
        }

        private void TreeViewMouseMove(object sender, MouseEventArgs e)
        {
            if (!_dragDropTreeView) return;
            Cursor = _cur;
            Util.SelectNode = treeView.SelectedNode;
            panel_H.MouseMove -= Singlton.ControlPMouseMove;
        }

        /// /////////////////

        #region NoodlesTools
        private void ДобавитьТаблицуToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (Util.Ii == null)
            {
                MessageBox.Show(Resources.Head_ДобавитьТаблицуToolStripMenuItemClick_Выбор_поставшика_не_определён);
                return;
            }

            if (!string.IsNullOrEmpty(Util.Ii.Base))
            {
                var f = new AddTable();
                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        var t = new DataTable();
                        var c = new DataColumn("id") {AutoIncrement = true};
                        t.Columns.Add(c);
                        t.PrimaryKey = new[] {c};

                        var tables = new TableS(f.TableNAme, t, new Random().Next(100), 100);
                        if (Singlton.ControlP.Controls.OfType<TableS>().Any(ss => ss.Table.TableName == tables.Table.TableName))
                        {
                            throw new Exception("Duble Table");
                        }
                        Singlton.ControlP.Controls.Add(tables);
                    }
                    catch (Exception ee)
                    {
                        MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                    }
                }
            }
            else
            {
                MessageBox.Show(Resources.Head_ДобавитьТаблицуToolStripMenuItemClick_No_Slect_base_, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }
        }

        private void УдалитьВсёToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show(Resources.Head_УдалитьВсёToolStripMenuItemClick_Подтвердите_намерения_, Resources.Head_УдалитьВсёToolStripMenuItemClick_Ахтунг__, MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    Singlton.DataSet.Relations.CollectionChanged -= RelationsCollectionChanged;
                    if (Singlton.ControlP.Controls.Count != 0)
                    {
                        Singlton.ControlP.Controls.Clear();
                        Singlton.IncrementForControlName = 0;
                        ToolGrouMoveEnable(false);
                        Singlton.DataSet = new DataSet();
                    }
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error);
            }
        }

        private void СгруппироватьToolStripMenuItemClick(object sender, EventArgs e)
        {
            Singlton.GroupMoveLeft();
            ToolGrouMoveEnable(false);
        }

        private void СгруппироватьToolStripMenuItem1Click(object sender, EventArgs e)
        {
            Singlton.GroupMoveRight();
            ToolGrouMoveEnable(false);
        }

        private void СгруппироватььToolStripMenuItemClick(object sender, EventArgs e)
        {
            Singlton.GroupMoveTop();
            ToolGrouMoveEnable(false);
        }

        private void СгруппироватьToolStripMenuItem2Click(object sender, EventArgs e)
        {
            Singlton.GroupMoveBottom();
            ToolGrouMoveEnable(false);
        }

        private void LockToolStripMenuItemClick(object sender, EventArgs e)
        {
            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (c is ILock)
                {
                    ((ILock) c).Lock(lockToolStripMenuItem.Checked);
                }
            }
        }

        private void СохранитьСхемуToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (SFD.ShowDialog() == DialogResult.OK)
            {
                Singlton.SerializableTable(SFD.FileName);
            }
        }

        private void ЗагрузитьСхемуToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                Singlton.DesirealizeTable(OFD.FileName);
            }
        }

        private void ToolGrouMoveEnable(bool c)
        {
            сгруппироватьToolStripMenuItem.Enabled = c;
            сгруппироватьToolStripMenuItem1.Enabled = c;
            сгруппироватьToolStripMenuItem2.Enabled = c;
            сгруппироватььToolStripMenuItem.Enabled = c;
            if (!c)
                Singlton.ListControlForMove.Clear();
        }

        private void Form1ControlAdded(object sender, ControlEventArgs e)
        {
            сохранитьСхемуToolStripMenuItem.Enabled = true;
            загрузитьСхемуToolStripMenuItem.Enabled = false;
            удалитьВсёToolStripMenuItem.Enabled = true;
            lockToolStripMenuItem.Enabled = true;
        }

        private void Form1ControlRemoved(object sender, ControlEventArgs e)
        {
            сохранитьСхемуToolStripMenuItem.Enabled = panel_H.Controls.Count != 0;
            загрузитьСхемуToolStripMenuItem.Enabled = panel_H.Controls.Count == 0;
            удалитьВсёToolStripMenuItem.Enabled = panel_H.Controls.Count != 0;
            lockToolStripMenuItem.Enabled = panel_H.Controls.Count != 0;
        }

        private void DataSetTablesCountToolStripMenuItemClick(object sender, EventArgs e)
        {
            MessageBox.Show("" + Singlton.DataSet.Tables.Count);
        }

        #endregion

        private void Panel1MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
            }
            else
            {
                ToolGrouMoveEnable(false);
            }
        }

        private void Panel1MouseUp(object sender, MouseEventArgs e)
        {
            if (Singlton.ListControlForMove.Count != 0)
            {
                ToolGrouMoveEnable(true);
            }
            Cursor = Cursors.Default;
            if (!_dragDropTreeView) return;
            Util.GreateDiagramma(Util.SelectNode, e.X, e.Y);
            _dragDropTreeView = false;
            panel_H.MouseMove += Singlton.ControlPMouseMove;
        }


        private void TreeViewAfterSelect(object sender, TreeViewEventArgs e)
        {
            Singlton.DataSet.Relations.CollectionChanged -= RelationsCollectionChanged;
            if ((int) e.Node.Tag == 0)
            {
                _suppler.Base = e.Node.Text;
                Util.SimpleMappingTable.Clear();
                if (Singlton.ControlP.Controls.Count != 0)
                {
                    Singlton.ControlP.Controls.Clear();
                    Singlton.IncrementForControlName = 0;
                    ToolGrouMoveEnable(false);
                    Singlton.DataSet = new DataSet();
                }
            }

            else if ((int) e.Node.Tag == 1)
            {
                try
                {
                    if (_suppler.Base != e.Node.Parent.Text)
                    {
                        Util.SimpleMappingTable.Clear();
                        if (Singlton.ControlP.Controls.Count != 0)
                        {
                            Singlton.ControlP.Controls.Clear();
                            Singlton.IncrementForControlName = 0;
                            ToolGrouMoveEnable(false);
                            Singlton.DataSet = new DataSet();
                        }
                    }
                    _suppler.Base = e.Node.Parent.Text;


                    DataTable dataTable = _suppler.SelectFromTable(e.Node.Text);
                    GridView_Data.DataSource = dataTable;
                    GridView_Data.Tag = e.Node.Text;
                    _menuber.Hide();
                    searchcolumns.Items.Clear();

                    foreach (DataColumn column in dataTable.Columns)
                    {
                        searchcolumns.Items.Add(column.ColumnName);
                    }
                    searchcolumns.SelectedIndex = 0;


                    if (e.Node.Text.ToLower() != "information_schema")
                    {
                        var t2 = Util.Ii.ShowFieldsForEdit(e.Node.Text);
                        e.Node.Nodes.Clear();
                        foreach (DataRow r2 in t2.Rows)
                        {
                            var ff = r2[4].ToString() != "PRI" ? new TreeNode(r2[0].ToString(), 9, 9) : new TreeNode(r2[0].ToString(), 10, 10);
                            ff.Tag = -1;
                            e.Node.Nodes.Add(ff);
                        }
                    }
                    _menuber.TextBoxTextClear();
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                }
            }
        }

        private void GridViewDataDataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }


        private void MenyGridOpened(object sender, EventArgs e)
        {
            if (GridView_Data.DataSource == null) return;
            изменитьДанныеВБазеToolStripMenuItem.Enabled = false;
            var t = ((DataTable) GridView_Data.DataSource).GetChanges();
            if (t != null)
            {
                изменитьДанныеВБазеToolStripMenuItem.Enabled = true;
            }
        }

        private void ИзменитьДанныеВБазеToolStripMenuItemClick(object sender, EventArgs e)
        {
            var t1 = (DataTable) GridView_Data.DataSource;
            t1.TableName = (string) GridView_Data.Tag;
            _suppler.Updata(t1);
        }

        private void СоздатьДиаграммуToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (treeView.SelectedNode == null) return;
            Singlton.DataSet.Relations.CollectionChanged -= RelationsCollectionChanged;
            try
            {
                if ((int) treeView.SelectedNode.Tag == 1)
                {
                    var c = new TableS(treeView.SelectedNode.Text, _suppler.SelectFromTable(treeView.SelectedNode.Text), 100, new Random().Next(100));
                    foreach (Control cc in Singlton.ControlP.Controls)
                    {
                        var ss = cc as TableS;
                        if (ss == null) continue;
                        if (ss.Table.TableName == c.Table.TableName)
                        {
                            throw new Exception("Duble Table");
                        }
                    }
                    Singlton.ControlP.Controls.Add(c);
                }
                if ((int) treeView.SelectedNode.Tag != 0) return;
                Util.LRelation.Clear();
                Singlton.DataSet.Relations.Clear();
                Singlton.DataSet.Tables.Clear();
                int x1 = 100, y1 = 100;
                DataTable tt = _suppler.ShowTablesFromBase(treeView.SelectedNode.Text);

                foreach (DataRow er in tt.Rows)
                {
                    var c = new TableS(er[0].ToString(), _suppler.SelectFromTable(er[0].ToString()), x1, y1);
                    Singlton.ControlP.Controls.Add(c);
                    y1 = y1 + 20;
                    x1 = x1 + 40;
                    ////////////////////////////
                    List<RelationObject> l = Util.Ii.GetRelationObject(er[0].ToString());
                    foreach (RelationObject i in l)
                    {
                        Util.LRelation.Add(i);
                    }
                    /////////////////////////////
                }
                /////////////////////////////////////////////
                foreach (RelationObject i in Util.LRelation)
                {
                    //if (i.Active == false)
                    //{
                    try
                    {
                        string key = i.Id;
                        DataColumn par = Util.GetColumn(Singlton.DataSet, i.TableNameParent, i.ColumnNameTableChild);
                        DataColumn childr = Util.GetColumn(Singlton.DataSet, i.TableNameChild, i.ColumnNameTableParen);


                        var dr = new DataRelation(key, par, childr);
                        dr.ExtendedProperties.Add("OnDelete", i.OnDelete);
                        dr.ExtendedProperties.Add("OnUpdate", i.OnUpdate);
                        Singlton.DataSet.Tables[i.TableNameChild].ParentRelations.Add(dr);


                        // Object o = Singlton.CreateObject("IAddRelation");

                        //Mover M = new Mover(i.TableNameParent,
                        //    i.TableNameChild, key);
                        //i.Active = true;
                    }
                    catch (Exception ee)
                    {
                        throw new Exception(ee.Message);
                    }
                    //}
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }
            finally
            {
                Singlton.DataSet.Relations.CollectionChanged += RelationsCollectionChanged;
            }
        }

        private void ToolStripButton2Click(object sender, EventArgs e)
        {
            treeView.CollapseAll();
        }

        private void ToolStripButton3Click(object sender, EventArgs e)
        {
            treeView.ExpandAll();
        }

        private void MenuTreeBaseOpening(object sender, CancelEventArgs e)
        {
            if (treeView.SelectedNode == null || (int) treeView.SelectedNode.Tag == -1)
            {
                e.Cancel = true;
                return;
            }
            switch ((int) treeView.SelectedNode.Tag)
            {
                case 0:
                    создатьДиаграммуToolStripMenuItem.Text = Resources.Head_MenuTreeBaseOpening_Create_diagrams_All_Table;
                    cjToolStripMenuItem.Enabled = true;
                    dropBaseToolStripMenuItem.Enabled = true;
                    toolStripMenuItem1.Enabled = false;
                    break;
                case 1:
                    создатьДиаграммуToolStripMenuItem.Text = Resources.Head_MenuTreeBaseOpening_Create_diagram_;
                    cjToolStripMenuItem.Enabled = false;
                    dropBaseToolStripMenuItem.Enabled = false;
                    toolStripMenuItem1.Enabled = true;
                    break;
            }
        }


        private void ToolStripButton5Click(object sender, EventArgs e)
        {
            var f = new ConectorForm();
            if (f.ShowDialog() == DialogResult.OK)
            {
                Form1Load(null, null);
            }
        }

        private void ToolStripButton6Click(object sender, EventArgs e)
        {
            var f = new MapperShowmen();
            f.ShowDialog();
        }

        private void ToolStripButton8Click(object sender, EventArgs e)
        {
            var f = new SettingMap();
            f.ShowDialog();
        }

        private void ToolStripButton10Click(object sender, EventArgs e)
        {
            var document = new XmlDocument();
            document.Load(Application.StartupPath + "/pattern/areal.xml");
            var lastChild = document.LastChild;
            if (Util.Ii == null)
            {
                MessageBox.Show(Resources.Head_ToolStripButton10Click_No_select_provider__);
                return;
            }
            var xmlNodeList = lastChild.SelectNodes("app");
            if (xmlNodeList != null)
            {
                string xml = xmlNodeList[0].InnerText.Replace("CON", Util.Ii.Base != null ? Util.Ii.ConnectString : Settings1.Default.ConString);
                File.WriteAllText(Settings1.Default.path + "/" + "AppConfig_.xml", xml.Replace("ASM", Settings1.Default.assembly).Replace("DIALECT", Util.Ii.Dialect), Encoding.UTF8);
            }
            Process.Start(Settings1.Default.path + "\\AppConfig_.xml"); //
        }

        //Удаение связи
        public void RelationsCollectionChanged(object sender, CollectionChangeEventArgs e)
        {
            var r = (DataRelation) e.Element;
            try
            {
                if (e.Action == CollectionChangeAction.Remove)
                {
                    Util.Ii.DrobForeignKey(r.ChildTable.TableName, r.RelationName);
                }
                else if (e.Action == CollectionChangeAction.Add)
                {
                    //для добавления по одному
                    if (r.ExtendedProperties.Count != 3)
                    {
                        Util.Ii.CreateForeiginKey(r.ChildTable.TableName, r.ParentTable.TableName, r.ChildColumns[0].ColumnName, r.ParentColumns[0].ColumnName, r.RelationName);
                    }
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }

            //
        }

        private void ContextMenuStrip2Opening(object sender, CancelEventArgs e)
        {
            if (panel_H.Controls.Count != 0)
            {
                crateBaseToolStripMenuItem.Enabled = true;
                загрузитьСхемуToolStripMenuItem.Enabled = false;
                toolStripMenuItem2.Enabled = true;
                unfoldAllTableToolStripMenuItem.Enabled = true;
                сохранитьСхемуToolStripMenuItem.Enabled = true;
                lockToolStripMenuItem.Enabled = true;
                удалитьВсёToolStripMenuItem.Enabled = true;
            }
            else
            {
                crateBaseToolStripMenuItem.Enabled = false;
                toolStripMenuItem2.Enabled = false;
                unfoldAllTableToolStripMenuItem.Enabled = false;
                загрузитьСхемуToolStripMenuItem.Enabled = true;
                сохранитьСхемуToolStripMenuItem.Enabled = false;
                lockToolStripMenuItem.Enabled = false;
                удалитьВсёToolStripMenuItem.Enabled = false;
            }
            добавитьТаблицуToolStripMenuItem.Enabled = Util.UsingConstructor;
        }

        private void TreeViewBeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
        }

        private void CjToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (Util.Ii.Base != "")
            {
                var f = new AddTable();
                if (f.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Util.Ii.CreateTable(f.TableNAme);
                        var nTable = new TreeNode(f.TableNAme, 7, 7) {Tag = 1};
                        treeView.Nodes[treeView.Nodes.IndexOf(treeView.SelectedNode)].Nodes.Add(nTable);
                    }
                    catch (Exception ee)
                    {
                        MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                    }
                }
            }
            else
            {
                MessageBox.Show(Resources.Head_CjToolStripMenuItemClick_No_Base_Select_);
            }
        }

        private void DropBaseToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.Head_DropBaseToolStripMenuItemClick_Drop_Base__ + treeView.SelectedNode.Text + Resources.Head_DropBaseToolStripMenuItemClick__, Resources.Head_DropBaseToolStripMenuItemClick_Delete_, MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            try
            {
                Util.Ii.DeleteBase(treeView.SelectedNode.Text);
                treeView.SelectedNode.Remove();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }
        }

        private void CreateBaseToolStripMenuItemClick(object sender, EventArgs e)
        {
            var f = new AddBase();
            if (f.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((BaseType) Settings1.Default.ProviderEnum == BaseType.MySql)
                        Util.Ii.CreateBase(f.NameBase, "");
                    else if ((BaseType) Settings1.Default.ProviderEnum == BaseType.Mssql)
                    {
                        Util.Ii.CreateBase(f.NameBase, f.PathBase);
                        MessageBox.Show(Resources.Head_CreateBaseToolStripMenuItemClick_Base_is_created__ + f.PathBase + Resources.Head_CreateBaseToolStripMenuItemClick__ + f.NameBase + Resources.Head_CreateBaseToolStripMenuItemClick__mdf);
                        Settings1.Default.SelectedPath_MSSQL_BASE = f.NameBase;
                        Settings1.Default.Save();
                    }
                    var node = new TreeNode(f.NameBase) {Tag = 0};
                    treeView.Nodes.Add(node);
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                }
            }
        }

        private void ToolStripMenuItem1Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(Resources.Head_ToolStripMenuItem1Click_Delete_table___ + treeView.SelectedNode.Text + Resources.Head_ToolStripMenuItem1Click__, Resources.Head_ToolStripMenuItem1Click_Удаление, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    Util.Ii.DeleteTable(treeView.SelectedNode.Text);
                    treeView.SelectedNode.Remove();
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
                }
            }
        }

        private void ToolStripButton11Click(object sender, EventArgs e)
        {
            Process.Start(Settings1.Default.path);
        }


        private void DataGridViewExeCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void Button1Click(object sender, EventArgs e)
        {
            try
            {
                dataGridViewEXE.DataSource = Util.Ii.SqlExecute(string.Concat(textSQL.Document.Lines));
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message + Resources.Head_Button1Click_ + ee.StackTrace, Resources.Head_Form1Load_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }
        }


        private void DataGridViewExeDataError1(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }


        private void EntryInModeOfTheConstructorToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (Util.UsingConstructor == false)
            {
                if (MessageBox.Show(Resources.Head_EntryInModeOfTheConstructorToolStripMenuItemClick_Вы_хотите_преейти_в_режим_конструктора_базы_по_xml_файлам_, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Util.UsingConstructor = true;
                    toolStripStatusLabel1.Text = Resources.Head_EntryInModeOfTheConstructorToolStripMenuItemClick_Conctructor;
                    Util.DubleIi = _suppler;
                    Util.Ii = new Constructor();
                    toolStripButton5.Enabled = false;
                    treeView.Enabled = false;
                }
            }
            else
            {
                if (MessageBox.Show(Resources.Head_EntryInModeOfTheConstructorToolStripMenuItemClick_Вы_хотите__выйти_из_режима_конструктора_, "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Util.UsingConstructor = false;
                    toolStripStatusLabel1.Text = "";
                    Util.Ii = Util.DubleIi;
                    toolStripButton5.Enabled = true;
                    treeView.Enabled = true;
                }
            }
        }

      

        private void ToolStripMenuItem2Click(object sender, EventArgs e)
        {
            foreach (Control c in panel_H.Controls)
            {
                var openOrClose = c as IOpenOrClose;
                if (openOrClose != null)
                {
                    openOrClose.Open();
                }
            }
            Singlton.RefreshMoverForRelationAll();
        }

        private void UnfoldAllTableToolStripMenuItemClick(object sender, EventArgs e)
        {
            foreach (Control c in panel_H.Controls)
            {
                var openOrClose = c as IOpenOrClose;
                if (openOrClose != null)
                {
                    openOrClose.Close();
                }
            }
            Singlton.RefreshMoverForRelationAll();
        }

        private void HeadFormClosing(object sender, FormClosingEventArgs e)
        {
            if (Util.Ii != null)
                Util.Ii.Dispose();
        }

        private void CrateBaseToolStripMenuItemClick(object sender, EventArgs e)
        {
            var f = new CreateBase(treeView, panel_H);
            f.SelectNodeS += FSelectNodeS;
            f.ShowDialog();
        }

        private void FSelectNodeS(object sender, TreeViewEventArgs e)
        {
            TreeViewAfterSelect(sender, e);
        }

        private void Splitter1SplitterMoved(object sender, SplitterEventArgs e)
        {
            Settings1.Default.SplitY = ((Splitter) sender).SplitPosition;
            Settings1.Default.Save();
        }

        private void Splitter3SplitterMoved(object sender, SplitterEventArgs e)
        {
            Settings1.Default.SplitX = ((Splitter) sender).SplitPosition;
            Settings1.Default.Save();
        }

        private void Splitter2SplitterMoved(object sender, SplitterEventArgs e)
        {
            Settings1.Default.SplitX2 = ((Splitter) sender).SplitPosition;
            Settings1.Default.Save();
        }

        private void TabControl1SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings1.Default.SelectTab = tabControl1.SelectedTab.TabIndex;
            Settings1.Default.Save();
        }
    }
}
