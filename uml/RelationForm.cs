﻿using System;
using System.Data;
//using System.Linq;
using System.Windows.Forms;
using MoverUML;
using uml.Properties;

namespace uml
{

    public partial class RelationForm : Form,IAddRelation
    {
        DataRelation _dr;
        public RelationForm()
        {
            InitializeComponent();
        }
        private void Form2Load(object sender, EventArgs e)
        {
            
            comboBox_TableParent.Items.Clear();
            comboBox_tableChild.Items.Clear();
            foreach (Control c in Singlton.ControlP.Controls)
            {
                if (!(c is TableS)) continue;
                comboBox_TableParent.Items.Add(c.Name);
                comboBox_tableChild.Items.Add(c.Name);
            }
            var ii = comboBox_Delete.Items.IndexOf(Settings1.Default.OnDelete.Trim());
            comboBox_Delete.SelectedIndex = ii != -1 ? ii : 0;

             ii = comboBox_Update.Items.IndexOf(Settings1.Default.OnUpdate.Trim());
            comboBox_Update.SelectedIndex = ii != -1 ? ii : 0;

           //  NO ACTION | CASCADE | SET NULL | SET DEFAULT }
            if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
            {
                comboBox_Delete.Items.Clear();
                comboBox_Update.Items.Clear();
                foreach (var s in new[] { "NO ACTION", "CASCADE", "SET NULL", "SET DEFAULT" })
                {
                    comboBox_Delete.Items.Add(s);
                    comboBox_Update.Items.Add(s);
                }
                comboBox_Update.SelectedIndex = 0;
                comboBox_Delete.SelectedIndex = 0;
            }
            //comboBox_Delete.SelectedIndex = 0;
            //comboBox_Update.SelectedIndex = 0;
        }
        public string KeyTabl
        {
            get { return comboBox_TableParent.Text; }
        }
        public string ParentTabl
        {
            get { return comboBox_tableChild.Text; }
        }
        public DataRelation Relation
        {
            get { return _dr; }
            set { _dr = value; }
        }

        private void Button1Click(object sender, EventArgs e)
        {
            if (listBox_ColumChild.SelectedIndex == -1 || listBox_ColumnParent.SelectedIndex == -1)
            {
                MessageBox.Show(Resources.Form2_Button1Click_No_select_column_);
                return;
            }
            Settings1.Default.OnDelete = comboBox_Delete.Text;
            Settings1.Default.OnUpdate = comboBox_Update.Text;
             var s1Parent=new DataColumn[1];
             var s2Child=new DataColumn[1];
            
           
             s1Parent[0] = Util.GetColumn(Singlton.DataSet, comboBox_TableParent.Text, listBox_ColumnParent.SelectedItem.ToString());
             s2Child[0] = Singlton.DataSet.Tables[comboBox_tableChild.Text].Columns[listBox_ColumChild.SelectedItem.ToString()];
                try
                {
                    _dr = new DataRelation("FK"+Guid.NewGuid().ToString().Replace('-','_'), s1Parent, s2Child);
                    _dr.ExtendedProperties.Add("OnDelete", comboBox_Delete.Text);
                    _dr.ExtendedProperties.Add("OnUpdate", comboBox_Update.Text);
                    DialogResult = DialogResult.OK;

                    _dr.ExtendedProperties.Add("ion", 1);
                    Util.Ii.CreateForeiginKey(_dr.ChildTable.TableName,
                                          _dr.ParentTable.TableName,
                                          _dr.ChildColumns[0].ColumnName,
                                          _dr.ParentColumns[0].ColumnName,
                                         _dr.RelationName);
                    Singlton.DataSet.Tables[_dr.ChildTable.TableName].ParentRelations.Add(_dr);

                    var m = new Mover(_dr.ParentTable.TableName,
                        _dr.ChildTable.TableName, _dr.RelationName);
                  
                }
                catch (Exception ee)
                {
                    MessageBox.Show(Resources.Form2_Button1Click_Ошибка_создания_связи__ + ee.Message, Resources.Form2_Button1Click_error);
                    button1.DialogResult = DialogResult.None;

                    DialogResult = DialogResult.None;
                }
                //Singlton.DataSet.Tables[comboBox_tableChild.Text].ParentRelations.Add(DR);
           

          
        }

        private void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox_ColumnParent.Items.Clear();
          foreach(DataColumn c in Singlton.DataSet.Tables[Singlton.DataSet.Tables.IndexOf(comboBox_TableParent.Text)].Columns)
          {
              listBox_ColumnParent.Items.Add(c.ColumnName);
          }
        }

        private void ComboBox2SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox_ColumChild.Items.Clear();
            foreach (DataColumn c in Singlton.DataSet.Tables[Singlton.DataSet.Tables.IndexOf(comboBox_tableChild.Text)].Columns)
            {
                listBox_ColumChild.Items.Add(c.ColumnName);
            }
        }
    }
}
