﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Xml;
using MoverUML;
using System.ComponentModel;
using Fireball.Windows.Forms;
using uml.Properties;

namespace uml
{
    public static class Util
    {
        public static CodeEditorControl EditControl;
        public static ISupplier DubleIi;
        public static ISupplier Ii;
        static string _createtable, _showfields, _showkey, _showtable, _droptable, _showdatabases, _addcolumn,
              _change, _dropfield, _typecolumn, _deletecolumn, _modify, _dropbase, _showcomment,
              _addkey, _dropkey;
        //Util() { }
        public static Constructor ctor = new Constructor();
        public static bool Constuctor ;
        public static bool UsingConstructor ;
        public static TreeNode SelectNode;
        static Util()
        {
            //try
            //{


            var doc = new XmlDocument();
            doc.Load(Application.StartupPath + "/pattern/MySQL_SQL.xml");
            var areal = doc.LastChild;
            var xmlNodeList = areal.SelectNodes("createtable");
            if (xmlNodeList != null) _createtable = xmlNodeList[0].InnerText;
            var selectNodes = areal.SelectNodes("showfields");
            if (selectNodes != null) _showfields = selectNodes[0].InnerText;
            var nodeList = areal.SelectNodes("showkey");
            if (nodeList != null) _showkey = nodeList[0].InnerText;
            var nodes = areal.SelectNodes("showtable");
            if (nodes != null) _showtable = nodes[0].InnerText;
            var list = areal.SelectNodes("droptable");
            if (list != null) _droptable = list[0].InnerText;
            var xmlNodeList1 = areal.SelectNodes("showdatabases");
            if (xmlNodeList1 != null)
                _showdatabases = xmlNodeList1[0].InnerText;
            var selectNodes1 = areal.SelectNodes("addcolumn");
            if (selectNodes1 != null) _addcolumn = selectNodes1[0].InnerText;
            var nodeList1 = areal.SelectNodes("change");
            if (nodeList1 != null) _change = nodeList1[0].InnerText;
            var nodes1 = areal.SelectNodes("dropfield");
            if (nodes1 != null) _dropfield = nodes1[0].InnerText;
            var list1 = areal.SelectNodes("typecolumn");
            if (list1 != null) _typecolumn = list1[0].InnerText;
            var xmlNodeList2 = areal.SelectNodes("deletecolumn");
            if (xmlNodeList2 != null)
                _deletecolumn = xmlNodeList2[0].InnerText;
            _modify = areal.SelectNodes("modify")[0].InnerText;
            _dropbase = areal.SelectNodes("dropbase")[0].InnerText;
            _showcomment = areal.SelectNodes("showcomment")[0].InnerText;
            _addkey = areal.SelectNodes("addkey")[0].InnerText;
            _dropkey = areal.SelectNodes("dropkey")[0].InnerText;

            //}
            //catch
            //{
            //}
        }
        /// <summary>
        /// Функция печати запроса и ошибки
        /// </summary>
        /// <param name="sql"></param>
        public static void PrintSql(string sql)
        {
            EditControl.Document.Text = String.Format("{0}{1}{2}", Environment.NewLine, sql, Environment.NewLine) + EditControl.Document.Text;

        }
        public static Panel PanelH;
        public static MainForm FormMainForm;

        public static DataSet Ds = new DataSet();
        public static List<string> SimpleMappingTable = new List<string>();
        /// <summary>
        /// Перенос между листбоксами
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void Carrying(ListBox from, ListBox to)
        {
            if (from.SelectedItems.Count == 0) return;

            foreach (var o in from.SelectedItems)
            {
                to.Items.Add(o.ToString());

            }
            foreach (var s in to.Items)
            {
                from.Items.Remove(s);
            }
        }
        public static List<RelationObject> LRelation = new List<RelationObject>();

        public static DataColumn GetColumn(DataSet ds, string table, string columnName)
        {
            return ds.Tables[table].Columns[columnName];
        }
        class Upper
        {
            public readonly int Top;
            public readonly int Left;
            private readonly int _h;
            private readonly int _w;
            public readonly string Name;
            public readonly bool Lock;
            public readonly bool Expand;
            public Upper(int top, int left, int h, int w, string name, bool @lock, bool expand)
            {
                Top = top;
                Left = left;
                _h = h;
                _w = w;
                Name = name;
                Lock = @lock;
                Expand = expand;
            }
        }
        public static void UpdatePanelH()
        {
            Singlton.DataSet.Relations.CollectionChanged -= FormMainForm.RelationsCollectionChanged;
            LRelation.Clear();
            Singlton.DataSet = new DataSet();
            var lupper = new List<Upper>();
            foreach (Control c in PanelH.Controls)
            {
                var t = c as TableS;
                if (t != null)
                {
                    lupper.Add(new Upper(t.Top, t.Left, t.Height, t.Width, t.Name, t.LOCK, t.Expand));
                }
            }
            PanelH.Controls.Clear();
            foreach (var u in lupper)
            {
                var c = new TableS(u.Name, Ii.SelectFromTable(u.Name), u.Left, u.Top) {LOCK = u.Lock, Expand = u.Expand};
                Singlton.ControlP.Controls.Add(c);
                var l = Ii.GetRelationObject(u.Name);
                foreach (var i in l)
                {
                    LRelation.Add(i);
                }
            }
            foreach (RelationObject i in LRelation)
            {

                if (i.Active == false)
                {
                    var key = i.Id;
                    var p = GetColumn(Singlton.DataSet, i.TableNameParent, i.ColumnNameTableChild);
                    var p1 = GetColumn(Singlton.DataSet, i.TableNameChild, i.ColumnNameTableParen);
                    var dr = new DataRelation(key, p, p1);
                    dr.ExtendedProperties.Add("OnDelete", i.OnDelete);
                    dr.ExtendedProperties.Add("OnUpdate", i.OnUpdate);
                    Singlton.DataSet.Tables[i.TableNameChild].ParentRelations.Add(dr);
                    var o = Singlton.CreateObject("IAddRelation");
                    var m = new Mover(i.TableNameParent,
                    i.TableNameChild, key);
                    i.Active = true;
                }

            }
            Singlton.DataSet.Relations.CollectionChanged += FormMainForm.RelationsCollectionChanged;

        }
        public static bool PingMySql(string connectString)
        {
            var c = new MySql.Data.MySqlClient.MySqlConnection(connectString);

            c.Open();
            try
            {
                if (c.Ping())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ee)
            {
                throw new Exception(ee.Message);
            }
            finally
            {
                c.Close();
            }

        }
        public static bool PingMyPgsql(string connectString)
        {
            var c = new Npgsql.NpgsqlConnection(connectString);

            c.Open();
            try
            {
                c.Open();
                return false;

            }
            catch (Exception ee)
            {
                throw new Exception(ee.Message);
            }
            finally
            {
                c.Close();
            }

        }




        internal static bool PingMssql(string connstring)
        {
            var con = new System.Data.SqlClient.SqlConnection(connstring);
            try
            {
                con.Open();
                return true;
            }
            catch (Exception ee)
            {
                throw new Exception(ee.Message);

            }
            finally
            {
                con.Close();
            }
        }
        public static string Limit
        {

            get
            {
                var limit = string.Empty;

                if ((BaseType)Settings1.Default.ProviderEnum == BaseType.MySql)
                {
                    if (Settings1.Default.UsingLimit)
                    {
                        limit = string.Format("limit 0,{0}", Settings1.Default.LimitRows);
                    }
                }
                else if ((BaseType)Settings1.Default.ProviderEnum == BaseType.PgSql)
                {
                }
                else if ((BaseType)Settings1.Default.ProviderEnum == BaseType.Mssql)
                {
                    if (Settings1.Default.UsingLimit)
                    {
                        limit = string.Format("TOP {0}", Settings1.Default.LimitRows);
                    }
                }
                return limit;



            }
        }

        public static void GreateDiagramma(TreeNode n, int x, int y)
        {

            if (n == null) return;
            Singlton.DataSet.Relations.CollectionChanged -= RelationsCollectionChanged;
            try
            {

                if ((int)n.Tag == 1)
                {

                    var c = new TableS(n.Text, Ii.SelectFromTable(n.Text), x, y);
                    foreach (Control cc in Singlton.ControlP.Controls)
                    {
                        var ss = cc as TableS;
                        if (ss == null) continue;
                        if (ss.Table.TableName == c.Table.TableName)
                        {
                            throw new Exception("Duble Table");
                        }
                    }
                    Singlton.ControlP.Controls.Add(c);

                }
                if ((int) n.Tag != 0) return;
                LRelation.Clear();
                Singlton.DataSet.Relations.Clear();
                Singlton.DataSet.Tables.Clear();
                int x1 = 100, y1 = 100;
                var tt = Ii.ShowTablesFromBase(n.Text);

                foreach (DataRow er in tt.Rows)
                {
                    //   string tttt = er[0].ToString();
                    var c = new TableS(er[0].ToString(), Ii.SelectFromTable(er[0].ToString()), x1, y1);
                    Singlton.ControlP.Controls.Add(c);
                    y1 = y1 + 20;
                    x1 = x1 + 40;
                    ////////////////////////////
                    var l = Ii.GetRelationObject(er[0].ToString());
                    foreach (var i in l)
                    {
                        LRelation.Add(i);
                    }
                    /////////////////////////////

                }
                /////////////////////////////////////////////
                foreach (var i in LRelation)
                {
                    //if (i.Active == false)
                    //{
                    try
                    {
                        var key = i.Id;
                        var par = GetColumn(Singlton.DataSet, i.TableNameParent, i.ColumnNameTableChild);
                        var childr = GetColumn(Singlton.DataSet, i.TableNameChild, i.ColumnNameTableParen);


                        var dr = new DataRelation(key, par, childr);
                        dr.ExtendedProperties.Add("OnDelete", i.OnDelete);
                        dr.ExtendedProperties.Add("OnUpdate", i.OnUpdate);
                        Singlton.DataSet.Tables[i.TableNameChild].ParentRelations.Add(dr);



                        // Object o = Singlton.CreateObject("IAddRelation");

                        //Mover M = new Mover(i.TableNameParent,
                        //    i.TableNameChild, key);
                        //i.Active = true;
                    }
                    catch (Exception ee)
                    {
                        throw new Exception(ee.Message);
                    }
                    //}



                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.Util_GreateDiagramma_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }
            finally
            {

                Singlton.DataSet.Relations.CollectionChanged += RelationsCollectionChanged;
            }

        }
        public static void RelationsCollectionChanged(Object sender, CollectionChangeEventArgs e)
        {
            var r = (DataRelation)e.Element;
            try
            {
                switch (e.Action)
                {
                    case CollectionChangeAction.Remove:
                        Ii.DrobForeignKey(r.ChildTable.TableName, r.RelationName);
                        break;
                    case CollectionChangeAction.Add:
                        if (r.ExtendedProperties.Count != 3)
                        {
                            Ii.CreateForeiginKey(r.ChildTable.TableName,
                                                      r.ParentTable.TableName,
                                                      r.ChildColumns[0].ColumnName,
                                                      r.ParentColumns[0].ColumnName,
                                                      r.RelationName);
                        }
                        break;
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.Util_GreateDiagramma_Error, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, 0);
            }

            //
        }




    }
}
