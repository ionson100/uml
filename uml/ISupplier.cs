﻿using System;
using System.Collections.Generic;
using System.Data;

namespace uml
{
   public interface ISupplier:IDisposable
    {
        DataTable ShowDataBase();
        
         DataTable ShowTablesFromBase(string bases);
        
         DataTable SelectFromTable(string table);

         string Base
         {
             get;
             set;
         }

         string ConnectString
         {
             get;

         }
         string Dialect
         {
             get;
         }
       
         void Updata(DataTable t);
      
         DataTable ShowFields(string tablename);
       
         List<RelationObject> GetRelationObject(string tableName);
       
         void DrobForeignKey(string tableName, string id);

         void CreateForeiginKey(string tableNameChild, string tableNameParent, string columnNameChild, string columnNameParenr, string idRelation);
         string[] GetTypeColumn();
         DataTable ShowFieldsForEdit(String tableName);
         void DeletePrimaryKey(string tableName);
         void AddPrimaryKey( string tableName, string columnnane);
         void ModufyColumn(string table, string nameOld, string nameNew, string type, string defaults, string nulls, string comment);
         void AddColumn(string table, string name, string type, string defaults, string nulls);
         void DeleteColumn(string table, string columnname);
         void CreateBase(string basename,string patch);
         void DeleteBase(string basename);
         void RenameRelation(DataRelation newRelation, string idOldRelation);
         void CreateTable(String tableName);
         DataSet SelectFromTableForMap(string sql);
         void DeleteTable(string tableName);
         DataTable SqlExecute(string sql);
         String[] Word
         {
             get;
         }
        
        // bool Ping(string ConnectString);
        
       
    }
}
