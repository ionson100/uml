﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using MoverUML;
using uml.Properties;
//using System.Linq;

namespace uml
{


    public partial class TableS : UserControl, IChangeColour, ILock, ISerializableTables, IOpenOrClose
    {
        
        bool _expand = true;
        DataTable _table = new DataTable();
        Control _c;
        bool _lock = true;
        Int32 _x, _y,_h=200;
        Color _baseColorPanel;
        bool _blockingDrag = true;
        public TableS()
        {
            InitializeComponent();
      
        }
        public TableS(string name,DataTable T, int x, int y)
        {
            InitializeComponent();
            _c = Singlton.ControlP;
            Name = name;
        
           
            if (T != null)
                Table = T;
            _table.TableName = name;
            Left = x;
            Top = y;
        }
        public TableS(string name,DataTable T)
        {
            InitializeComponent(); 
            _c = Singlton.ControlP;
            Name = name;
           
            if(T!=null)
            Table = T;
            Table.TableName = name;
          
           
        }

        private void Panel6MouseMove(object sender, MouseEventArgs e)
        {
            if (!_lock) return;
            if (e.Button == MouseButtons.Left)
            {
                if (Top + e.Y - _y > 0 && Left + e.X - _x > 0
                    && Top + e.Y - _y + 40 < _c.Height && Left + e.X - _x+Width+15 <_c.Width)
                {
                    if (_blockingDrag)
                    {
                       Top = Top + e.Y - _y;
                       
                        Left = Left + e.X - _x;
                    }
                    else
                    {
                        foreach (Control c in _c.Controls)
                        {
                            if (!(c is IChangeColour)) continue;
                            if (((IChangeColour) c).GetBlockingDrag()) continue;
                            c.Top = c.Top + e.Y - _y;
                            c.Left = c.Left + e.X - _x;
                        }
                    }
                    
                }
            }

        }

        private void Panel6MouseDown(object sender, MouseEventArgs e)
        {
        //    panel6.Controls.SetChildIndex(panel6.Controls.Find(this.Name, false)[0], 0);//ставим наверхъ
            Parent.Controls.SetChildIndex(this, 0);//ставим наверхъ
            _x = e.X; _y = e.Y;
        }

        private void TableSLoad(object sender, EventArgs e)
        {

            _c = Singlton.ControlP;
            panel1.BackColor = _c.BackColor;
            panel2.BackColor = _c.BackColor;
            panel3.BackColor = _c.BackColor;
            panel4.BackColor = _c.BackColor;
            _baseColorPanel = panel6.BackColor;
            label1.Text = Name;
            

                Singlton.DataSet.Tables.Add(_table);
          

            listBox1.Items.Clear();
            foreach (DataColumn c in _table.Columns)
            {
                listBox1.Items.Add(c.ColumnName);
            }
           
          
               
           
        }

        private void Panel3MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Width = Width + e.X;
            }
           
        }

        private void Panel1MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            Width = Width - e.X;
            Left = Left + e.X;
        }

        private void Panel4MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Height = Height + e.Y;
            }
        }

        private void Panel2MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Height = Height - e.Y;
                Top = Top + e.Y;
            }
        }

        private void PictureBox1Click(object sender, EventArgs e)
        {
            Parent.Controls.SetChildIndex(this, 0);
            if (pictureBox1.Tag.ToString() != "1")
            {
                pictureBox1.Image = Resources.Collapse_large;
                pictureBox1.Tag = "1";
                panel6.Dock = DockStyle.Top;
                 Height = _h;
                 _expand = true;
            }
            else
            {
                _h = Height;
                pictureBox1.Image = Resources.Expand_large;
                pictureBox1.Tag = "2";
                panel6.Dock = DockStyle.Bottom;
                 Height = 28;
                 _expand = false;
            }
            Singlton.RefreshMoverForRelationOne(Name);
        }
        public bool Expand
        {
            get { return _expand; }
            set
            {
                if (value == false)
                {
                    _h = Height;
                    pictureBox1.Image = Resources.Expand_large;
                    pictureBox1.Tag = "2";
                    panel6.Dock = DockStyle.Bottom;
                    Height = 28;
                    _expand = false;
                }
            }
        }


        private void СоздатьСвязьToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var o = Singlton.CreateObject("IAddRelation");
                if (o == null) return;
                if (((Form)o).ShowDialog() == DialogResult.OK)
                {
                    //((IAddRelation)o).Relation.ExtendedProperties.Add("ion", 1);
                    //Util.ii.CreateForeiginKey(((IAddRelation)o).Relation.ChildTable.TableName,
                    //                      ((IAddRelation)o).Relation.ParentTable.TableName,
                    //                      ((IAddRelation)o).Relation.ChildColumns[0].ColumnName,
                    //                      ((IAddRelation)o).Relation.ParentColumns[0].ColumnName,
                    //                      ((IAddRelation)o).Relation.RelationName);
                    //Singlton.DataSet.Tables[((IAddRelation)o).Relation.ChildTable.TableName].ParentRelations.Add(((IAddRelation)o).Relation);

                    //Mover M = new Mover(((IAddRelation)o).Relation.ParentTable.TableName,
                    //    ((IAddRelation)o).Relation.ChildTable.TableName, ((IAddRelation)o).Relation.RelationName);


                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message, Resources.TableS_СоздатьСвязьToolStripMenuItemClick_Error);
            }
        }

        private void Panel6MouseUp(object sender, MouseEventArgs e)
        {
           
            if (!_lock) return;
            if (!_blockingDrag)
            {
                        foreach (Control c in _c.Controls)
                        {
                            var changeColour = c as IChangeColour;
                            if (changeColour == null) continue;
                            changeColour.SetBlockingDrag(true); 
                            changeColour.ChangeColourBase();
                        }
            }
            Singlton.RefreshMoverForRelationOne(Name);
            Singlton.RefreshMoverForRelationAll();
           
          
           
        }

        private void УдалитьТаблицуToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(Resources.TableS_УдалитьТаблицуToolStripMenuItemClick_Подтвердите_намерения_,
                                Resources.TableS_УдалитьТаблицуToolStripMenuItemClick_Ахтунг__,
                                MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) !=
                DialogResult.OK) return;
            try
            {

                Singlton.DataSet.Relations.CollectionChanged -= Util.FormMainForm.RelationsCollectionChanged;
                       
                       
                Singlton.DataSet.Tables[Singlton.DataSet.Tables.IndexOf(_table.TableName)].Constraints.Clear();
                Singlton.DeleteRelationOne(_table.TableName);
                _c.Controls.Remove(this);
                Singlton.RefreshMoverForRelationAll();
                Singlton.DataSet.Tables.Remove(_table.TableName);

            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
            finally
            {
                Singlton.DataSet.Relations.CollectionChanged += Util.FormMainForm.RelationsCollectionChanged;

            }
        }

        #region IChangeColour Members

        void IChangeColour.ChangeColourForMoving(Color colorForMoving)
        {
            panel6.BackColor = colorForMoving;
        }

        void IChangeColour.ChangeColourBase()
        {
            panel6.BackColor = _baseColorPanel;
        }

        void IChangeColour.SetBlockingDrag(bool blockingDrag)
        {
            _blockingDrag = blockingDrag;
        }

        bool IChangeColour.GetBlockingDrag()
        {
            return _blockingDrag;
        }
      
        #endregion

        #region ILock Members

        void ILock.Lock(bool Lock)
        {
            _lock = Lock;
            pictureBox3.Visible = !Lock;

        }
        public bool LOCK
        {
            get { return _lock; }
            set { ((ILock)this).Lock(value); }
        }
        public void PictureTagSet(object  tag)
        {
            pictureBox1.Tag = tag;
            _expand = pictureBox1.Tag.ToString() == "1";
        }

        public void GetH(int h)
        {
            _h = h;
        }

        #endregion

        private void РедактироватьТаблицуToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var o = Singlton.CreateObject("IEditTable");
                if (o == null) return;
                var f = (IEditTable)o;
                f.Table = _table;
                if (((Form) o).ShowDialog() != DialogResult.OK) return;
                _table = ((IEditTable)o).Table;
                listBox1.Items.Clear();
                foreach (DataColumn c in _table.Columns)
                {
                    listBox1.Items.Add(c.ColumnName);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }

        }

        #region ISerializableTables Members

        SerializableTables ISerializableTables.GetSerializableTables
        {
            get
            {
                var f = new SerializableTables
                            {
                                ControlLeft = Left,
                                ControlHeight = Height,
                                ControlLock = _lock,
                                ControlName = Name,
                                ControlTop = Top,
                                ControlWidth = Width,
                                TagPicture = pictureBox1.Tag,
                                H = _h
                            };

                return f;
            }
           
        }
        public DataTable Table
        {
            get
            {
                return _table;
            }
            set
            {
                _table = value;
                listBox1.Items.Clear();
                foreach (DataColumn c in _table.Columns)
                {
                    listBox1.Items.Add(c.ColumnName);
                }
            }
        }

        #endregion

        private void RelationCountToolStripMenuItemClick(object sender, EventArgs e)
        {
            MessageBox.Show("" + _table.ParentRelations.Count);
        }

        private void CToolStripMenuItemClick(object sender, EventArgs e)
        {
            MessageBox.Show("" + _table.ChildRelations.Count);

        }















        #region IOpenOrClose Members

        public void Close()
        {
            _h = Height;
            pictureBox1.Image = Resources.Expand_large;
            pictureBox1.Tag = "2";
            panel6.Dock = DockStyle.Bottom;
            Height = 28;
            _expand = false;
        }

        public void Open()
        {
            pictureBox1.Image = Resources.Collapse_large;
            pictureBox1.Tag = "1";
            panel6.Dock = DockStyle.Top;
            Height = _h;
            _expand = true;
        }

        #endregion

     
    }
}
