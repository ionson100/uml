﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace uml
{
    public partial class SettingMap : Form
    {
        string _path;
        public SettingMap()
        {
            InitializeComponent();
        }

        private void SettingMapLoad(object sender, EventArgs e)
        {
            //Settings1.Default.BideriectionalType = 1;
            //Settings1.Default.Save();
           
            checkBox_limit.Checked = Settings1.Default.UsingLimit;
            textBox_limit.Text = Settings1.Default.LimitRows.ToString(CultureInfo.InvariantCulture);
            textBox_limit.ReadOnly = !textBox_limit.ReadOnly;
            textBox_namespace.Text = Settings1.Default.namecpase;
            textBox_assembly.Text = Settings1.Default.assembly;
            checkBox_help.Checked = Settings1.Default.help;
            checkBox_overrites.Checked = Settings1.Default.overrides;
            checkBox_relation.Checked = Settings1.Default.relation;
            _path = Settings1.Default.path;
            folderBrowserDialog1.SelectedPath = _path;
           
            foreach (string s in Enum.GetNames(typeof(MapperCardsharp.BiderictionalType)))
            {
                MappType.Items.Add(s);
            }
        //    int ii=
             MappType.SelectedIndex=MappType.Items.IndexOf(Enum.GetName(typeof(MapperCardsharp.BiderictionalType),Settings1.Default.BideriectionalType));
             checkBox1.Checked = Settings1.Default.UseBiderctional;
            textBox_limit.ReadOnly =! Settings1.Default.UsingLimit;

        }

        private void CheckBox2CheckedChanged(object sender, EventArgs e)
        {

        }

        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                _path = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Button2Click(object sender, EventArgs e)
        {
            try
            {
                Settings1.Default.LimitRows = int.Parse(textBox_limit.Text);
                Settings1.Default.UsingLimit = checkBox_limit.Checked;
                Settings1.Default.namecpase = textBox_namespace.Text;
                Settings1.Default.assembly = textBox_assembly.Text;
                Settings1.Default.help = checkBox_help.Checked;
                Settings1.Default.overrides = checkBox_overrites.Checked;
                Settings1.Default.relation = checkBox_relation.Checked;
                Settings1.Default.path = _path;
                Settings1.Default.UseBiderctional = checkBox1.Checked;
                Settings1.Default.BideriectionalType = (int)Enum.Parse(typeof(MapperCardsharp.BiderictionalType), MappType.Text);
                Settings1.Default.Save();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
             Close();

        }

        private void CheckBox1CheckedChanged(object sender, EventArgs e)
        {
            MappType.Enabled = ((CheckBox)sender).Checked;
        }

        private void Button1Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CheckBoxLimitCheckedChanged(object sender, EventArgs e)
        {
            textBox_limit.ReadOnly = !checkBox_limit.Checked;
        }
    }
}
