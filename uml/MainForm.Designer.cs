﻿namespace uml
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Fireball.Windows.Forms.LineMarginRender lineMarginRender2 = new Fireball.Windows.Forms.LineMarginRender();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьТаблицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.сгруппироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сгруппироватьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сгруппироватььToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сгруппироватьToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.сохранитьСхемуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьСхемуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.unfoldAllTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.lockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьВсёToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.dataSetTablesCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entryInModeOfTheConstructorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crateBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.CodeEditControl = new Fireball.Windows.Forms.CodeEditorControl();
            this.syntaxDocument2 = new Fireball.Syntax.SyntaxDocument(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.GridView_Data = new System.Windows.Forms.DataGridView();
            this.MenyGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.изменитьДанныеВБазеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.datamenuadd = new System.Windows.Forms.ToolStripButton();
            this.datamenuedit = new System.Windows.Forms.ToolStripButton();
            this.datamenudeleteone = new System.Windows.Forms.ToolStripButton();
            this.datamenudeleteall = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.searchcolumns = new System.Windows.Forms.ToolStripComboBox();
            this.searchTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.dataFreeText = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textSQL = new Fireball.Windows.Forms.CodeEditorControl();
            this.syntaxDocument1 = new Fireball.Syntax.SyntaxDocument(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewEXE = new System.Windows.Forms.DataGridView();
            this.panelTree = new System.Windows.Forms.Panel();
            this.treeView = new System.Windows.Forms.TreeView();
            this.MenuTreeBase = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.создатьДиаграммуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cjToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.panel_H = new System.Windows.Forms.Panel();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gadfafToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.erwerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.werwerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.joinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.contextMenuStrip2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_Data)).BeginInit();
            this.MenyGrid.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEXE)).BeginInit();
            this.panelTree.SuspendLayout();
            this.MenuTreeBase.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "preferences.ico");
            this.imageList1.Images.SetKeyName(1, "Collapse_large.bmp");
            this.imageList1.Images.SetKeyName(2, "eventlogSuccessAudit.ico");
            this.imageList1.Images.SetKeyName(3, "Expand_large.bmp");
            this.imageList1.Images.SetKeyName(4, "PushpinHS.png");
            this.imageList1.Images.SetKeyName(5, "services.ico");
            this.imageList1.Images.SetKeyName(6, "mynetworkplaces.ico");
            this.imageList1.Images.SetKeyName(7, "TableHS.png");
            this.imageList1.Images.SetKeyName(8, "CLSDFOLD.ICO");
            this.imageList1.Images.SetKeyName(9, "asssa.ico");
            this.imageList1.Images.SetKeyName(10, "PrimaryKeyHS.png");
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьТаблицуToolStripMenuItem,
            this.toolStripSeparator2,
            this.сгруппироватьToolStripMenuItem,
            this.сгруппироватьToolStripMenuItem1,
            this.сгруппироватььToolStripMenuItem,
            this.сгруппироватьToolStripMenuItem2,
            this.toolStripSeparator1,
            this.сохранитьСхемуToolStripMenuItem,
            this.загрузитьСхемуToolStripMenuItem,
            this.toolStripSeparator3,
            this.unfoldAllTableToolStripMenuItem,
            this.toolStripMenuItem2,
            this.lockToolStripMenuItem,
            this.удалитьВсёToolStripMenuItem,
            this.toolStripSeparator4,
            this.dataSetTablesCountToolStripMenuItem,
            this.entryInModeOfTheConstructorToolStripMenuItem,
            this.crateBaseToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(231, 336);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip2Opening);
            // 
            // добавитьТаблицуToolStripMenuItem
            // 
            this.добавитьТаблицуToolStripMenuItem.Image = global::uml.Properties.Resources.Edit_RedoHS;
            this.добавитьТаблицуToolStripMenuItem.Name = "добавитьТаблицуToolStripMenuItem";
            this.добавитьТаблицуToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.добавитьТаблицуToolStripMenuItem.Text = "Create table";
            this.добавитьТаблицуToolStripMenuItem.Click += new System.EventHandler(this.ДобавитьТаблицуToolStripMenuItemClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(227, 6);
            // 
            // сгруппироватьToolStripMenuItem
            // 
            this.сгруппироватьToolStripMenuItem.Enabled = false;
            this.сгруппироватьToolStripMenuItem.Image = global::uml.Properties.Resources.FillLeftHS;
            this.сгруппироватьToolStripMenuItem.Name = "сгруппироватьToolStripMenuItem";
            this.сгруппироватьToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.сгруппироватьToolStripMenuItem.Text = "Л_Пердвинуть";
            this.сгруппироватьToolStripMenuItem.Click += new System.EventHandler(this.СгруппироватьToolStripMenuItemClick);
            // 
            // сгруппироватьToolStripMenuItem1
            // 
            this.сгруппироватьToolStripMenuItem1.Enabled = false;
            this.сгруппироватьToolStripMenuItem1.Image = global::uml.Properties.Resources.FillRightHS;
            this.сгруппироватьToolStripMenuItem1.Name = "сгруппироватьToolStripMenuItem1";
            this.сгруппироватьToolStripMenuItem1.Size = new System.Drawing.Size(230, 22);
            this.сгруппироватьToolStripMenuItem1.Text = "П_Предвинуть";
            this.сгруппироватьToolStripMenuItem1.Click += new System.EventHandler(this.СгруппироватьToolStripMenuItem1Click);
            // 
            // сгруппироватььToolStripMenuItem
            // 
            this.сгруппироватььToolStripMenuItem.Enabled = false;
            this.сгруппироватььToolStripMenuItem.Image = global::uml.Properties.Resources.FillUpHS;
            this.сгруппироватььToolStripMenuItem.Name = "сгруппироватььToolStripMenuItem";
            this.сгруппироватььToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.сгруппироватььToolStripMenuItem.Text = "В_Передвинуть";
            this.сгруппироватььToolStripMenuItem.Click += new System.EventHandler(this.СгруппироватььToolStripMenuItemClick);
            // 
            // сгруппироватьToolStripMenuItem2
            // 
            this.сгруппироватьToolStripMenuItem2.Enabled = false;
            this.сгруппироватьToolStripMenuItem2.Image = global::uml.Properties.Resources.FillDownHS;
            this.сгруппироватьToolStripMenuItem2.Name = "сгруппироватьToolStripMenuItem2";
            this.сгруппироватьToolStripMenuItem2.Size = new System.Drawing.Size(230, 22);
            this.сгруппироватьToolStripMenuItem2.Text = "Н_Передвинуть";
            this.сгруппироватьToolStripMenuItem2.Click += new System.EventHandler(this.СгруппироватьToolStripMenuItem2Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(227, 6);
            // 
            // сохранитьСхемуToolStripMenuItem
            // 
            this.сохранитьСхемуToolStripMenuItem.Image = global::uml.Properties.Resources.MoveToFolderHS;
            this.сохранитьСхемуToolStripMenuItem.Name = "сохранитьСхемуToolStripMenuItem";
            this.сохранитьСхемуToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.сохранитьСхемуToolStripMenuItem.Text = "Save scheme";
            this.сохранитьСхемуToolStripMenuItem.Click += new System.EventHandler(this.СохранитьСхемуToolStripMenuItemClick);
            // 
            // загрузитьСхемуToolStripMenuItem
            // 
            this.загрузитьСхемуToolStripMenuItem.Image = global::uml.Properties.Resources.OpenSelectedItemHS;
            this.загрузитьСхемуToolStripMenuItem.Name = "загрузитьСхемуToolStripMenuItem";
            this.загрузитьСхемуToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.загрузитьСхемуToolStripMenuItem.Text = "Загрузить схему";
            this.загрузитьСхемуToolStripMenuItem.Click += new System.EventHandler(this.ЗагрузитьСхемуToolStripMenuItemClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(227, 6);
            // 
            // unfoldAllTableToolStripMenuItem
            // 
            this.unfoldAllTableToolStripMenuItem.Image = global::uml.Properties.Resources.Collapse_large;
            this.unfoldAllTableToolStripMenuItem.Name = "unfoldAllTableToolStripMenuItem";
            this.unfoldAllTableToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.unfoldAllTableToolStripMenuItem.Text = "CollapseAllTable";
            this.unfoldAllTableToolStripMenuItem.Click += new System.EventHandler(this.UnfoldAllTableToolStripMenuItemClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = global::uml.Properties.Resources.Expand_large;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(230, 22);
            this.toolStripMenuItem2.Text = "ExpandAllTable";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.ToolStripMenuItem2Click);
            // 
            // lockToolStripMenuItem
            // 
            this.lockToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.lockToolStripMenuItem.Checked = true;
            this.lockToolStripMenuItem.CheckOnClick = true;
            this.lockToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lockToolStripMenuItem.DoubleClickEnabled = true;
            this.lockToolStripMenuItem.Image = global::uml.Properties.Resources.ProtectFormHS;
            this.lockToolStripMenuItem.Name = "lockToolStripMenuItem";
            this.lockToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.lockToolStripMenuItem.Text = "Lock";
            this.lockToolStripMenuItem.Click += new System.EventHandler(this.LockToolStripMenuItemClick);
            // 
            // удалитьВсёToolStripMenuItem
            // 
            this.удалитьВсёToolStripMenuItem.Image = global::uml.Properties.Resources.delete_16x;
            this.удалитьВсёToolStripMenuItem.Name = "удалитьВсёToolStripMenuItem";
            this.удалитьВсёToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.удалитьВсёToolStripMenuItem.Text = "Delete all";
            this.удалитьВсёToolStripMenuItem.Click += new System.EventHandler(this.УдалитьВсёToolStripMenuItemClick);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(227, 6);
            // 
            // dataSetTablesCountToolStripMenuItem
            // 
            this.dataSetTablesCountToolStripMenuItem.Enabled = false;
            this.dataSetTablesCountToolStripMenuItem.Name = "dataSetTablesCountToolStripMenuItem";
            this.dataSetTablesCountToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.dataSetTablesCountToolStripMenuItem.Text = "DataSetTablesCount";
            this.dataSetTablesCountToolStripMenuItem.Click += new System.EventHandler(this.DataSetTablesCountToolStripMenuItemClick);
            // 
            // entryInModeOfTheConstructorToolStripMenuItem
            // 
            this.entryInModeOfTheConstructorToolStripMenuItem.Name = "entryInModeOfTheConstructorToolStripMenuItem";
            this.entryInModeOfTheConstructorToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.entryInModeOfTheConstructorToolStripMenuItem.Text = "Entry in mode of the constructor";
            this.entryInModeOfTheConstructorToolStripMenuItem.Click += new System.EventHandler(this.EntryInModeOfTheConstructorToolStripMenuItemClick);
            // 
            // crateBaseToolStripMenuItem
            // 
            this.crateBaseToolStripMenuItem.Name = "crateBaseToolStripMenuItem";
            this.crateBaseToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.crateBaseToolStripMenuItem.Text = "Crate base";
            this.crateBaseToolStripMenuItem.Click += new System.EventHandler(this.CrateBaseToolStripMenuItemClick);
            // 
            // SFD
            // 
            this.SFD.Filter = "XML|*.xml|All|*.*";
            // 
            // OFD
            // 
            this.OFD.FileName = "openFileDialog1";
            this.OFD.Filter = "XML|*.xml|All|*.*";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 610);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1226, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.tabControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 457);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1226, 153);
            this.panel3.TabIndex = 7;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1224, 151);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.TabControl1SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.CodeEditControl);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1216, 125);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "sql";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // CodeEditControl
            // 
            this.CodeEditControl.ActiveView = Fireball.Windows.Forms.CodeEditor.ActiveView.BottomRight;
            this.CodeEditControl.AutoListPosition = null;
            this.CodeEditControl.AutoListSelectedText = "a123";
            this.CodeEditControl.AutoListVisible = false;
            this.CodeEditControl.BackColor = System.Drawing.Color.FloralWhite;
            this.CodeEditControl.BorderStyle = Fireball.Windows.Forms.ControlBorderStyle.FixedSingle;
            this.CodeEditControl.CopyAsRTF = false;
            this.CodeEditControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CodeEditControl.Document = this.syntaxDocument2;
            this.CodeEditControl.InfoTipCount = 1;
            this.CodeEditControl.InfoTipPosition = null;
            this.CodeEditControl.InfoTipSelectedIndex = 1;
            this.CodeEditControl.InfoTipVisible = false;
            lineMarginRender2.Bounds = new System.Drawing.Rectangle(19, 0, 19, 16);
            this.CodeEditControl.LineMarginRender = lineMarginRender2;
            this.CodeEditControl.Location = new System.Drawing.Point(3, 3);
            this.CodeEditControl.LockCursorUpdate = false;
            this.CodeEditControl.Name = "CodeEditControl";
            this.CodeEditControl.ReadOnly = true;
            this.CodeEditControl.Saved = false;
            this.CodeEditControl.ShowGutterMargin = false;
            this.CodeEditControl.ShowLineNumbers = false;
            this.CodeEditControl.ShowScopeIndicator = false;
            this.CodeEditControl.Size = new System.Drawing.Size(1210, 119);
            this.CodeEditControl.SmoothScroll = false;
            this.CodeEditControl.SplitviewH = -4;
            this.CodeEditControl.SplitviewV = -4;
            this.CodeEditControl.TabGuideColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(219)))), ((int)(((byte)(214)))));
            this.CodeEditControl.TabIndex = 0;
            this.CodeEditControl.WhitespaceColor = System.Drawing.SystemColors.ControlDark;
            // 
            // syntaxDocument2
            // 
            this.syntaxDocument2.Lines = new string[] {
        "  "};
            this.syntaxDocument2.MaxUndoBufferSize = 1000;
            this.syntaxDocument2.Modified = false;
            this.syntaxDocument2.UndoStep = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.toolStrip2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1216, 125);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.GridView_Data);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 28);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1210, 94);
            this.panel5.TabIndex = 2;
            // 
            // GridView_Data
            // 
            this.GridView_Data.AllowUserToAddRows = false;
            this.GridView_Data.AllowUserToDeleteRows = false;
            this.GridView_Data.AllowUserToOrderColumns = true;
            this.GridView_Data.BackgroundColor = System.Drawing.Color.Azure;
            this.GridView_Data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView_Data.ContextMenuStrip = this.MenyGrid;
            this.GridView_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridView_Data.Location = new System.Drawing.Point(0, 0);
            this.GridView_Data.Name = "GridView_Data";
            this.GridView_Data.Size = new System.Drawing.Size(1210, 94);
            this.GridView_Data.TabIndex = 0;
            this.GridView_Data.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.GridViewDataDataError);
            // 
            // MenyGrid
            // 
            this.MenyGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.изменитьДанныеВБазеToolStripMenuItem});
            this.MenyGrid.Name = "MenyGrid";
            this.MenyGrid.Size = new System.Drawing.Size(205, 26);
            this.MenyGrid.Opened += new System.EventHandler(this.MenyGridOpened);
            // 
            // изменитьДанныеВБазеToolStripMenuItem
            // 
            this.изменитьДанныеВБазеToolStripMenuItem.Name = "изменитьДанныеВБазеToolStripMenuItem";
            this.изменитьДанныеВБазеToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.изменитьДанныеВБазеToolStripMenuItem.Text = "Изменить данные в базе?";
            this.изменитьДанныеВБазеToolStripMenuItem.Click += new System.EventHandler(this.ИзменитьДанныеВБазеToolStripMenuItemClick);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datamenuadd,
            this.datamenuedit,
            this.datamenudeleteone,
            this.datamenudeleteall,
            this.toolStripSeparator7,
            this.searchcolumns,
            this.searchTextBox,
            this.toolStripSeparator8,
            this.dataFreeText,
            this.toolStripButton13,
            this.toolStripSeparator9});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1210, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "datafreeText";
            // 
            // datamenuadd
            // 
            this.datamenuadd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.datamenuadd.Image = global::uml.Properties.Resources.add_unl;
            this.datamenuadd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.datamenuadd.Name = "datamenuadd";
            this.datamenuadd.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.datamenuadd.Size = new System.Drawing.Size(32, 22);
            this.datamenuadd.Text = "Добавть строку";
            // 
            // datamenuedit
            // 
            this.datamenuedit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.datamenuedit.Enabled = false;
            this.datamenuedit.Image = global::uml.Properties.Resources.edit_uml;
            this.datamenuedit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.datamenuedit.Name = "datamenuedit";
            this.datamenuedit.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.datamenuedit.Size = new System.Drawing.Size(32, 22);
            this.datamenuedit.Text = "Редактировать строку";
            // 
            // datamenudeleteone
            // 
            this.datamenudeleteone.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.datamenudeleteone.Image = global::uml.Properties.Resources.delete_one;
            this.datamenudeleteone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.datamenudeleteone.Name = "datamenudeleteone";
            this.datamenudeleteone.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.datamenudeleteone.Size = new System.Drawing.Size(32, 22);
            this.datamenudeleteone.Text = "Удалить сстроку";
            // 
            // datamenudeleteall
            // 
            this.datamenudeleteall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.datamenudeleteall.Image = global::uml.Properties.Resources.delete_all;
            this.datamenudeleteall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.datamenudeleteall.Name = "datamenudeleteall";
            this.datamenudeleteall.Padding = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.datamenudeleteall.Size = new System.Drawing.Size(32, 22);
            this.datamenudeleteall.Text = "Удалить всё";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // searchcolumns
            // 
            this.searchcolumns.Name = "searchcolumns";
            this.searchcolumns.Size = new System.Drawing.Size(121, 25);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(100, 25);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // dataFreeText
            // 
            this.dataFreeText.Name = "dataFreeText";
            this.dataFreeText.Size = new System.Drawing.Size(300, 25);
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Image = global::uml.Properties.Resources.find;
            this.toolStripButton13.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton13.Text = "toolStripButton13";
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Controls.Add(this.splitter3);
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1216, 125);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "SQLExecute";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textSQL);
            this.panel4.Controls.Add(this.button1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(543, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(673, 125);
            this.panel4.TabIndex = 2;
            // 
            // textSQL
            // 
            this.textSQL.ActiveView = Fireball.Windows.Forms.CodeEditor.ActiveView.BottomRight;
            this.textSQL.AutoListPosition = null;
            this.textSQL.AutoListSelectedText = "a123";
            this.textSQL.AutoListVisible = false;
            this.textSQL.BorderStyle = Fireball.Windows.Forms.ControlBorderStyle.RaisedThin;
            this.textSQL.ChildBorderStyle = Fireball.Windows.Forms.ControlBorderStyle.Etched;
            this.textSQL.CopyAsRTF = false;
            this.textSQL.Document = this.syntaxDocument1;
            this.textSQL.HighLightActiveLine = true;
            this.textSQL.InfoTipCount = 1;
            this.textSQL.InfoTipPosition = null;
            this.textSQL.InfoTipSelectedIndex = 1;
            this.textSQL.InfoTipVisible = false;
            this.textSQL.LineMarginRender = lineMarginRender2;
            this.textSQL.Location = new System.Drawing.Point(6, 8);
            this.textSQL.LockCursorUpdate = false;
            this.textSQL.Name = "textSQL";
            this.textSQL.Saved = false;
            this.textSQL.ShowGutterMargin = false;
            this.textSQL.ShowLineNumbers = false;
            this.textSQL.ShowScopeIndicator = false;
            this.textSQL.Size = new System.Drawing.Size(611, 368);
            this.textSQL.SmoothScroll = false;
            this.textSQL.SplitviewH = -4;
            this.textSQL.SplitviewV = -4;
            this.textSQL.TabGuideColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(219)))), ((int)(((byte)(214)))));
            this.textSQL.TabIndex = 3;
            this.textSQL.WhitespaceColor = System.Drawing.SystemColors.ControlDark;
            // 
            // syntaxDocument1
            // 
            this.syntaxDocument1.Lines = new string[] {
        " "};
            this.syntaxDocument1.MaxUndoBufferSize = 1000;
            this.syntaxDocument1.Modified = false;
            this.syntaxDocument1.UndoStep = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(623, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(43, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Goo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // splitter3
            // 
            this.splitter3.Location = new System.Drawing.Point(540, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(3, 125);
            this.splitter3.TabIndex = 1;
            this.splitter3.TabStop = false;
            this.splitter3.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.Splitter3SplitterMoved);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dataGridViewEXE);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(540, 125);
            this.panel1.TabIndex = 0;
            // 
            // dataGridViewEXE
            // 
            this.dataGridViewEXE.BackgroundColor = System.Drawing.Color.Azure;
            this.dataGridViewEXE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEXE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewEXE.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewEXE.Name = "dataGridViewEXE";
            this.dataGridViewEXE.Size = new System.Drawing.Size(540, 125);
            this.dataGridViewEXE.TabIndex = 0;
            this.dataGridViewEXE.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewExeCellContentClick);
            this.dataGridViewEXE.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridViewExeDataError1);
            // 
            // panelTree
            // 
            this.panelTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTree.Controls.Add(this.treeView);
            this.panelTree.Controls.Add(this.panel2);
            this.panelTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelTree.Location = new System.Drawing.Point(0, 0);
            this.panelTree.Name = "panelTree";
            this.panelTree.Size = new System.Drawing.Size(164, 454);
            this.panelTree.TabIndex = 9;
            // 
            // treeView
            // 
            this.treeView.BackColor = System.Drawing.Color.Azure;
            this.treeView.ContextMenuStrip = this.MenuTreeBase;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageIndex = 8;
            this.treeView.ImageList = this.imageList1;
            this.treeView.Location = new System.Drawing.Point(0, 28);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(162, 424);
            this.treeView.TabIndex = 5;
            this.treeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.TreeViewBeforeSelect);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeViewAfterSelect);
            // 
            // MenuTreeBase
            // 
            this.MenuTreeBase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьДиаграммуToolStripMenuItem,
            this.createBaseToolStripMenuItem,
            this.cjToolStripMenuItem,
            this.dropBaseToolStripMenuItem,
            this.toolStripMenuItem1});
            this.MenuTreeBase.Name = "MenuTreeBase";
            this.MenuTreeBase.Size = new System.Drawing.Size(175, 114);
            this.MenuTreeBase.Opening += new System.ComponentModel.CancelEventHandler(this.MenuTreeBaseOpening);
            // 
            // создатьДиаграммуToolStripMenuItem
            // 
            this.создатьДиаграммуToolStripMenuItem.Name = "создатьДиаграммуToolStripMenuItem";
            this.создатьДиаграммуToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.создатьДиаграммуToolStripMenuItem.Text = "Создать диаграмму";
            this.создатьДиаграммуToolStripMenuItem.Click += new System.EventHandler(this.СоздатьДиаграммуToolStripMenuItemClick);
            // 
            // createBaseToolStripMenuItem
            // 
            this.createBaseToolStripMenuItem.Image = global::uml.Properties.Resources.Edit_RedoHS;
            this.createBaseToolStripMenuItem.Name = "createBaseToolStripMenuItem";
            this.createBaseToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.createBaseToolStripMenuItem.Text = "Create Base";
            this.createBaseToolStripMenuItem.Click += new System.EventHandler(this.CreateBaseToolStripMenuItemClick);
            // 
            // cjToolStripMenuItem
            // 
            this.cjToolStripMenuItem.Image = global::uml.Properties.Resources.Edit_RedoHS;
            this.cjToolStripMenuItem.Name = "cjToolStripMenuItem";
            this.cjToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.cjToolStripMenuItem.Text = "CreateTable";
            this.cjToolStripMenuItem.Click += new System.EventHandler(this.CjToolStripMenuItemClick);
            // 
            // dropBaseToolStripMenuItem
            // 
            this.dropBaseToolStripMenuItem.Image = global::uml.Properties.Resources.delete_16x;
            this.dropBaseToolStripMenuItem.Name = "dropBaseToolStripMenuItem";
            this.dropBaseToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.dropBaseToolStripMenuItem.Text = "Drop Base";
            this.dropBaseToolStripMenuItem.Click += new System.EventHandler(this.DropBaseToolStripMenuItemClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = global::uml.Properties.Resources.delete_16x;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(174, 22);
            this.toolStripMenuItem1.Text = "DropTable";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItem1Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(162, 28);
            this.panel2.TabIndex = 4;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(160, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::uml.Properties.Resources.Collapse_large;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "CollapseAll";
            this.toolStripButton2.Click += new System.EventHandler(this.ToolStripButton2Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::uml.Properties.Resources.Expand_large;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.ToolTipText = "ExpandAll";
            this.toolStripButton3.Click += new System.EventHandler(this.ToolStripButton3Click);
            // 
            // panel_H
            // 
            this.panel_H.AutoScroll = true;
            this.panel_H.ContextMenuStrip = this.contextMenuStrip2;
            this.panel_H.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_H.Location = new System.Drawing.Point(167, 0);
            this.panel_H.Name = "panel_H";
            this.panel_H.Size = new System.Drawing.Size(1059, 454);
            this.panel_H.TabIndex = 11;
            this.panel_H.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel1MouseDown);
            this.panel_H.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel1MouseUp);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator6,
            this.toolStripButton5,
            this.toolStripButton8,
            this.toolStripSeparator5,
            this.toolStripButton6,
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripButton9});
            this.toolStrip3.Location = new System.Drawing.Point(167, 0);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip3.Size = new System.Drawing.Size(1059, 25);
            this.toolStrip3.TabIndex = 13;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(19, 22);
            this.toolStripLabel1.Text = "    ";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton5.Image = global::uml.Properties.Resources.group;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(101, 22);
            this.toolStripButton5.Text = "Making Connection";
            this.toolStripButton5.ToolTipText = "Making Connection";
            this.toolStripButton5.Click += new System.EventHandler(this.ToolStripButton5Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton8.Image = global::uml.Properties.Resources.repair;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(65, 22);
            this.toolStripButton8.Tag = "Setting";
            this.toolStripButton8.Text = "SettingMap";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.ToolStripButton8Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton6.Image = global::uml.Properties.Resources.TableHS;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(61, 22);
            this.toolStripButton6.Text = "SimplyMap";
            this.toolStripButton6.ToolTipText = "SimplyMapp";
            this.toolStripButton6.Click += new System.EventHandler(this.ToolStripButton6Click);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton10.Image = global::uml.Properties.Resources.EditTableHS;
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(58, 22);
            this.toolStripButton10.Text = "ConfigFile";
            this.toolStripButton10.ToolTipText = "ConfigFile";
            this.toolStripButton10.Click += new System.EventHandler(this.ToolStripButton10Click);
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(78, 22);
            this.toolStripButton11.Text = "DirectotyData";
            this.toolStripButton11.Click += new System.EventHandler(this.ToolStripButton11Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Enabled = false;
            this.toolStripButton9.Image = global::uml.Properties.Resources.FormRunHS;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "RunAll";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gadfafToolStripMenuItem,
            this.erwerToolStripMenuItem,
            this.werwerToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1226, 24);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // gadfafToolStripMenuItem
            // 
            this.gadfafToolStripMenuItem.Name = "gadfafToolStripMenuItem";
            this.gadfafToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.gadfafToolStripMenuItem.Text = "Exhibit";
            // 
            // erwerToolStripMenuItem
            // 
            this.erwerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sToolStripMenuItem,
            this.settingToolStripMenuItem,
            this.connectToolStripMenuItem});
            this.erwerToolStripMenuItem.Name = "erwerToolStripMenuItem";
            this.erwerToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.erwerToolStripMenuItem.Text = "Setting";
            // 
            // sToolStripMenuItem
            // 
            this.sToolStripMenuItem.Image = global::uml.Properties.Resources.repair;
            this.sToolStripMenuItem.Name = "sToolStripMenuItem";
            this.sToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.sToolStripMenuItem.Text = "Map";
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.settingToolStripMenuItem.Text = "Design";
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.Image = global::uml.Properties.Resources.group;
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.connectToolStripMenuItem.Text = "Connect";
            // 
            // werwerToolStripMenuItem
            // 
            this.werwerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleToolStripMenuItem,
            this.joinToolStripMenuItem,
            this.subClassToolStripMenuItem,
            this.componentToolStripMenuItem});
            this.werwerToolStripMenuItem.Name = "werwerToolStripMenuItem";
            this.werwerToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.werwerToolStripMenuItem.Text = "Mapp";
            // 
            // simpleToolStripMenuItem
            // 
            this.simpleToolStripMenuItem.Image = global::uml.Properties.Resources.TableHS;
            this.simpleToolStripMenuItem.Name = "simpleToolStripMenuItem";
            this.simpleToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.simpleToolStripMenuItem.Text = "Simple";
            // 
            // joinToolStripMenuItem
            // 
            this.joinToolStripMenuItem.Image = global::uml.Properties.Resources.TableHS;
            this.joinToolStripMenuItem.Name = "joinToolStripMenuItem";
            this.joinToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.joinToolStripMenuItem.Text = "Join";
            // 
            // subClassToolStripMenuItem
            // 
            this.subClassToolStripMenuItem.Image = global::uml.Properties.Resources.TableHS;
            this.subClassToolStripMenuItem.Name = "subClassToolStripMenuItem";
            this.subClassToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.subClassToolStripMenuItem.Text = "SubClass";
            // 
            // componentToolStripMenuItem
            // 
            this.componentToolStripMenuItem.Image = global::uml.Properties.Resources.TableHS;
            this.componentToolStripMenuItem.Name = "componentToolStripMenuItem";
            this.componentToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.componentToolStripMenuItem.Text = "Component";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.contentsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.contentsToolStripMenuItem.Text = "Contents";
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(164, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 454);
            this.splitter2.TabIndex = 10;
            this.splitter2.TabStop = false;
            this.splitter2.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.Splitter2SplitterMoved);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 454);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1226, 3);
            this.splitter1.TabIndex = 8;
            this.splitter1.TabStop = false;
            this.splitter1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.Splitter1SplitterMoved);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuBar;
            this.ClientSize = new System.Drawing.Size(1226, 632);
            this.Controls.Add(this.toolStrip3);
            this.Controls.Add(this.panel_H);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelTree);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MapperCardsharp";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HeadFormClosing);
            this.Load += new System.EventHandler(this.Form1Load);
            this.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.Form1ControlAdded);
            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.Form1ControlRemoved);
            this.contextMenuStrip2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridView_Data)).EndInit();
            this.MenyGrid.ResumeLayout(false);
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEXE)).EndInit();
            this.panelTree.ResumeLayout(false);
            this.MenuTreeBase.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem добавитьТаблицуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьВсёToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сгруппироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сгруппироватьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem сгруппироватььToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сгруппироватьToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem lockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьСхемуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузитьСхемуToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.ToolStripMenuItem dataSetTablesCountToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelTree;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel_H;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView GridView_Data;
        private System.Windows.Forms.ContextMenuStrip MenyGrid;
        private System.Windows.Forms.ToolStripMenuItem изменитьДанныеВБазеToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip MenuTreeBase;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gadfafToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem erwerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem werwerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem joinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem componentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dropBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cjToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьДиаграммуToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.DataGridView dataGridViewEXE;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripMenuItem entryInModeOfTheConstructorToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private Fireball.Windows.Forms.CodeEditorControl textSQL;
        private Fireball.Syntax.SyntaxDocument syntaxDocument1;
        private Fireball.Windows.Forms.CodeEditorControl CodeEditControl;
        private Fireball.Syntax.SyntaxDocument syntaxDocument2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem unfoldAllTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crateBaseToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton datamenuadd;
        private System.Windows.Forms.ToolStripButton datamenuedit;
        private System.Windows.Forms.ToolStripButton datamenudeleteone;
        private System.Windows.Forms.ToolStripButton datamenudeleteall;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripComboBox searchcolumns;
        private System.Windows.Forms.ToolStripTextBox searchTextBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ToolStripTextBox dataFreeText;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
    }
}

