﻿using System;
using System.Windows.Forms;

namespace uml
{
    public partial class MySQLConnect : Form
    {
        string connstring;
        public MySQLConnect(string connstring)
        {
            InitializeComponent( );
            this.connstring = connstring;
        }

        private void MySQLConnectLoad(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(connstring)) return;
           // Data Source=localhost;Database=;User ID=root;Password=;port=3306;charset=utf8
            var s = connstring.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            textBox_host.Text = s[0].Remove(0, s[0].IndexOf('=')+1);
            textBox_user.Text = s[2].Remove(0, s[2].IndexOf('=') + 1);
            textBox_pasword.Text = s[3].Remove(0, s[3].IndexOf('=') + 1);
            textBox_Port.Text = s[4].Remove(0, s[4].IndexOf('=') + 1);
            textBox_Charset.Text = s[5].Remove(0, s[5].IndexOf('=') + 1);
        }

        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string conn="Data Source=" +
                  textBox_host.Text +
                  ";Database=;User ID=" +
                  textBox_user.Text + ";Password=" +
                  textBox_pasword.Text + ";port=" +
                  textBox_Port.Text + ";charset=" +
                  textBox_Charset.Text;
            using (var con= new MySql.Data.MySqlClient.MySqlConnection(conn))
            {
                try
                {
                    con.Open();
                    if (con.Ping())
                        MessageBox.Show("Ok");

                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, (MessageBoxOptions)0);
                }
                finally
                {
                    con.Close();
                }

            }
        }
        public string ConnString
        {
            get
            {
                return "Data Source=" +
                 textBox_host.Text +
                 ";Database=;User ID=" +
                 textBox_user.Text + ";Password=" +
                 textBox_pasword.Text + ";port=" +
                 textBox_Port.Text + ";charset=" +
                 textBox_Charset.Text;
            }
        }
    }
}
