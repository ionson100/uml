﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using MoverUML;
using System.Diagnostics;
using uml.Properties;

namespace uml
{
    public partial class MapperComponent : Form
    {
/*
        List<string> _l = new List<string>();
*/
        public MapperComponent()
        {
            InitializeComponent();
        }

        private void MapperComponentLoad(object sender, EventArgs e)
        {
           foreach(DataTable t in Singlton.DataSet.Tables)
           {
               comboBox_table.Items.Add(t.TableName);
           }
        }

        private void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox_1.Items.Clear();
            var t=Util.Ii.ShowFields(comboBox_table.Text);
            foreach (DataRow r in t.Rows)
            {
                listBox_1.Items.Add(r[0].ToString());

            }
        }

        private void Button1Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox_1, listBox_2);
           
        }

        private void Button2Click(object sender, EventArgs e)
        {
            Util.Carrying(listBox_2, listBox_1);
            
        }

        private void Button5Click(object sender, EventArgs e)
        {
            if (listBox_1.Items.Count == 0 | listBox_2.Items.Count == 0)
            {
                MessageBox.Show(Resources.MapperComponent_Button5Click_No_Data_for_map);
                return;
            }
            var ltm = new List<string>();
            foreach (string s in listBox_2.Items)
            {
                ltm.Add(s);
            }

            try
            {
                MapperCardsharp.Mapp.ComponentMap(Application.StartupPath + "/pattern/areal.xml",
                    Settings1.Default.path,
                    Singlton.DataSet,
                    comboBox_table.Text, 
                    textBox_class_name.Text,
                    ltm,
                    Settings1.Default.help,
                    Settings1.Default.overrides,
                    Settings1.Default.namecpase,
                    Settings1.Default.assembly);
                Process.Start(Settings1.Default.path);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }

        }

        private void Button3Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
