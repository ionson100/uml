﻿using System.Collections.Generic;
using System.Data;

namespace MapperCardsharp
{
  public  class DiagnostEventA
    {
      List<DataTable> _tableRelation = new List<DataTable>();
      DataRelationCollection _col;
      public List<DataTable> TableL
      {
          
          get { return _tableRelation; }
          set {_tableRelation=value; }
      }
      public DataRelationCollection Col
      {

          get { return _col; }
          set { _col = value; }
      }

    }
}
