﻿using System.Collections.Generic;
using System.Data;
using System.Xml;

namespace MapperCardsharp
{
    class ExtendedPropertiesWriter
    {
        readonly List<DataTable> _ltable = new List<DataTable>();
        public ExtendedPropertiesWriter(DataSet ds, string filePattern,BiderictionalType type)
        {
           
           
            var document = new XmlDocument();
            document.Load(filePattern);
            var lastChild = document.LastChild;
           
            var tableNameActiw = new List<string>();
            foreach (DataRelation r in ds.Relations)
            {
                if (tableNameActiw.IndexOf(r.ParentTable.TableName) == -1) tableNameActiw.Add(r.ParentTable.TableName);
                if (tableNameActiw.IndexOf(r.ChildTable.TableName) == -1) tableNameActiw.Add(r.ChildTable.TableName);
            }
            foreach (DataTable t in ds.Tables)
            {
               
                if(tableNameActiw.IndexOf(t.TableName)==-1)
                t.ExtendedProperties.Clear();
                else
                    t.ExtendedProperties.Add("list", new List<string[]>());
            }
            
        
            var xml = "";
            switch (type)
            {
                case BiderictionalType.Set:
                    xml = "BidirectionalXML_one_to_many";
                    break;
                case BiderictionalType.Bag:
                    xml = "Bag";
                    break;
                case BiderictionalType.List:
                    xml = "List";
                    break;
            }


            var xmlNodeList = lastChild.SelectNodes("BidirectionalXML_many_to_one");
            if (xmlNodeList != null)
            {
                var bidirectionalXmlManyToOne = xmlNodeList[0].InnerText;
                var selectNodes = lastChild.SelectNodes("Bidirectionzl_CS_many_to_one");
                if (selectNodes != null)
                {
                    var bidirectionzlCsManyToOne = selectNodes[0].InnerText;
                    var nodeList = lastChild.SelectNodes("Bidirectiona_property_many_to_one");
                    if (nodeList != null)
                    {
                        var bidirectionaPropertyManyToOne = nodeList[0].InnerText;

                        var nodes = lastChild.SelectNodes(xml);
                        if (nodes != null)
                        {
                            var bidirectionalXmlOneToMany = nodes[0].InnerText;
                            var list = lastChild.SelectNodes("Bidirectionzl_CS_one_to_many");
                            if (list != null)
                            {
                                var bidirectionzlCsOneToMany = list[0].InnerText;
                                var xmlNodeList1 = lastChild.SelectNodes("Bidirectiona_property_one_to_many");
                                if (xmlNodeList1 != null)
                                {
                                    var bidirectionaPropertyOneToMany = xmlNodeList1[0].InnerText;

          


                                    foreach (DataRelation r in ds.Relations)
                                    {
                                        if (ds.Tables[r.ChildTable.TableName] != null && ds.Tables[r.ParentTable.TableName] != null)
                                        {
                                            if (_ltable.IndexOf(r.ParentTable) == -1) _ltable.Add(r.ParentTable);
                                            if (_ltable.IndexOf(r.ChildTable) == -1) _ltable.Add(r.ChildTable);


                                            var s = new[] { 
                                                                          bidirectionalXmlManyToOne.Replace("#NAME_CLASS",r.ParentTable.TableName).Replace("#NAME__COLUMN",r.ParentColumns[0].ColumnName), 
                                                                          bidirectionzlCsManyToOne.Replace("#NAME_CLASS",r.ParentTable.TableName),
                                                                          bidirectionaPropertyManyToOne.Replace("#NAME_CLASS",r.ParentTable.TableName) };

                                            ((List<string[]>)ds.Tables[r.ChildTable.TableName].ExtendedProperties["list"]).Add(s);

                                          


                                            var s1 = new[] { 
                                                                           bidirectionalXmlOneToMany.Replace("#NAME_CLASS",r.ChildTable.TableName).Replace("#NAME__COLUMN",r.ChildColumns[0].ColumnName).Replace("#BIDIRECTIONAL_TYPE",type.ToString().ToLower()), 
                                                                           bidirectionzlCsOneToMany.Replace("#NAME_CLASS",r.ChildTable.TableName).Replace("#BIDIRECTIONAL_TYPE",type.ToString()),
                                                                           bidirectionaPropertyOneToMany.Replace("#NAME_CLASS",r.ChildTable.TableName).Replace("#BIDIRECTIONAL_TYPE",type.ToString()) };
                                         
                                            ((List<string[]>)ds.Tables[r.ParentTable.TableName].ExtendedProperties["list"]).Add(s1);


                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
            //}
            // }
        }
        public List<DataTable> TableList
        {
            get { return _ltable; }
        }
    }
}
