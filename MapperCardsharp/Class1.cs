﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;

namespace MapperCardsharp
{
    public enum BiderictionalType { Set=1,List=2,Bag=3 }
    public delegate void DiactostRelation(DiagnostEventA e);
    public class Mapp
    {
        public static event DiactostRelation Ee;
        //List<string> L = new List<string>();
        static string _key = "";
        /// <summary>
        /// конструктор закрытый
        /// </summary>
         Mapp()
        {
        }
        /// <summary>
        /// Гененрация базоваого типа
        /// </summary>
        /// <param name="filePattern">путь к файлу areal.xml паттерны шаблоны генерации</param>
        /// <param name="dirictoryName">путь к папке куда писать фалы</param>
        /// <param name="ds">DataSet таблицы общие</param>
        /// <param name="tableMappName">список названий таблиц для маппинга</param>
        /// <param name="showHelp">включать хелп</param>
         /// <param name="showOverrides"> включать перегрузку Overrides </param>
         /// <param name="Namespace">Namespace приложения</param>
         /// <param name="assembly">Assembly приложения</param>
        /// <param name="type">тип генерации</param>
         public static void SinpleMapp(string filePattern,
                     string dirictoryName,
                    DataSet ds,
               List<string> tableMappName,
                       bool showHelp,
                       bool showOverrides,
                     string Namespace,
                     string assembly,
          BiderictionalType type

             )

         {

             var document = new XmlDocument();
             document.Load(filePattern);
             var lastChild = document.LastChild;
            
             var builder = new StringBuilder("");
      
                 var dataSet = ds;
            var xmlNodeList = lastChild.SelectNodes("head");
            if (xmlNodeList != null)
            {
                string innerText = xmlNodeList[0].InnerText;
                var selectNodes = lastChild.SelectNodes("colum");
                if (selectNodes != null)
                {
                    string str2 = selectNodes[0].InnerText;
                    var nodeList = lastChild.SelectNodes("bootom");
                    if (nodeList != null)
                    {
                        string str3 = nodeList[0].InnerText;


                        foreach (DataTable t in ds.Tables)
                        {
                            t.ExtendedProperties.Clear();
                        }
            
                
                        var wtritter = new ExtendedPropertiesWriter(ds, filePattern, type);

                        var nodes = lastChild.SelectNodes("uncus");
                        var index = 0;
                        if (nodes != null)
                        {
                            var strArray = nodes[0].InnerText.Split(new[] { ';' });
                            foreach (DataTable table in dataSet.Tables)
                            {
                                if (tableMappName.IndexOf(table.TableName) == -1) continue;
                                var str4 = table.TableName;// strArray2[index].Remove(0, strArray2[0].IndexOf("FROM") + 4).Substring(0, strArray2[index].Remove(0, strArray2[0].IndexOf("FROM") + 4).IndexOf("LIMIT")).Trim();
                                var builder2 = new StringBuilder("");
                                foreach (DataColumn column in table.Columns)
                                {
                                    if (column.Unique)
                                    {
                                        if (column.ColumnName==table.PrimaryKey[0].ColumnName)
                                        {
                                            builder2.Append(strArray[1].Replace("\"Name\"", "\"" + column.ColumnName + "\"").Replace("\"Type\"", "\"" + column.DataType.Name + "\""));
                                            _key = column.ColumnName;
                                        }
                                        else
                                        {
                                            builder2.Append(strArray[0].Replace("\"Name\"", "\"" + column.ColumnName + "\"").Replace("\"Type\"", "\"" + column.DataType.Name + "\"").Replace("\"Length\"", "\"" + column.MaxLength + "\""));
                                            _key = column.ColumnName;
                                        }
                                    }
                                    else if (column.MaxLength == -1)
                                    {
                                        var str5 = str2.Replace("Name", column.ColumnName).Replace("Type", column.DataType.Name.Remove(0, column.DataType.Name.IndexOf('.') + 1)).Replace("\"Length\"", "\"" + column.MaxLength + "\"");
                                        builder2.Append("         " + str5.Substring(0, str5.IndexOf("length=", StringComparison.Ordinal)).Trim() + "/>");
                                        // Mapp.key = column.ColumnName;
                                    }
                                    else
                                    {
                                        builder2.Append(str2.Replace("Name", column.ColumnName).Replace("Type", column.DataType.Name.Remove(0, column.DataType.Name.IndexOf('.') + 1)).Replace("\"Length\"", "\"" + column.MaxLength + "\""));
                                        // Mapp.key = column.ColumnName;
                                    }
                                    builder2.Append("\r\n");
                                }
                                string str6 = "";
                                if (showHelp)
                                {
                                    var list = lastChild.SelectNodes("helper");
                                    if (list != null)
                                        str6 = list[0].InnerText;
                                }


                                if (table.ExtendedProperties["list"] != null)
                                {
                                    builder2.Append("\r\n");
                                    foreach (var s in (List<string[]>)table.ExtendedProperties["list"])///////////////////////////////////////////
                                    {
                                        if (builder2.ToString().IndexOf(s[0], StringComparison.Ordinal) == -1)
                                        {
                                            if (type == BiderictionalType.List)
                                            {
                                                builder2.Append(s[0].Replace("#ID_NAME__COLUMN",_key) + "\r\n");
                                            }
                                            else
                                            {
                                                builder2.Append(s[0] + "\r\n");
                                            }
                                        }
                                    }
                                }

                                File.WriteAllText(dirictoryName + "/" + str4 + ".hbm.xml", innerText.Replace("\"Class\"", "\"" + str4 + "\"").Replace("\"Table\"", "\"" + str4 + "\"").Replace("\"Namespace\"", "\"" + Namespace + "\"").Replace("\"Assembly\"", "\"" + assembly + "\"") + "\r\n" + builder2 + "\r\n" + str3 + "\r\n\r\n\r\n" + str6, Encoding.UTF8);
                                index++;
                            }
                            var xmlNodeList1 = lastChild.SelectNodes("overrides");
                            if (xmlNodeList1 != null)
                            {
                                var str7 = xmlNodeList1[0].InnerText;
                                builder.ToString().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                index = 0;
                                foreach (DataTable table2 in dataSet.Tables)
                                {


                                    _key = table2.PrimaryKey[0].ColumnName;//new cod ion
                     


                                    if (tableMappName.IndexOf(table2.TableName) == -1) continue;
                                    var builder3 = new StringBuilder();
                                    var builder4 = new StringBuilder();
                                    var newValue = table2.TableName;// strArray2[index].Remove(0, strArray2[0].IndexOf("FROM") + 4).Substring(0, strArray2[index].Remove(0, strArray2[0].IndexOf("FROM") + 4).IndexOf("LIMIT")).Trim();
                                    foreach (DataColumn column2 in table2.Columns)
                                    {
                                        builder4.Append("        private " + column2.DataType.Name + " _" + column2.ColumnName + ";\r\n");
                                        builder3.Append("        public virtual " + column2.DataType.Name + " " + column2.ColumnName + "\r\n        {\r\n            get { return _" + column2.ColumnName + "; }\r\n");
                                        builder3.Append("            set { _" + column2.ColumnName + "= value; }\r\n        }\r\n");
                                    }





                                    if (table2.ExtendedProperties["list"] != null)/////////////////////////////////////////////////////////
                                    {
                      
                                        foreach (var s in (List<string[]>)table2.ExtendedProperties["list"])
                                        {
                                            if (builder4.ToString().IndexOf(s[2], StringComparison.Ordinal) == -1)
                                                builder4.Append(s[2] + "\r\n");
                                        }
                                    }

                                    var builder5 = new StringBuilder("");
                                    builder5.Append("/*Code is generated by program MapperCardsharp*/\r\n using System;\r\n using System.Collections.Generic;\r\n using System.Text;\r\n using Iesi.Collections;\r\n using NHibernate.Mapping;");
                                    builder5.Append("\r\n\r\nnamespace " + Namespace + "\r\n{\r\n");
                                    builder5.Append("    public class " + newValue + "\r\n    {\r\n");
                                    builder5.Append(builder4 + "\r\n");
                                    builder5.Append("        public " + newValue + "()\r\n        {\r\n        }\r\n\r\n");
                                    builder5.Append(builder3);

                                    if (table2.ExtendedProperties["list"] != null)////////////////////////////////////////////////
                                    {
                                        builder4.Append("\r\n");
                                        foreach (string[] s in (List<string[]>)table2.ExtendedProperties["list"])
                                        {
                                            if (builder5.ToString().IndexOf(s[1], StringComparison.Ordinal) == -1) 
                                                builder5.Append(s[1] + "\r\n");
                                        }
                                    }

                                    if (showOverrides)
                                    {
                                        builder5.Append(str7.Replace("#CLASS", newValue).Replace("#KEY", _key) + "\r\n");
                                    }
                                    builder5.Append("    }\r\n}");
                                    File.WriteAllText(dirictoryName + "/" + newValue + ".cs", builder5.ToString(), Encoding.UTF8);
                                    index++;
                    
                                }
                            }
                        }
                        if (Ee != null)
                        {
                            var eee = new DiagnostEventA {Col = ds.Relations, TableL = wtritter.TableList};
                            Ee(eee);
                        }
                    }
                }
            }
         }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePattern"></param>
        /// <param name="dirictoryName"></param>
        /// <param name="ds"></param>
        /// <param name="tableNameHed"></param>
        /// <param name="tableMappNameJoin"></param>
        /// <param name="showHelp"></param>
        /// <param name="showOverrides"></param>
        /// <param name="Namespace"></param>
        /// <param name="assembly"></param>

         public static void JoinMapp(string filePattern,
                     string dirictoryName,
                    DataSet ds,
                     string tableNameHed,
               List<string> tableMappNameJoin,
                       bool showHelp,
                       bool showOverrides,
                     string Namespace,
                     string assembly)
         {
            var document = new XmlDocument();
             document.Load(filePattern);
             var lastChild = document.LastChild;
            
             var builder = new StringBuilder("");

            var list = lastChild.SelectNodes("head");
            if (list != null)
            {
                var innerText = list[0].InnerText;
                var xmlNodeList1 = lastChild.SelectNodes("colum");
                if (xmlNodeList1 != null)
                {
                    var str2 = xmlNodeList1[0].InnerText;
                    var selectNodes1 = lastChild.SelectNodes("boins");
                    if (selectNodes1 != null)
                    {
                        var str3 = selectNodes1[0].InnerText;
                        var nodeList1 = lastChild.SelectNodes("bootom");
                        if (nodeList1 != null)
                        {
                            var str4 = nodeList1[0].InnerText;
                            var nodes1 = lastChild.SelectNodes("overrides");
                            if (nodes1 != null)
                            {
                                var str17 = nodes1[0].InnerText;
                                var list1 = lastChild.SelectNodes("uncus");
                                if (list1 != null)
                                {
                                    var strArray = list1[0].InnerText.Split(new[] { ';' });
                                    builder.ToString().Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    int num = 0;
                                    string tableClass = tableNameHed;
                                    var builder2 = new StringBuilder();
                                    var prk = ds.Tables[tableNameHed].PrimaryKey;
                                    foreach (DataColumn r in ds.Tables[tableNameHed].Columns)
                                    {
                                        builder2.Append(prk[0].ColumnName.ToUpper() == r.ColumnName.ToUpper()
                                                            ? strArray[1].Replace("name=\"Name\"",
                                                                                  "name=\"" + r.ColumnName + "_1\"").
                                                                  Replace("column=\"Name\"",
                                                                          "column=\"" + r.ColumnName + "\"").Replace(
                                                                              "\"Type\"",
                                                                              "\"" + r.DataType + "\"").
                                                                  Replace("native", "assigned")
                                                            : str2.Replace("name=\"Name\"",
                                                                           "name=\"" + r.ColumnName + "_1\"").Replace(
                                                                               "column=\"Name\"",
                                                                               "column=\"" + r.ColumnName + "\"").
                                                                  Replace("Type", r.DataType.Name).Replace(
                                                                      "length=\"Length\"", " "));
                                        builder2.Append("\r\n");
                                    }
                                    var builder3 = new StringBuilder();
                                   var builder4 = new StringBuilder();
                                   var builder5 = new StringBuilder();
                                    foreach (var o in tableMappNameJoin)
                                    {
                                        builder4.Remove(0, builder4.Length);
                                        builder3.Remove(0, builder3.Length);
                                        const string newValue = "keyColumn";
                                        var prk2 = ds.Tables[o].PrimaryKey;
                                        foreach (DataColumn r in ds.Tables[o].Columns)/////////////////////////////////////////////////////////new
                                        {
                                            if (prk2[0].ColumnName.ToUpper() == r.ColumnName.ToUpper())
                                            {
                                                // builder3.Append(strArray[1].Replace("name=\"Name\"", "name=\"" + R.ColumnName + "_1\"").Replace("column=\"Name\"", "column=\"" + R.ColumnName + "\"").Replace("\"Type\"", "\"" + R.ColumnName + "\"").Replace("native", "assigned"));
                                            }
                                            else
                                            {
                                                builder3.Append(str2.Replace("Name", r.ColumnName).Replace("Type", r.DataType.Name).Replace("length=\"Length\"", " ") + "\r\n");
                                            }
                                        }
                                        builder4.Append(str3.Replace("#NAME", o).Replace("#TABLE", o).Replace("#KeyID", newValue).Replace("#PROPERTY", builder3.ToString()));
                                        builder5.Append(builder4);
                                    }
                                    string str6 = "";
                                    if (showHelp)
                                    {
                                        var selectNodes = lastChild.SelectNodes("helper");
                                        if (selectNodes != null) str6 = selectNodes[0].InnerText;
                                    }
                                    File.WriteAllText(dirictoryName + "/" + tableClass + ".hbm.xml", string.Concat(new object[] { innerText.Replace("\"Class\"", "\"" + tableClass + "\"").Replace("\"Table\"", "\"" + tableClass + "\"").Replace("\"Namespace\"", "\"" + Namespace + "\"").Replace("\"Assembly\"", "\"" + assembly + "\""), "\r\n", builder2.ToString(), "\r\n", builder5, "\r\n", str4, "\r\n\r\n\r\n", str6 }), Encoding.UTF8);
                                    num++;
                                    var xmlNodeList = lastChild.SelectNodes("headlist");
                                    if (xmlNodeList != null)
                                    {
                                        var str7 = xmlNodeList[0].InnerText;
                                        var selectNodes = lastChild.SelectNodes("ctor");
                                        if (selectNodes != null)
                                        {
                                            var str8 = selectNodes[0].InnerText;
                                            var nodeList = lastChild.SelectNodes("metod");
                                            if (nodeList != null)
                                            {
                                                var str9 = nodeList[0].InnerText;
                                                var nodes = lastChild.SelectNodes("metodJoin");
                                                if (nodes != null)
                                                {
                                                    var str10 = nodes[0].InnerText;
                                                    builder2.Remove(0, builder2.Length);
                                                    builder2.Append(str7.Replace("#CLASS", tableClass).Replace("#NAMECPASE", Namespace) + "\r\n\r\n");
                                                    foreach (DataColumn r in ds.Tables[tableNameHed].Columns)
                                                    {
                                                        builder2.Append("        private " + r.DataType + " _" + r.ColumnName + "_1;\r\n");
                                                    }
                                                    builder2.Append("\r\n");
                                                    builder2.Append(str8.Replace("#CLASS", tableClass) + "\r\n");
                                                    builder2.Append("        #region Public\r\n");
                                                    foreach (DataColumn r in ds.Tables[tableNameHed].Columns)
                                                    {
                                                        builder2.Append(str9.Replace("#TYPE",r.DataType.ToString()).Replace("#METODNAME", r.ColumnName + "_1") + "\r\n");
                                                    }
                                                    builder2.Append("        #endregion\r\n");


                                                    _key = ds.Tables[tableNameHed].PrimaryKey[0].ColumnName+"_1";

                                                    if (showOverrides)
                                                    {
                                                        builder2.Append(str17.Replace("#CLASS", tableClass).Replace("#KEY", _key) + "\r\n");
                                                    }
                                                    builder2.Append("    }\r\n}");
                                                    File.WriteAllText(dirictoryName + "/" + tableClass + ".cs", builder2.ToString(), Encoding.UTF8);




                                                    foreach (string o in tableMappNameJoin)//join
                                                    {
                                                        builder2.Remove(0, builder2.Length);
                                                        builder2.Append(str7.Replace("#CLASS", o).Replace("#NAMECPASE", Namespace) + "\r\n\r\n");
                                                        foreach (DataColumn r in ds.Tables[o].Columns)
                                                        {
                                                            builder2.Append("        private " + r.DataType.Name + " _" + r.ColumnName + ";\r\n");
                                                        }
                                                        foreach (DataColumn r in ds.Tables[tableNameHed].Columns)
                                                        {
                                                            builder2.Append("        private " + r.DataType.Name + " _" + r.ColumnName + "_1;\r\n");
                                                        }
                                                        builder2.Append("\r\n");
                                                        builder2.Append(str8.Replace("#CLASS", o) + "\r\n");
                                                        builder2.Append("        #region Public\r\n");
                                                        foreach (DataColumn r in ds.Tables[o].Columns)
                                                        {
                                                            builder2.Append(str9.Replace("#TYPE", r.DataType.Name).Replace("#METODNAME", r.ColumnName) + "\r\n");
                                                        }
                                                        foreach (DataColumn r in ds.Tables[tableNameHed].Columns)
                                                        {
                                                            builder2.Append(str10.Replace("#TYPE", r.DataType.Name).Replace("#METODNAME",r.ColumnName) + "\r\n");
                                                        }
                                                        builder2.Append("        #endregion\r\n   ");



                                                        _key = ds.Tables[o].PrimaryKey[0].ColumnName;

                                                        if (showOverrides)
                                                        {
                                                            builder2.Append(str17.Replace("#CLASS", tableClass).Replace("#KEY", _key) + "\r\n");
                                                        }
                                                        builder2.Append("    }\r\n}");




                                                        File.WriteAllText(dirictoryName + "/" + o + ".cs", builder2.ToString(), Encoding.UTF8);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
         }
         public static void ComponentMap(string filePattern,
                     string dirictoryName,
                     DataSet dataSet,
                     string tableName,
                     string classComponentName, 
               List<string> componentTableColumnName,
                       bool showHelp,
                       bool showOverrides,
                     string Namespace,
                     string assembly)
         {
             //this.Left.Clear();
             //if (((this.Conect == "") || (this.Patch == "")) || (this.LV1.Items.Count == 0))
             //{
             //    MessageBox.Show("Не назначена строка соединения, или папка назначения    \r\n или не выбраны поля для компонента", "error");
             //}
             //else
             //{
             XmlDocument document = new XmlDocument();
             document.Load(filePattern);
             XmlNode lastChild = document.LastChild;
             var xmlNodeList = lastChild.SelectNodes("head");
             if (xmlNodeList != null)
             {
                 var innerText = xmlNodeList[0].InnerText;
                 var selectNodes = lastChild.SelectNodes("colum");
                 if (selectNodes != null)
                 {
                     var str2 = selectNodes[0].InnerText;
                     var nodeList = lastChild.SelectNodes("bootom");
                     int num = 0;
                     if (nodeList != null)
                     {
                         var str3 = nodeList[0].InnerText;
                         var nodes = lastChild.SelectNodes("uncus");
                         if (nodes != null)
                         {
                             string[] strArray = nodes[0].InnerText.Split(new[] { ';' });

                             var builder = new StringBuilder("");
                             foreach (DataColumn column in dataSet.Tables[tableName].Columns)
                             {
                                 if (column.Unique)
                                 {
                                     if (column.MaxLength == -1)
                                     {
                                         builder.Append(strArray[1].Replace("\"Name\"", "\"" + column.ColumnName + "\"").Replace("\"Type\"", "\"" + column.DataType.Name + "\""));
                                         _key = column.ColumnName;
                                     }
                                     else
                                     {
                                         builder.Append(strArray[0].Replace("\"Name\"", "\"" + column.ColumnName + "\"").Replace("\"Type\"", "\"" + column.DataType.Name + "\"").Replace("\"Length\"", "\"" + column.MaxLength.ToString(CultureInfo.InvariantCulture) + "\""));
                                         _key = column.ColumnName;
                                     }
                                     builder.Append("\r\n");
                                     continue;
                                 }
                               
                                 if (componentTableColumnName.IndexOf(column.ColumnName) == -1)
                                 {
                                     builder.Append(str2.Replace("Name", column.ColumnName).Replace("Type", column.DataType.Name.Remove(0, column.DataType.Name.IndexOf('.') + 1)).Replace("\"Length\"", "\"" + column.MaxLength.ToString(CultureInfo.InvariantCulture) + "\""));
                                 }
                              
                                 builder.Append("\r\n");
                             }
                             builder.Append("         <component name=\"" + classComponentName + "\"  class=\"" + classComponentName + "\" >\r\n");
                             foreach (string str5 in componentTableColumnName)
                             {
                                 DataColumn column = dataSet.Tables[tableName].Columns[str5];
                                 builder.Append(str2.Replace("Name", column.ColumnName).Replace("Type", column.DataType.Name.Remove(0, column.DataType.Name.IndexOf('.') + 1)).Replace("\"Length\"", "\"" + column.MaxLength.ToString(CultureInfo.InvariantCulture) + "\""));
                             }
                             builder.Append("         </component>\r\n");
                             string str6 = "";
                             if (showHelp)
                             {
                                 var list = lastChild.SelectNodes("helper");
                                 if (list != null)
                                     str6 = list[0].InnerText;
                             }
                             File.WriteAllText(dirictoryName + "/" + tableName + ".hbm.xml", innerText.
                                                                                                 Replace("\"Class\"", "\"" + tableName + "\"").
                                                                                                 Replace("\"Table\"", "\"" + tableName + "\"").
                                                                                                 Replace("\"Namespace\"", "\"" + Namespace + "\"").
                                                                                                 Replace("\"Assembly\"", "\"" + assembly + "\"")
                                                                                             + "\r\n" + builder + "\r\n" + str3 + "\r\n\r\n\r\n" + str6, Encoding.UTF8);
                             /////////////////////////////////////////////         
                             var xmlNodeList1 = lastChild.SelectNodes("overrides");
                             if (xmlNodeList1 != null)
                             {
                                 var str7 = xmlNodeList1[0].InnerText;

                                 var builder2 = new StringBuilder();
                                 var builder3 = new StringBuilder();
                                 foreach (DataColumn column2 in dataSet.Tables[tableName].Columns)
                                 {
                                     if (componentTableColumnName.IndexOf(column2.ColumnName) == -1)
                                     {
                                         builder3.Append("        private " + column2.DataType.Name + " " + column2.ColumnName + "_;\r\n");
                                         builder2.Append("        public virtual " + column2.DataType.Name + " " + column2.ColumnName + "\r\n        {\r\n            get { return " + column2.ColumnName + "_; }\r\n");
                                         builder2.Append("            set { " + column2.ColumnName + "_ = value; }\r\n        }\r\n");
                                     }
                                 }
                                 builder3.Append("        private " + classComponentName + " _" + classComponentName + ";\r\n");
                                 builder2.Append("        public virtual " + classComponentName + " " + classComponentName + "\r\n        {\r\n            get { return _" + classComponentName + "; }\r\n");
                                 builder2.Append("            set { _" + classComponentName + " = value; }\r\n        }\r\n");
                                 StringBuilder builder4 = new StringBuilder("");
                                 builder4.Append(" using System;\r\n using System.Collections.Generic;\r\n using System.Text;");
                                 builder4.Append("\r\n\r\nnamespace " + Namespace + "\r\n{\r\n");
                                 builder4.Append("    public class " + tableName + ":" + classComponentName + "\r\n    {\r\n");
                                 builder4.Append(builder3 + "\r\n");
                                 builder4.Append("        public " + tableName + "()\r\n        {\r\n        }\r\n\r\n");
                                 builder4.Append(builder2);
                                 if (showOverrides)
                                 {
                                     builder4.Append(str7.Replace("#CLASS",tableName).Replace("#KEY", _key) + "\r\n");
                                 }
                                 builder4.Append("    }\r\n}");
                                 File.WriteAllText(dirictoryName + "/" + tableName + ".cs", builder4.ToString(), Encoding.UTF8);
                             }

                             var builder5 = new StringBuilder();
                             var builder6 = new StringBuilder();
                             foreach (DataColumn column3 in dataSet.Tables[tableName].Columns)
                             {
                                 if (componentTableColumnName.IndexOf(column3.ColumnName) != -1)
                                 {
                                     builder6.Append("        private " + column3.DataType.Name + " " + column3.ColumnName + "_;\r\n");
                                     builder5.Append("        public virtual " + column3.DataType.Name + " " + column3.ColumnName + "\r\n        {\r\n            get { return " + column3.ColumnName + "_; }\r\n");
                                     builder5.Append("            set { " + column3.ColumnName + "_ = value; }\r\n        }\r\n");
                                 }
                             }
                           
                             var builder7 = new StringBuilder("");
                             builder7.Append(" using System;\r\n using System.Collections.Generic;\r\n using System.Text;");
                             builder7.Append("\r\n\r\nnamespace " + Namespace + "\r\n{\r\n");
                             builder7.Append("    public class " + classComponentName + "\r\n    {\r\n");
                             builder7.Append(builder6 + "\r\n");
                             builder7.Append("        public " + classComponentName + "()\r\n        {\r\n        }\r\n\r\n");
                             builder7.Append(builder5);
                             builder7.Append("    }\r\n}");
                             File.WriteAllText(dirictoryName + "/" + classComponentName + ".cs", builder7.ToString(), Encoding.UTF8);
                         }
                     }
                 }
             }
         }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePattern"></param>
        /// <param name="dirictoryName"></param>
        /// <param name="ds"></param>
        /// <param name="tableMappSubClassName"></param>
        /// <param name="tableNameGeneralClass"></param>
        /// <param name="discriminatorName"></param>
        /// <param name="showHelp"></param>
        /// <param name="showOverrides"></param>
        /// <param name="Namespace"></param>
        /// <param name="assembly"></param>
         public static void SubClass(
                      string filePattern,
                      string dirictoryName,
                     DataSet ds,
                List<string> tableMappSubClassName,
                      string tableNameGeneralClass,
                      string discriminatorName,
                        bool showHelp,
                        bool showOverrides,
                      string Namespace,
                      string assembly)
        {
           
                var document = new XmlDocument();
                document.Load(filePattern);
                var lastChild = document.LastChild;
            var selectNodes = lastChild.SelectNodes("colum");
            if (selectNodes != null)
            {
                var innerText = selectNodes[0].InnerText;
            }
            var nodeList = lastChild.SelectNodes("colum2");
            if (nodeList != null)
            {
                var str = nodeList[0].InnerText;
                var xmlNodeList = lastChild.SelectNodes("subClassHead");
                if (xmlNodeList != null)
                {
                    var str2 = xmlNodeList[0].InnerText;
                    var nodes = lastChild.SelectNodes("subclass");
                    if (nodes != null)
                    {
                        var str3 = nodes[0].InnerText;
                        var list = lastChild.SelectNodes("headlist");
                        if (list != null)
                        {
                            var str4 = list[0].InnerText;
                            var xmlNodeList1 = lastChild.SelectNodes("ctor");
                            if (xmlNodeList1 != null)
                            {
                                var str5 = xmlNodeList1[0].InnerText;
                                var selectNodes1 = lastChild.SelectNodes("metod");
                                if (selectNodes1 != null)
                                {
                                    var str6 = selectNodes1[0].InnerText;
                                    var nodeList1 = lastChild.SelectNodes("metod2");
                                    if (nodeList1 != null)
                                    {
                                        var str7 = nodeList1[0].InnerText;
                                        var builder = new StringBuilder("");
                                        var builder2 = new StringBuilder("");
                                        var builder3 = new StringBuilder("");
                                        builder.Append(str2.Replace("#NAMESPASE", Namespace).Replace("#ASSEMBLY", assembly).Replace("#CLASSN", tableNameGeneralClass).Replace("#DISCOLUM", discriminatorName));
                                        int num = 1;
                                        foreach (DataTable table in ds.Tables)
                                        {
                                            if (tableMappSubClassName.IndexOf(table.TableName) == -1) continue;
                                            if (builder2.Length > 0)
                                            {
                                                builder2.Remove(0, builder2.Length);
                                            }
                                            if (builder3.Length > 0)
                                            {
                                                builder3.Remove(0, builder3.Length);
                                            }
                                            foreach (DataColumn column in table.Columns)
                                            {
                                                if (column.DataType.BaseType != null && column.DataType.BaseType.Name == "ValueType")
                                                {
                                                    builder2.Append(str.Replace("Name", column.ColumnName + "_" + num).Replace("Type", column.DataType.Name) + "\r\n");
                                                }
                                                else
                                                {
                                                    builder2.Append(str.Replace("Name", column.ColumnName + "_" + num).Replace("Type", column.DataType.Name).Replace("Length", column.MaxLength.ToString(CultureInfo.InvariantCulture)) + "\r\n");
                                                }
                                            }
                                            builder3.Append(str3.Replace("#NAME", table.TableName).Replace("#VAL", "N" + num).Replace("#PROPERTY", "\r\n" + builder2));
                                            builder3.Append("\r\n         #SUBCLASS \r\n");
                                            builder = builder.Replace("#SUBCLASS", builder3 + "\r\n");
                                            num++;
                                        }
                                        File.WriteAllText(dirictoryName + "/" + tableNameGeneralClass + ".hbm.xml", builder.ToString().Replace("#SUBCLASS", ""), Encoding.UTF8);
                                        builder.Remove(0, builder.Length);
                                        var builder4 = new StringBuilder("");
                                        builder.Append(str4.Replace("#NAMECPASE", Namespace).Replace("#CLASS", tableNameGeneralClass) + "\r\n");
                                        builder.Append("        private UInt32 id_;\r\n        private String " + discriminatorName + "_;\r\n");
                                        num = 1;
                                        foreach (DataTable table in ds.Tables)
                                        {
                                            if (tableMappSubClassName.IndexOf(table.TableName) == -1) continue;
                                            foreach (DataColumn column in table.Columns)
                                            {
                        
                                                // string[] strArray2 = node5.Tag.ToString().Split(new char[] { ':' });
                                                builder.Append(string.Concat(new object[] { "        private ", column.DataType.Name, " ", column.ColumnName, num, "_", num, "_;\r\n" }));
                                                builder4.Append("\r\n" + str7.Replace("#TYPE", column.DataType.Name).Replace("#METODNAME", column.ColumnName + "_" + num).Replace("#M2", string.Concat(new object[] { column.ColumnName, num, "_", num, "_" })));
                                            }
                                            num++;
                                        }
                                        builder.Append(str5.Replace("#CLASS", tableNameGeneralClass) + "\r\n");
                                        builder.Append(str6.Replace("#TYPE", "UInt32").Replace("#METODNAME", "id") + "\r\n");
                                        builder.Append(str6.Replace("#TYPE", "String").Replace("#METODNAME", discriminatorName) + "\r\n");
                                        builder.Append(builder4);
                                        builder.Append("\r\n    }\r\n}");
                                        File.WriteAllText(dirictoryName + "/" + tableNameGeneralClass + ".cs", builder.ToString(), Encoding.UTF8);
                                        num = 1;
                                        foreach (DataTable table in ds.Tables)
                                        {
                                            if (tableMappSubClassName.IndexOf(table.TableName) == -1) continue;
                                            builder.Remove(0, builder.Length);
                                            builder4.Remove(0, builder4.Length);
                                            builder.Append(str4.Replace("#NAMECPASE", Namespace).Replace("#CLASS", table.TableName + ":" + tableNameGeneralClass) + "\r\n");
                                            foreach (DataColumn column in table.Columns)
                                            {
                                                //string[] strArray3 = node7.Tag.ToString().Split(new char[] { ':' });
                                                builder.Append(string.Concat(new object[] { "        private ", column.DataType.Name, " ", column.ColumnName, num, "_", num, "_;\r\n" }));
                                                builder4.Append("\r\n" + str7.Replace("#TYPE", column.DataType.Name).Replace("#METODNAME", column.ColumnName + "_" + num).Replace("#M2", string.Concat(new object[] { column.ColumnName, num, "_", num, "_" })));
                                            }
                                            builder.Append(str5.Replace("#CLASS", table.TableName) + "\r\n");
                                            builder.Append(builder4);
                                            builder.Append("\r\n    }\r\n}");
                                            File.WriteAllText(dirictoryName + "/" + table.TableName + ".cs", builder.ToString(), Encoding.UTF8);
                                            num++;
                                        }
                                        var nodes1 = lastChild.SelectNodes("createTable");
                                        if (nodes1 != null)
                                        {
                                            var str8 = nodes1[0].InnerText;
                                            var list1 = lastChild.SelectNodes("colum1");
                                            if (list1 != null)
                                            {
                                                var str9 = list1[0].InnerText;
                                                var xmlNodeList2 = lastChild.SelectNodes("colum22");
                                                if (xmlNodeList2 != null)
                                                {
                                                    var str10 = xmlNodeList2[0].InnerText;
                                                    num = 1;
                                                    builder4.Remove(0, builder4.Length);
                                                    foreach (DataTable table in ds.Tables)
                                                    {
                                                        if (tableMappSubClassName.IndexOf(table.TableName) == -1) continue;
                                                        builder.Remove(0, builder.Length);
                                                        foreach (DataColumn column in table.Columns)
                                                        {
                                                            if (column.DataType.BaseType != null && column.DataType.BaseType.Name == "ValueType")
                                                            {
                                                                builder.Append(str10.Replace("#COLUM", column.ColumnName + "_" + num) + "\r\n");
                                                            }
                                                            else
                                                            {
                                                                builder.Append(str9.Replace("#COLUM", column.ColumnName + "_" + num) + "\r\n");
                                                            }
                                                        }
                                                        builder4.Append(builder);
                                                        num++;
                                                    }
                                                }
                                            }
                                            builder.Remove(0, builder.Length);
                                            builder.Append(str8.Replace("#TABLE", tableNameGeneralClass).Replace("#discriminator", discriminatorName).Replace("#COLLUMS", builder4.ToString()));
                                        }
                                        File.WriteAllText(dirictoryName + "/" + tableNameGeneralClass + ".sql", builder.ToString(), Encoding.UTF8);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
         
        

        
         
    }
}
